"""
Test the fixtures
"""

from lhcbdirac_log import DataProvider


class TestFixture:
    """
    Test the fixtures
    """

    def test_empty0(self, data_provider: DataProvider):
        assert not data_provider.empty
        assert not data_provider.get(42).empty
        assert data_provider.get(64).empty

    def test_empty1(self, empty_data_provider: DataProvider):
        assert empty_data_provider.empty

    def test_empty2(self, readonly_data_provider: DataProvider):
        assert not readonly_data_provider.empty
        assert not readonly_data_provider.get(42).empty
        assert readonly_data_provider.get(64).empty

    def test_empty3(self, empty_readonly_data_provider: DataProvider):
        assert empty_readonly_data_provider.empty

    def test_empty4(self, any_empty_data_provider: DataProvider):
        assert any_empty_data_provider.empty

    def test_empty5(self, any_non_empty_data_provider: DataProvider):
        assert not any_non_empty_data_provider.empty
        assert not any_non_empty_data_provider.get(42).empty
        assert any_non_empty_data_provider.get(64).empty

    def test_readonly0(self, data_provider: DataProvider):
        assert not data_provider.readonly
        assert not data_provider.get(42).readonly
        assert not data_provider.get(42).get("data").readonly
        assert not data_provider.get(64).readonly

    def test_readonly1(self, empty_data_provider: DataProvider):
        assert not empty_data_provider.readonly

    def test_readonly2(self, readonly_data_provider: DataProvider):
        assert readonly_data_provider.readonly
        assert readonly_data_provider.get(42).readonly
        assert readonly_data_provider.get(42).get("data").readonly
        assert readonly_data_provider.get(64).readonly

    def test_readonly3(self, empty_readonly_data_provider: DataProvider):
        assert empty_readonly_data_provider.readonly

    def test_readonly5(self, any_readonly_data_provider: DataProvider):
        assert any_readonly_data_provider.readonly

    def test_readonly6(self, any_non_readonly_data_provider: DataProvider):
        assert not any_non_readonly_data_provider.readonly

    def test_compressed0(self, data_provider: DataProvider):
        assert data_provider.compressed == (data_provider.dict_provider is not None)
        assert data_provider.get(42).compressed == (data_provider.dict_provider is not None)
        assert data_provider.get(42).get("data").compressed == (data_provider.dict_provider is not None)
        assert data_provider.get(64).compressed == (data_provider.dict_provider is not None)

    def test_compressed1(self, empty_data_provider: DataProvider):
        assert empty_data_provider.compressed == (empty_data_provider.dict_provider is not None)

    def test_compressed2(self, readonly_data_provider: DataProvider):
        assert readonly_data_provider.compressed == (readonly_data_provider.dict_provider is not None)
        assert readonly_data_provider.get(42).compressed == (readonly_data_provider.dict_provider is not None)
        assert readonly_data_provider.get(42).get("data").compressed == (readonly_data_provider.dict_provider is not None)
        assert readonly_data_provider.get(64).compressed == (readonly_data_provider.dict_provider is not None)

    def test_compressed3(self, empty_readonly_data_provider: DataProvider):
        assert empty_readonly_data_provider.compressed == (empty_readonly_data_provider.dict_provider is not None)

    def test_compressed4(self, any_empty_data_provider: DataProvider):
        assert any_empty_data_provider.compressed == (any_empty_data_provider.dict_provider is not None)

    def test_compressed5(self, any_readonly_data_provider: DataProvider):
        assert any_readonly_data_provider.compressed == (any_readonly_data_provider.dict_provider is not None)

    def test_compressed6(self, any_non_readonly_data_provider: DataProvider):
        assert any_non_readonly_data_provider.compressed == (any_non_readonly_data_provider.dict_provider is not None)

    def test_compressed7(self, any_non_empty_data_provider: DataProvider):
        assert any_non_empty_data_provider.compressed == (any_non_empty_data_provider.dict_provider is not None)
        assert any_non_empty_data_provider.get(42).compressed == (any_non_empty_data_provider.dict_provider is not None)
        assert any_non_empty_data_provider.get(42).get("data").compressed == (any_non_empty_data_provider.dict_provider is not None)
        assert any_non_empty_data_provider.get(64).compressed == (any_non_empty_data_provider.dict_provider is not None)

    def test_compressed8(self, any_data_provider: DataProvider):
        assert any_data_provider.compressed == (any_data_provider.dict_provider is not None)

    def test_dict_empty0(self, data_provider: DataProvider):
        assert not data_provider.empty

    def test_dict_empty1(self, empty_data_provider: DataProvider):
        assert empty_data_provider.empty

    def test_dict_empty2(self, readonly_data_provider: DataProvider):
        assert not readonly_data_provider.empty

    def test_dict_empty3(self, empty_readonly_data_provider: DataProvider):
        assert empty_readonly_data_provider.empty

    def test_dict_empty4(self, any_empty_data_provider: DataProvider):
        assert any_empty_data_provider.empty

    def test_dict_empty5(self, any_non_empty_data_provider: DataProvider):
        assert not any_non_empty_data_provider.empty

    def test_dict_readonly0(self, data_provider: DataProvider):
        assert not data_provider.readonly

    def test_dict_readonly1(self, empty_data_provider: DataProvider):
        assert not empty_data_provider.readonly

    def test_dict_readonly2(self, readonly_data_provider: DataProvider):
        assert readonly_data_provider.readonly

    def test_dict_readonly3(self, empty_readonly_data_provider: DataProvider):
        assert empty_readonly_data_provider.readonly

    def test_dict_readonly4(self, any_readonly_data_provider: DataProvider):
        assert any_readonly_data_provider.readonly

    def test_dict_readonly5(self, any_non_readonly_data_provider: DataProvider):
        assert not any_non_readonly_data_provider.readonly
