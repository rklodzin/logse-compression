"""
Test the worker module.
"""

import pytest

from lhcbdirac_log import DictProvider
from lhcbdirac_log.zstd.pool import TaskQueue, Worker, Task
import time


class MockTask(Task[None]):

    __slots__ = ("_done", "_crash",)

    def __init__(self, crash: bool = False) -> None:
        self._done = False
        self._crash = crash
        super().__init__(None)

    def run(self) -> None:
        if self._crash:
            raise Exception("crash")
        self._done = True


def test_worker(dict_provider: DictProvider) -> None:
    """Test the worker class."""
    task1 = MockTask()
    task2 = MockTask(True)
    task3 = MockTask()
    task4 = MockTask()
    task_queue = TaskQueue(0, dict_provider)
    task_queue.put(task1)
    task_queue.put(task2)
    task_queue.put(task3)

    worker = Worker(task_queue)
    assert not worker.running

    worker.start()
    assert worker.running

    time.sleep(0.1)
    assert worker.running

    task_queue.put(None)
    task_queue.put(task4)
    worker.join()

    assert not worker.running
    assert task1._done
    assert not task2._done
    assert task3._done
    assert not task4._done

    assert len(task_queue.rejected) == 1
    assert task_queue.rejected[0][0] is task2
