"""Exceptions classes for the data provider layer.

This module provides the exceptions classes for the data provider layer implementations.

Classes:
    - ReadOnlyError: Raised when a read-only operation is attempted.
    - DictNotExistsError: Raised when a dict does not exist.
    - DataNotExistsError: Raised when a data does not exist.
    - JobNotExistsError: Raised when a job does not exist.
    - DictExistsError: Raised when a dict already exists.
    - DataExistsError: Raised when a data already exists.
    - JobExistsError: Raised when a job already exists.
    - DictInvalidError: Raised when an invalid dict is requested.
"""

__all__ = (
    "ReadOnlyError",
    "DataExistsError",
    "DataNotExistsError",
    "DictExistsError",
    "DictInvalidError",
    "DictNotExistsError",
    "JobExistsError",
    "JobNotExistsError",
)


class ReadOnlyError(Exception):
    """Raised when a write operation is attempted on a read-only object."""


class DictNotExistsError(Exception):
    """Raised when a dict does not exist."""


class DataNotExistsError(Exception):
    """Raised when a data does not exist."""


class JobNotExistsError(Exception):
    """Raised when a job does not exist."""


class DictExistsError(Exception):
    """Raised when a dict already exists."""


class DataExistsError(Exception):
    """Raised when a data already exists."""


class JobExistsError(Exception):
    """Raised when a job already exists."""


class DictInvalidError(Exception):
    """Raised when an invalid dict is requested."""
