"""This module provides the processor pool system for the zstd module.

Classes:
    - ProcessorPool: a pool of zstd processors, managing their assignments.
"""

import os
from collections.abc import Callable
from threading import RLock

from ..processors import ZstandardProcessor

__all__ = ("ProcessorPool",)


class ProcessorPool[P: ZstandardProcessor]:
    """[Internal] A pool of data processors, managing their assignments.

    Used by tasks to get a free processor, and to release it when done.
    """

    __slots__ = (
        "_factory",
        "_free",
        "_pool",
        "_processors",
        "_workers",
        "_lock",
    )

    def __init__(
        self,
        processor_factory: Callable[[], P],
        workers: int = os.cpu_count() or 2,
        processors: int | None = -3,
    ) -> None:
        """Initialize the pool.

        Args:
            processor_factory: a callable returning a new processor instance (may be a class)
            workers: the fixed number of workers (default: os.cpu_count() or 2)
            processors: the fixed number of processors, gave as positive (workers <= processors),
                or the variable number of processors, gave as negative (1 <= -processors <= workers),
                where the real number of processors is: N = max(workers, (loaded dict + 1) * -processors). (default None, maximum)

        Raises:
            ValueError: if processors is out of range
        """
        super().__init__()

        if processors is None:
            processors = -workers

        if -1 < processors < workers or processors < -workers:
            msg = f"processors is out of range, got {processors} (workers: {workers})"
            raise ValueError(msg)

        self._factory = processor_factory
        self._workers = workers
        self._processors = processors
        self._pool: set[P] = {processor_factory() for _ in range(processors)}
        self._free: dict[str | None, set[P]] = {None: self._pool.copy()}  # unused processors
        self._lock = RLock()

    def release(self, processor: P) -> None:
        """Release a processor, marking it as available for other workers.

        Args:
            processor: the processor to release

        Notes:
            - no check is done to ensure the processor is not in use
            - releasing an already released processor has not effect
        """
        with self._lock:
            self._free.setdefault(processor.dict_name, set()).add(processor)

    def __getitem__(self, dict_name: str | None) -> P | None:
        """Get a free processor for the given dictionary name, or None if not available.

        Args:
            dict_name: the processor dictionary name, or None for non-assigned processors

        Returns:
            a free processor, or None if not available

        Notes:
            - the pool will create new processors if needed, depending on dict_name and if the pool is variable
        """
        with self._lock:
            procs = self._free.setdefault(dict_name, set())

            # check if the pool is variable (<0) and needs to be updated (hint)
            if not procs and self._processors < 0:
                for _ in range(len(self._pool), max(self._workers, -self._processors * len(self._free))):  # len: loaded + None
                    self._pool.add(processor := self._factory())
                    self._free[None].add(processor)

            return procs.pop() if procs else None

    def get_any(self) -> P | None:
        """Get any free processor.

        Returns:
            a free processor, or None if none available

        Notes:
            - tries to return a non-assigned processor first
        """
        with self._lock:
            if procs := self._free[None]:  # opti: try with None first
                return procs.pop()

            for procs in self._free.values():
                if procs:
                    return procs.pop()

            return None
