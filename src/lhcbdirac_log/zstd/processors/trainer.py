"""This module provides the Zstandard trainer wrapper class.

The trainer is used to create dictionaries from a sample of data.

Classes:
    - ZstandardTrainer: Zstandard trainer wrapper class.
"""

from typing import BinaryIO, Self, override

from zstandard import ZstdCompressionDict, train_dictionary

from ...providers import DataEntry, DictProvider
from .processor import ZstandardProcessor

__all__ = ("ZstandardTrainer",)


class ZstandardTrainer(ZstandardProcessor[ZstandardProcessor]):
    """Wrapper for Zstandard trainer.

    Performs dictionary training from data, using Zstandard.

    Notes:
        - not thread-safe
    """

    __slots__ = ()

    @override
    def _new(self) -> Self:
        return self

    @override
    def process(self, *data: bytes | bytearray | memoryview, dict_id: int = 0, dict_size: int = 0) -> bytes:
        """Train a dictionary from the provided data.

        Args:
            *data: the data to use for training
            dict_id: the zstd dictionary id to use (default is 0 = auto)
            dict_size: the final size of the dictionary to train (default is 0 = auto)

        Returns:
            the trained dictionary data

        Raises:
            ValueError: if the dataset is invalid (too small or empty dataset, or only too big samples...)
            ZstdError: if the training fails
        """
        return self._train([i for i in data if len(data) > 0], dict_id=dict_id, dict_size=dict_size).as_bytes()

    @override
    def process_stream(self, instream: BinaryIO, outstream: BinaryIO, insize: int = -1) -> tuple[int, int]:
        """Process the provided stream. Not implemented.

        Args:
            instream: ignored
            outstream: ignored
            insize: ignored

        Returns:
            a tuple with the number of bytes read and written

        Raises:
            NotImplementedError: always
        """
        raise NotImplementedError

    def train(self, dict_provider: DictProvider, *data: DataEntry, dict_size: int = 0) -> bytes:
        """Train a dict with the sample data.

        The newly trained dictionary is added to the dictionary provider,
        and is available through the `dict` property.

        Args:
            dict_provider: the destination dictionary provider
            *data: the data to use for training
            dict_size: the final size of the dictionary to train (0: auto)

        Returns:
            the trained dictionary data

        Raises:
            ValueError: if the dataset is invalid (too small or empty dataset, or only too big samples...)
            ZstdError: if the training fails
        """
        fdata = [i for i in data if i.size > 0]  # filter out empty data

        if len(fdata) <= 1:
            msg = f"Not enough data to train a dictionary, at least 2 non-empty samples are required, got {len(fdata)}"
            raise ValueError(msg)

        fdata.sort(key=lambda x: x.size)  # sort by size (smallest first)

        samples = []  # dataset
        size = 0

        # load samples until max sample limit or max size limit is reached
        for i in range(min(len(fdata), self._config.train_max_sample)):
            dt = fdata[i]
            size += dt.size

            if size > self._config.train_max_size:
                size -= dt.size
                break

            with dt.reader() as r:
                samples.append(bytes(r.read()))

        d = self._train(samples, dict_size=dict_size, size=size)
        self._dict = dict_provider.add(fdata[0].dict_name, dt := d.as_bytes(), d.dict_id())
        return dt

    def _train(self, samples: list[bytes], *, dict_id: int = 0, size: int = -1, dict_size: int = 0) -> ZstdCompressionDict:
        """[Internal] Train a dictionary from the provided data.

        Args:
            samples: the data to use for training
            dict_id: the zstd dictionary id to use (default is 0 = auto)
            size: the total size of the samples (default is -1 = auto computed)
            dict_size: the final size of the dictionary to train (default is 0 = auto)

        Returns:
            the trained zstd dictionary

        Raises:
            ValueError: if the dataset is invalid (too small or empty dataset, or only too big samples...)
            ZstdError: if the training fails
        """
        if len(samples) <= 1:
            msg = f"Not enough data to train a dictionary, at least 2 non-empty, not too big, samples are required, got {len(samples)} (all samples size limit: {self._config.train_max_size})"
            raise ValueError(msg)

        sp = self._config.train_split_point or 0.75

        if int(sp * len(samples)) <= 1:  # change the split point if too small
            sp = min(2 / len(samples) + 0.0001, 1)  # split point min (+ float epsilon)

        return train_dictionary(
            split_point=sp,
            dict_id=dict_id,
            samples=samples,
            level=self._config.train_level,
            notifications=self._config.train_notifications,
            threads=self._config.train_threads,
            dict_size=max(dict_size or round((sum(len(i) for i in samples) if size < 0 else size) * sp / self._config.train_dict_size_factor), 0x100),
        )
