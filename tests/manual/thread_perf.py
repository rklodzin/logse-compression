"""
Test the performance of the threading configuration
"""
import json
import os
import time

from lhcbdirac_log.providers.database import SQLDataProvider, SQLDriver

from lhcbdirac_log.config import Config
from lhcbdirac_log import CompressorPool
from lhcbdirac_log.providers.fsspec import RootDataProvider
from lhcbdirac_log.providers.filesystem import FSDataProvider
from lhcbdirac_log.providers.memory import MemoryDataProvider, MemoryDictProvider

if __name__ == '__main__':
    n = 500
    with FSDataProvider("dump-test") as cache:

        if len(cache) != n:
            print(f"dump {n} jobs to {cache}...")
            with RootDataProvider("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/logSE/lhcb/MC/2016/LOG/00211361/0000") as srv:
                srv.transfer(cache, limit=500)

        with (MemoryDataProvider("src") as src,
              MemoryDictProvider("dictgen") as zdict,
              MemoryDataProvider("dictgen", zdict) as dictgen,
              #MemoryDataProvider("dst", zdict) as dst,
              SQLDataProvider(SQLDriver.create("th.db")) as dst,
              ):

            print("load in memory...")
            cache.transfer(src)

            print("generate dictionary...")
            pool = CompressorPool(zdict)
            st = time.time()

            for job in src:
                out = dictgen.create(job.job)

                for data in job:
                    pool.add(data, out.create(data.name))

            pool.flush()
            pool.stop()
            pool.join()

            print("dict generated", time.time() - st)
            tr = {}

            if dst.dict_provider is not zdict:
                dst.dict_provider.clear()
                zdict.transfer(dst.dict_provider)

            for i in range(0, 1):  # , os.cpu_count() + 1, 2):
                tr[i] = {}

                for u in [1] + list(range(0, round(os.cpu_count() * 1.5) + 1, 2)):

                    print("test timing with", i, "zstd threads and", u, "workers")

                    cpool = CompressorPool(zdict,
                                           Config(compression_thread=i),
                                           workers=u)
                    st = time.time()

                    with RootDataProvider(
                           "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/logSE/lhcb/MC/2016/LOG/00211361/0000") as SRC:
                        src = []
                        for job in SRC:
                            src.append(job)
                            if len(src) == 500:
                                break

                        for job in src:
                            out = dst.create(job.job)

                            for data in job:
                                cpool.add(data, out.create(data.name))

                    cpool.flush()
                    cpool.stop()
                    cpool.join()

                    et = time.time()
                    tr[i][u] = et - st

                    print("time:", et - st)
                    print("rejected:", cpool.rejected)
                    dst.clear(force=True)

                    json.dump(tr, open("thread_perf_inout.json", "w"), indent=4)
            import code
            code.interact(local=locals())
