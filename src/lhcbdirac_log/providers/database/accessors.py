"""Implementation of the data provider layer for SQL databases.

This module provides the SQLite database implementation of entries classes for the data provider layer.

Classes:
    - SQLDataEntry: SQL Database data entry implementation.
    - SQLDictEntry: SQL Database dictionary entry implementation.
    - SQLJobEntry: SQL Database job entry implementation.
    - SQLDataIO: SQL Data file-like I/O implementation for binary data access.
"""

from __future__ import annotations

import os
from collections.abc import Buffer, Generator, Iterable
from io import BufferedIOBase, UnsupportedOperation
from typing import BinaryIO, Self, override

from ...config import Config
from ..base import DataEntry, DataExistsError, DataNotExistsError, DictEntry, JobEntry, JobExistsError, JobInfo, JobNotExistsError, ReadOnlyError
from .driver import SQLDriver

__all__ = (
    "SQLDataEntry",
    "SQLDictEntry",
    "SQLJobEntry",
    "SQLDataIO",
)


class SQLDataIO(BufferedIOBase, BinaryIO):
    """SQL Data file-like I/O implementation for binary data access.

    Supported methods and properties:
        - name
        - mode
        - readable
        - writable
        - read
        - write
        - seekable
        - seek
        - tell
        - flush
        - close
        - __enter__
        - __exit__
    """

    __slots__ = (
        "_driver",
        "_entry",
        "_need_flush",
        "_read_buffer",
        "_read_mode",
        "_read_view",
        "_seek",
        "_write_buffer",
        "_write_buffer_size",
    )

    def __init__(
        self,
        driver: SQLDriver,
        entry: SQLDataEntry,
        write_buffer: int = 0,
        *,
        read_mode: bool = True,
    ) -> None:
        """[Internal] Initialize the SQL Data file-like I/O.

        Args:
            driver: the SQL driver
            entry: the data entry to work with
            write_buffer: the size of the write buffer (default is driver.BLOB_LIMIT)
            read_mode: True if the I/O is in read mode, False otherwise
        """
        super().__init__()

        self._entry = entry
        self._driver = driver

        self._read_mode = read_mode

        self._read_view: memoryview | None = None
        self._read_buffer: bytes | None = None
        self._seek = 0

        self._need_flush = True
        self._write_buffer: bytearray = bytearray()
        self._write_buffer_size = min(write_buffer, driver.blob_limit) or driver.blob_limit

    @override
    def __enter__(self) -> Self:
        return super().__enter__()

    @property
    @override
    def name(self) -> str:
        """Get the file name.

        Returns:
            the file name
        """
        return self._entry.name

    @property
    @override
    def mode(self) -> str:
        """Get the file mode.

        Returns:
            the file mode
        """
        return "rb" if self._read_mode else "wb"

    @override
    def __next__(self) -> bytes:
        raise UnsupportedOperation

    @override
    def readline(self, limit: int | None = -1) -> bytes:
        """Unsupported operation."""
        raise UnsupportedOperation

    @override
    def readable(self) -> bool:
        """Check if the file is readable.

        Returns:
            True if the file is readable, False otherwise

        Raises:
            ValueError: if the I/O is closed and in read mode
        """
        if self.closed and self._read_mode:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        return self._read_mode

    @override
    def writable(self) -> bool:
        """Check if the file is writable.

        Returns:
            True if the file is writable, False otherwise

        Raises:
            ValueError: if the I/O is closed and in write mode
        """
        if self.closed and not self._read_mode:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        return not self._read_mode

    @override
    def seek(self, offset: int, whence: int = os.SEEK_SET) -> int:
        """Seek to a position.

        Args:
            offset: the offset to seek to
            whence: the reference point for the offset

        Returns:
            the new position

        Raises:
            ValueError: if the I/O is closed or invalid whence
            OSError: if the I/O is in write-only mode or invalid offset
        """
        if self.seekable():
            old = self._seek

            match whence:
                case os.SEEK_SET:
                    self._seek = offset
                case os.SEEK_CUR:
                    self._seek += offset
                case os.SEEK_END:
                    if self._read_buffer is None:
                        self.read(0)
                    self._seek = len(self._read_buffer) + offset
                case _:
                    msg = "Invalid whence"
                    raise ValueError(msg)

            if self._seek < 0:
                self._seek = old
                msg = "Invalid offset"
                raise OSError(msg)
        else:
            msg = "Cannot seek using write-only mode"
            raise OSError(msg)

        return self._seek

    @override
    def seekable(self) -> bool:
        """Check if the file is seekable.

        Returns:
            True if the file is seekable, False otherwise

        Raises:
            ValueError: if the I/O is closed
        """
        if self.closed and self._read_mode:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        return self._read_mode

    @override
    def tell(self) -> int:
        """Get the current position.

        Returns:
            the current position
        """
        if self.closed:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        return self._seek

    @override
    def readlines(self, hint: int = -1) -> list[bytes]:
        """Unsupported operation."""
        raise UnsupportedOperation

    @override
    def writelines(self, lines: Iterable[Buffer]) -> None:
        """Unsupported operation."""
        raise UnsupportedOperation

    @override
    def read(self, size: int | None = -1) -> bytes:
        """Read data from the database.

        Args:
            size: the number of bytes to read (default is -1, read all)

        Returns:
            the read data

        Raises:
            DataNotExistsError: if the data does not exist
        """
        if self.closed:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        if not self._read_mode:
            msg = "Cannot read using write-only mode"
            raise UnsupportedOperation(msg)

        if self._read_buffer is None or self._read_view is None:
            self._read_buffer = self._driver.load_data(self._entry.job, self._entry.name)
            self._read_view = memoryview(self._read_buffer)
            self._seek = 0

        if size is None or size < 0:
            size = len(self._read_view)

        s = self._seek
        self._seek = min(len(self._read_buffer), self._seek + size)

        return bytes(self._read_view[s : self._seek])

    def _flush(self) -> None:
        """[Internal] Flush the write buffer into the database.

        Raises:
            OSError: if the I/O is in read-only mode

        Notes:
            - clear the internal buffer after flush (avoid sqlalchemy data duplication and memory leak)
            - reflushing will override existing data so may not be called between write operations
        """
        if self._read_mode:
            return

        if self._need_flush:
            self._need_flush = False

            self._driver.save_data(
                self._entry.job,
                self._entry.name,
                self._write_buffer,
            )

            self._write_buffer = bytearray()  # "clear" the buffer (sqlalchemy doesn't release its memoryview, locking the buffer)

    @override
    def write(self, data: Buffer | bytes | bytearray | memoryview, /) -> int:
        """Write data to the database.

        Args:
            data: the data to write

        Returns:
            the number of bytes written

        Raises:
            ValueError: if the I/O is closed
            OSError: if the I/O is in read-only mode
            BufferError: if the write buffer limit will be reached
        """
        if self.closed:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        if self._read_mode:
            msg = "Cannot write using read-only mode"
            raise UnsupportedOperation(msg)

        if len(data) + len(self._write_buffer) >= self._write_buffer_size:
            msg = f"Buffer limit reached: {len(self._write_buffer)} / {self._write_buffer_size} bytes"
            raise BufferError(msg)

        if data:
            self._need_flush = True
            self._write_buffer.extend(data)

        self._seek += len(data)
        return len(data)

    @override
    def close(self) -> None:
        """Close the file-like I/O."""
        if not self.closed:
            self._flush()
            super().close()
            if self._read_view is not None:
                self._read_view.release()
                self._read_view = None

    def __repr__(self) -> str:
        """Get the string representation of the file-like I/O.

        Returns:
            the string representation
        """
        return f"<{self.__class__.__name__} name={self.name!r} mode={self.mode!r} closed={self.closed}>"


class SQLDataEntry(DataEntry):
    """SQL Database data entry implementation.

    Only supports the new compressed (Zstandard) format.
    """

    __slots__ = ("_driver",)

    def __init__(self, driver: SQLDriver, name: str, job: int, *, readonly: bool) -> None:
        """[Internal] Initialize the data entry.

        Args:
            driver: the SQL driver
            name: the data name
            job: the job id
            readonly: indicate weather the data is read-only or not

        Notes:
            - compressed is always True
        """
        self._driver = driver
        super().__init__(name, job, compressed=True, readonly=readonly)

    @override
    def _reader(self) -> BinaryIO:
        return SQLDataIO(self._driver, self)

    @override
    def _writer(self) -> BinaryIO:
        return SQLDataIO(self._driver, self, read_mode=False)

    @override
    def _size(self) -> int | None:
        return self._driver.get_data_size(self.job, self.name)

    @override
    def _delete(self) -> None:
        self._driver.delete_data(self.job, self.name)


class SQLDictEntry(DictEntry):
    """SQL Database dictionary entry implementation."""

    __slots__ = ("_driver",)

    def __init__(
        self,
        driver: SQLDriver,
        name: str,
        config: Config,
        data: bytes | None = None,
        zstd_id: int | None = None,
    ) -> None:
        """[Internal] Initialize the dictionary entry.

        Args:
            driver: the SQL driver
            name: the dictionary name
            config: the configuration to use for precomputing the dictionary
            data: the dictionary data (create a new dict if not None)
            zstd_id: the zstd dictionary id (None for unknown)
        """
        self._driver = driver
        super().__init__(name, config, data, zstd_id)

    @override
    @property
    def exists(self) -> bool:
        return self._driver.check_dict(self.dict_name) is not None

    @override
    @property
    def size(self) -> int:
        return self._driver.get_dict_size(self.dict_name)

    @override
    def _load_data(self) -> bytes:
        d, z = self._driver.load_dict(self.dict_name)
        self._zstd_id = z
        return d

    @override
    def _save(self) -> None:
        self._driver.save_dict(self.name, self._data, self._zstd_id)


class SQLJobEntry(JobEntry[SQLDataEntry]):
    """SQL Database job entry implementation.

    Only supports the new compressed (Zstandard) format.
    """

    __slots__ = ("_driver",)

    def __init__(self, driver: SQLDriver, job: int, *, readonly: bool, create: bool = True, exists_ok: bool = True) -> None:
        """[Internal] Initialize the job entry.

        Args:
            driver: the SQL driver
            job: the job id
            readonly: indicate weather the job is read-only or not
            create: if True, create the job (default is True)
            exists_ok: if True, ignore the error if the data already exists (default is True)

        Raises:
            JobExistsError: if the job exists and exists_ok is False
            JobNotExistsError: if the job does not exist and create is False

        Notes:
            - compressed is always True
        """
        self._driver = driver

        if self._driver.check_job(job):
            if not exists_ok:
                raise JobExistsError(job)
        elif create:
            self._driver.save_job(job)
        else:
            raise JobNotExistsError(job)

        super().__init__(job, compressed=True, readonly=readonly)

    @property
    def driver(self) -> SQLDriver:
        """Get the SQL driver.

        Returns:
            the SQL driver
        """
        return self._driver

    @override
    def _get(self, name: str, *, create: bool = False) -> SQLDataEntry:
        if not create and not self._driver.check_data(self._job, name):
            raise DataNotExistsError(name)

        return SQLDataEntry(self._driver, name, self._job, readonly=self._readonly)

    @override
    def _create(self, name: str, *, exists_ok: bool = False) -> SQLDataEntry:
        if not exists_ok and self._driver.check_data(self._job, name):
            raise DataExistsError(name)

        return SQLDataEntry(self._driver, name, self._job, readonly=self._readonly)

    @override
    def files(self) -> Generator[str, None, None]:
        yield from self._driver.get_files(self._job)

    @override
    @property
    def data_size(self) -> int:  # optimizes the default implementation
        return self._driver.get_job_size(self._job)

    @override
    def delete(self, name: str) -> None:  # optimizes the default implementation
        if self._readonly:
            msg = "Cannot delete data from read-only job"
            raise ReadOnlyError(msg)

        self._driver.delete_data(self._job, name)

    @override
    def clear(self) -> None:  # optimizes the default implementation
        if self._readonly:
            msg = "Cannot clear data from read-only job"
            raise ReadOnlyError(msg)

        self._driver.clear_job(self._job)

    @override
    def __len__(self) -> int:
        return self._driver.get_data_count(self._job)

    @override
    def _update_info(self) -> None:
        self._driver.save_job(self._job, self._info)

    @override
    def _load_info(self) -> JobInfo:
        return self._driver.load_job(self._job)
