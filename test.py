import sys

from src import SQLDriver, SQLDictProvider, SQLDataProvider
from src import FSDataProvider
from src import RootDataProvider
from src import CompressorPool
import time

if __name__ == '__main__':

    #i = RootDataProvider("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/logSE/lhcb/MC/2016/LOG/00211361/0000")
    o = FSDataProvider("dump-test")
    #print(len(o))
    #o.clear(force=True)
    #print(len(o))

    #i.transfer(o, 500)

    sql = SQLDriver.create("test-0.db")

    with (SQLDataProvider(sql) as out_provider,
          FSDataProvider("dump-test", readonly=True) as in_provider):

        out_provider.clear(force=True)
        #out_provider.dict_provider.clear()
        print("start", len(out_provider.dict_provider))

        st = time.time()

        pool = CompressorPool(out_provider.dict_provider)

        for job in in_provider:
            out = out_provider.create(job.job)

            print(f"Add {job.job} to compressor pool")

            for data in job:
                pool.add(data, out.create(data.name))

        print("waiting...")
        pool.flush()
        pool.stop()
        pool.join()
        print("Compressor pool joined in {:.2f} seconds".format(time.time() - st))
    print("end")
