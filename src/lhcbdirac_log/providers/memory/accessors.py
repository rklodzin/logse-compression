"""Implementation of the in-memory data provider layer.

This module provides the in-memory implementation of providers classes for the data provider layer.

Classes:
    - MemoryDataEntry: Memory data entry implementation.
    - MemoryDictEntry: Memory dictionary entry implementation.
    - MemoryJobEntry: Memory job entry implementation.
"""

import os
from collections.abc import Buffer, Generator, Iterable
from io import BufferedIOBase, UnsupportedOperation
from typing import BinaryIO, Self, override

from ...config import Config
from ..base import (
    DataEntry,
    DataExistsError,
    DataNotExistsError,
    DictEntry,
    DictNotExistsError,
    JobEntry,
    JobExistsError,
    JobNotExistsError,
    ReadOnlyError,
)

__all__ = (
    "MemoryDataEntry",
    "MemoryDictEntry",
    "MemoryJobEntry",
    "MemoryDataIO",
)


class MemoryDataIO(BufferedIOBase, BinaryIO):
    """Memory data file-like I/O implementation for binary data access.

    Supported methods and properties:
        - name
        - mode
        - readable
        - writable
        - read
        - write
        - seekable
        - seek
        - tell
        - flush
        - close
        - __enter__
        - __exit__

    Notes:
        - this exists as a workaround BytesIO limitations
        - allow access to the buffer, contrary to BytesIO
        - can be write / read-only, contrary to BytesIO
    """

    __slots__ = (
        "_buffer",
        "_seek",
        "_name",
        "_read_mode",
    )

    def __init__(
        self,
        name: str,
        buffer: bytearray,
        *,
        read_mode: bool = True,
    ) -> None:
        """[Internal] Initialize the Memory Data file-like I/O.

        Args:
            name: the data name
            buffer: the data buffer to read/write from/to
            read_mode: True if the I/O is in read mode, False otherwise
        """
        super().__init__()

        self._name = name
        self._buffer = buffer

        self._read_mode = read_mode

        self._seek = 0

    @override
    def __enter__(self) -> Self:
        return super().__enter__()

    @property
    @override
    def name(self) -> str:
        """Get the file name.

        Returns:
            the file name
        """
        return self._name

    @property
    @override
    def mode(self) -> str:
        """Get the file mode.

        Returns:
            the file mode
        """
        return "rb" if self._read_mode else "wb"

    @override
    def __next__(self) -> bytes:
        raise UnsupportedOperation

    @override
    def readline(self, limit: int | None = -1) -> bytes:
        """Unsupported operation."""
        raise UnsupportedOperation

    @override
    def readable(self) -> bool:
        """Check if the file is readable.

        Returns:
            True if the file is readable, False otherwise

        Raises:
            ValueError: if the I/O is closed and in read mode
        """
        if self.closed and self._read_mode:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        return self._read_mode

    @override
    def writable(self) -> bool:
        """Check if the file is writable.

        Returns:
            True if the file is writable, False otherwise

        Raises:
            ValueError: if the I/O is closed and in write mode
        """
        if self.closed and not self._read_mode:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        return not self._read_mode

    @override
    def seek(self, offset: int, whence: int = os.SEEK_SET) -> int:
        """Seek to a position.

        Args:
            offset: the offset to seek to
            whence: the reference point for the offset

        Returns:
            the new position

        Raises:
            ValueError: if the I/O is closed or invalid whence
            OSError: if the I/O is in write-only mode or invalid offset
        """
        if self.seekable():
            old = self._seek

            match whence:
                case os.SEEK_SET:
                    self._seek = offset
                case os.SEEK_CUR:
                    self._seek += offset
                case os.SEEK_END:
                    self._seek = len(self._buffer) + offset
                case _:
                    msg = "Invalid whence"
                    raise ValueError(msg)

            if self._seek < 0:
                self._seek = old
                msg = "Invalid offset"
                raise OSError(msg)
        else:
            msg = "Cannot seek using write-only mode"
            raise OSError(msg)

        return self._seek

    @override
    def seekable(self) -> bool:
        """Check if the file is seekable.

        Returns:
            True if the file is seekable, False otherwise

        Raises:
            ValueError: if the I/O is closed
        """
        if self.closed and self._read_mode:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        return self._read_mode

    @override
    def tell(self) -> int:
        """Get the current position.

        Returns:
            the current position
        """
        if self.closed:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        return self._seek

    @override
    def readlines(self, hint: int = -1) -> list[bytes]:
        """Unsupported operation."""
        raise UnsupportedOperation

    @override
    def writelines(self, lines: Iterable[Buffer]) -> None:
        """Unsupported operation."""
        raise UnsupportedOperation

    @override
    def read(self, size: int | None = -1) -> bytes:
        """Read data from the buffer.

        Args:
            size: the number of bytes to read (default is -1, read all)

        Returns:
            the read data

        Raises:
            DataNotExistsError: if the data does not exist
        """
        if self.closed:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        if not self._read_mode:
            msg = "Cannot read using write-only mode"
            raise UnsupportedOperation(msg)

        if size is None or size < 0:
            size = len(self._buffer)

        s = self._seek
        self._seek = min(len(self._buffer), self._seek + size)

        with memoryview(self._buffer) as m:
            return bytes(m[s : self._seek])

    @override
    def write(self, data: Buffer | bytes | bytearray | memoryview, /) -> int:
        """Write data to the buffer.

        Args:
            data: the data to write

        Returns:
            the number of bytes written

        Raises:
            ValueError: if the I/O is closed
            OSError: if the I/O is in read-only mode
        """
        if self.closed:
            msg = "I/O operation on closed file"
            raise ValueError(msg)

        if self._read_mode:
            msg = "Cannot write using read-only mode"
            raise UnsupportedOperation(msg)

        if data:
            self._buffer.extend(data)

        self._seek += len(data)
        return len(data)

    def __repr__(self) -> str:
        """Get the string representation of the file-like I/O.

        Returns:
            the string representation
        """
        return f"<{self.__class__.__name__} name={self.name!r} mode={self.mode!r} closed={self.closed}>"


class MemoryDataEntry(DataEntry):
    """Memory data entry implementation.

    This implementation supports both the old uncompressed and new compressed (Zstandard) formats.
    """

    __slots__ = ("_cache",)

    def __init__(self, name: str, job: int, cache: dict[str, bytearray], *, compressed: bool, readonly: bool) -> None:
        """[Internal] Initialize the data entry.

        Args:
            name: the data name
            job: the job id
            cache: the data cache for the job
            compressed: indicate whether the underlying data is compressed or not (in Zstandard)
            readonly: indicate weather the data is read-only or not
        """
        super().__init__(name, job, compressed=compressed, readonly=readonly)
        self._cache = cache

    def _reader(self) -> BinaryIO:
        return MemoryDataIO(self._name, self._cache[self._name])

    def _writer(self) -> BinaryIO:
        if (d := self._cache.get(self._name, None)) is None:
            self._cache[self._name] = d = bytearray()
        else:
            d.clear()

        return MemoryDataIO(self._name, d, read_mode=False)

    def _size(self) -> int | None:
        if (d := self._cache.get(self._name, None)) is None:
            return None

        return len(d)

    def _delete(self) -> None:
        d = self._cache.pop(self._name, None)
        if d is None:
            raise DataNotExistsError(self._name)

        d.clear()


class MemoryDictEntry(DictEntry):
    """Memory dictionary entry implementation."""

    __slots__ = ("_cache",)

    def __init__(self, name: str, cache: dict[str, bytes], config: Config, data: bytes | None = None, zstd_id: int | None = None) -> None:
        """[Internal] Initialize the dictionary entry.

        Args:
            name: the dictionary name
            cache: the dict cache
            config: the configuration to use for precomputing the dictionary
            data: the dictionary data (create a new dict if not None)
            zstd_id: the zstd dictionary id (None for unknown)
        """
        self._cache = cache
        super().__init__(name, config, data, zstd_id)

    @property
    @override
    def exists(self) -> bool:
        return self._name in self._cache

    @property
    @override
    def size(self) -> int:
        if (d := self._cache.get(self._name, None)) is None:
            return 0

        return len(d)

    @override
    def _load_data(self) -> bytes:
        if (d := self._cache.get(self._name, None)) is None:
            raise DictNotExistsError(self._name)

        return d

    @override
    def _save(self) -> None:
        self._cache[self._name] = self._data


class MemoryJobEntry(JobEntry[MemoryDataEntry]):
    """Memory job entry implementation.

    This implementation supports both the old uncompressed and new compressed (Zstandard) formats.
    """

    __slots__ = ("_cache",)

    def __init__(
        self, cache: dict[int, dict[str, bytearray]], job: int, *, compressed: bool, readonly: bool, create: bool = True, exists_ok: bool = True
    ) -> None:
        """[Internal] Initialize the job entry.

        Args:
            cache: the data provider cache (production cache)
            job: the job id
            compressed: indicate whether the underlying data is compressed or not (in Zstandard)
            readonly: indicate weather the job is read-only or not
            create: create the job cache if it doesn't exist, otherwise raise an error
            exists_ok: ignore the error if the job cache already exists

        """
        if job in cache:
            if not exists_ok:
                raise JobExistsError(job)
        elif create:
            cache[job] = {}
        else:
            raise JobNotExistsError(job)

        self._cache = cache[job]

        super().__init__(job, compressed=compressed, readonly=readonly)

    @override
    def _get(self, name: str, *, create: bool = False) -> MemoryDataEntry:
        if name not in self._cache and not create:
            raise DataNotExistsError(name)

        return MemoryDataEntry(name, self._job, self._cache, compressed=self.compressed, readonly=self._readonly)

    @override
    def _create(self, name: str, *, exists_ok: bool = False) -> MemoryDataEntry:
        if name in self._cache and not exists_ok:
            raise DataExistsError(name)

        return MemoryDataEntry(name, self._job, self._cache, compressed=self.compressed, readonly=self._readonly)

    @override
    def files(self) -> Generator[str, None, None]:
        yield from list(self._cache)

    @property
    @override
    def data_size(self) -> int:  # optimizes the default implementation
        return sum(len(d) for d in self._cache.values())

    @override
    def delete(self, name: str) -> None:
        if self._readonly:
            msg = f"Job '{self._job}' is read-only"
            raise ReadOnlyError(msg)

        if (d := self._cache.pop(name, None)) is None:
            raise DataNotExistsError(name)

        d.clear()

    @override
    def _update_info(self) -> None:  # pragma: no cover
        pass
