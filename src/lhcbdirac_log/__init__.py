"""This package provides both an easy zstd processing and a thread pool system for parallel zstd processing and a way to manage the data through the data providers layer.

Classes:
    - CompressorPool: a thread pool managing data compressors.
    - DecompressorPool: a thread pool managing data decompressors.
    - ThreadPool: an abstract thread pool managing workers and their tasks.
    - ZstandardCompressor: a data compressor.
    - ZstandardDecompressor: a data decompressor.
    - ZstandardProcessor: an abstract data processor.
    - ZstandardTrainer: a dictionary trainer.
    - DataEntry: a data entry.
    - DictEntry: a dictionary entry.
    - JobEntry: a job entry.
    - JobInfo: a job information.
    - DictProvider: a dictionary provider.
    - DataProvider: a data provider.
    - DictNotExistsError: a dictionary not exists error.
    - DictExistsError: a dictionary exists error.
    - DictInvalidError: a dictionary invalid error.
    - DataNotExistsError: a data not exists error.
    - DataExistsError: a data exists error.
    - JobExistsError: a job exists error.
    - JobNotExistsError: a job not exists error.
    - ReadOnlyError: a read-only error.
"""

from .providers import (
    DataEntry,
    DataExistsError,
    DataNotExistsError,
    DataProvider,
    DictEntry,
    DictExistsError,
    DictInvalidError,
    DictNotExistsError,
    DictProvider,
    JobEntry,
    JobExistsError,
    JobInfo,
    JobNotExistsError,
    ReadOnlyError,
)
from .zstd import (
    CompressorPool,
    DecompressorPool,
    ThreadPool,
    ZstandardCompressor,
    ZstandardDecompressor,
    ZstandardProcessor,
    ZstandardTrainer,
)

__all__ = (
    "CompressorPool",
    "DecompressorPool",
    "ThreadPool",
    "ZstandardCompressor",
    "ZstandardDecompressor",
    "ZstandardProcessor",
    "ZstandardTrainer",
    "DataEntry",
    "DictEntry",
    "JobEntry",
    "JobInfo",
    "DictProvider",
    "DataProvider",
    "DictNotExistsError",
    "DictExistsError",
    "DictInvalidError",
    "DataNotExistsError",
    "DataExistsError",
    "JobExistsError",
    "JobNotExistsError",
    "ReadOnlyError",
)
