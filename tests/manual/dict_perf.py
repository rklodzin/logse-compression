"""
Test the performance of the threading configuration
"""
import json
import os
import time
import sys

import pytest

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

from lhcbdirac_log import CompressorPool
from lhcbdirac_log.providers.fsspec import RootDataProvider
from lhcbdirac_log.providers.filesystem import FSDataProvider
from lhcbdirac_log.providers.memory import MemoryDataProvider, MemoryDictProvider

if __name__ == '__main__':
    n = 500
    with FSDataProvider("dump-test") as cache:

        if len(cache) != n:
            print(f"dump {n} jobs to {cache}...")
            with RootDataProvider("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/logSE/lhcb/MC/2016/LOG/00211361/0000") as srv:
                srv.transfer(cache, limit=500)

        with (MemoryDataProvider("src") as src,
              MemoryDictProvider("dictgen") as zdict,
              MemoryDictProvider("nodict") as ndict,
              MemoryDataProvider("dictgen", zdict) as dictgen,
              MemoryDataProvider("nodict", ndict) as nodict):

            print("load in memory...")
            cache.transfer(src, limit=100)

            print("compress with dictionaries...")
            pool = CompressorPool(zdict)
            st = time.time()

            for job in src:
                out = dictgen.create(job.job)

                for data in job:
                    pool.add(data, out.create(data.name))

            pool.flush()
            pool.stop()
            pool.join()

            print("done in", time.time() - st)
            print(pool.rejected, "jobs rejected")
            print("size:", dictgen.data_size)

            print("compress without dictionary...")

            for i in zdict:
                ndict.mark_invalid(i)

            cpool = CompressorPool(ndict)
            st = time.time()

            for job in src:
                out = nodict.create(job.job)

                for data in job:
                    cpool.add(data, out.create(data.name))

            cpool.flush()
            cpool.stop()
            cpool.join()

            print("done in", time.time() - st)
            print(pool.rejected, "jobs rejected")
            print("size:", nodict.data_size)

            import code
            code.interact(local=locals())


@pytest.fixture
def tempdir():
    import tempfile
    import shutil
    d = tempfile.mkdtemp()
    yield d
    shutil.rmtree(d)

def test_files0(tempdir: str):
    open(tempdir + '/file.txt', 'w')  # crée un fichier vide

def test_files1(tempdir: str):
    open(tempdir + '/file.txt', 'w').write('Hello, world!')  # crée un fichier
