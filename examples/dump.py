"""
This example demontrates how to dump / extract jobs from any sub-production to a local directory.
"""

from lhcbdirac_log import DecompressorPool
from lhcbdirac_log.providers.filesystem import FSDataProvider, FSDictProvider
from lhcbdirac_log.providers.fsspec import RootDataProvider
from lhcbdirac_log.providers.database import SQLDataProvider, SQLDriver

url = "root://xxxx.xxxx.xxxx//xxxxxxxx/xxxxxxxx/xxxxxxxx"
path = "~/xxxxxxxx/xxxxxxxx_xxxx.db"


def main() -> None:
    """
    This example demontrates how to dump / extract jobs from any sub-production to a local directory.
    """

    with RootDataProvider(url) as src:

        with FSDataProvider("./prod_1") as dst:

            src.transfer(dst)  # dump & extract old ZIP jobs to the local directory

    with SQLDataProvider(SQLDriver.create(path)) as src:

        with FSDataProvider("./prod_2",
                            FSDictProvider("./zstd-dict")) as dst:

            src.transfer(dst)  # dump & extract new Zstandard+SQLite jobs to the local directory
            # tranfered data is stored in ./prod_2 and is still compressed in zstd
            # also transfers zstd-dict automatically  to ./zstd-dict

        with FSDataProvider("./prod_3") as dst:
            # dump & extract new Zstandard+SQLite jobs to the local directory
            # tranfered data is stored in ./prod_3 and is decompressed
            pool = DecompressorPool(src.dict_provider)

            for job in src:
                out = dst.create(job.job)  # create a new job
                out.update_info(job.info)  # update the job metadata

                for data in job:  # iterate over the data
                    pool.add(data, out.create(data.name))  # add the data to the pool

            pool.flush()  # flush the remaining training queues
            pool.stop()  # ask the pool to stop
            pool.join()  # join the pool

            print("jobs rejected:", pool.rejected)  # print rejected jobs and the error (if any)
            print("src size:", src.data_size, src.size)  # print the source size
            print("dst size:", dst.data_size, dst.size)  # print the destination size


if __name__ == '__main__':
    main()

