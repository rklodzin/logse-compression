"""
Test the zstd processor
"""
from io import BytesIO

import pytest
from zstandard import ZstdError

from lhcbdirac_log import ZstandardCompressor, ZstandardProcessor, ZstandardDecompressor
from lhcbdirac_log import DictEntry, DictProvider, DataProvider
from lhcbdirac_log.zstd.pool import ProcessorPool
from lhcbdirac_log import ZstandardTrainer


class TestZstandardProcessor:
    """Test the Zstandard processor."""

    def test_processor(self, processor: ZstandardProcessor, dict_entry: DictEntry):
        assert processor.dict is None
        assert processor.dict_name is None
        assert processor._processor is None

        _ = processor.processor  # test the property getter and load the dict
        assert processor.processor is processor.processor  # cached

        assert processor._processor is not None

        processor.dict = dict_entry  # set the dict
        assert processor.dict == dict_entry
        assert processor.dict_name == dict_entry.dict_name
        assert processor._processor is None  # processor is reset

        _ = processor.processor  # test the property getter and load the dict
        assert processor.processor is processor.processor  # cached

        assert processor._processor is not None

        processor.dict = None  # reset the dict

        assert processor.dict is None
        assert processor.dict_name is None
        assert processor._processor is None  # processor is reset

    def test_init(self, processor_with_dict: ZstandardProcessor):
        assert processor_with_dict.dict is not None

        _ = getattr(processor_with_dict, 'processor')  # test the property getter and load the dict (getattr: pycharm error)
        assert processor_with_dict._processor is not None

        processor_with_dict.dict = None
        assert processor_with_dict._processor is None
        assert processor_with_dict.dict is None

    def test_compression0(self, compressor_with_dict: ZstandardCompressor, decompressor: ZstandardDecompressor, data_sample: tuple[tuple[bytes, ...], ...]):
        assert compressor_with_dict.dict is not None
        assert decompressor.dict is None

        for d in data_sample:
            v = compressor_with_dict.compress(*d)

            if isinstance(v, bytes):
                compressed = [v]
                assert len(d) == 1
            else:
                compressed = list(v)
                assert len(d) != 1

            assert len(compressed) == len(d)
            assert all((not len(i)) == (not len(u)) for i, u in zip(d, compressed))

            if any(d):
                with pytest.raises(ZstdError):
                    _ = list(decompressor.decompress(*compressed))
            else:
                v = decompressor.decompress(*compressed)
                assert not any([v] if isinstance(v, bytes) else v)

    def test_compression1(self, compressor: ZstandardCompressor, decompressor: ZstandardDecompressor, dict_entry: DictEntry, data_sample: tuple[tuple[bytes, ...], ...]):
        assert compressor.dict is None
        assert decompressor.dict is None

        for dc, dd in ((None, None), (None, dict_entry), (dict_entry, dict_entry)):

            compressor.dict = dc
            decompressor.dict = dd

            for d in data_sample:
                v = compressor.compress(*d)

                if isinstance(v, bytes):
                    compressed = [v]
                    assert len(d) == 1
                else:
                    compressed = list(v)
                    assert len(d) != 1

                assert len(compressed) == len(d)
                assert all((not len(i)) == (not len(u)) for i, u in zip(d, compressed))  # f(b"") = b""

                v = decompressor.decompress(*compressed)
                decompressed = [v] if isinstance(v, bytes) else list(v)

                assert len(decompressed) == len(d)
                assert all((not len(i)) == (not len(u)) for i, u in zip(compressed, decompressed))  # f(b"") = b""
                assert all(i == u for i, u in zip(d, decompressed))

    def test_stream0(self, compressor_with_dict: ZstandardCompressor, decompressor: ZstandardDecompressor):
        assert compressor_with_dict.dict is not None
        assert decompressor.dict is None

        for d in (b"", b"test" , b"test" * 10_000_000):  # empty, small, big
            with (BytesIO(d) as i,
                  BytesIO() as o,
                  BytesIO() as x):

                compressor_with_dict.compress_stream(i, o)
                o.seek(0)

                with pytest.raises(ZstdError):
                    _ = decompressor.decompress_stream(o, x)

    def test_stream1(self, compressor: ZstandardCompressor, decompressor: ZstandardDecompressor, dict_entry: DictEntry):
        assert compressor.dict is None
        assert decompressor.dict is None

        for dc, dd in ((None, None), (None, dict_entry), (dict_entry, dict_entry)):

            compressor.dict = dc
            decompressor.dict = dd

            for d in (b"", b"test" , b"test" * 10_000_000):  # empty, small, big
                with (BytesIO(d) as i,
                      BytesIO() as o1,
                      BytesIO() as o2,
                      BytesIO() as d1,
                      BytesIO() as d2):

                    r1, w1 = compressor.compress_stream(i, o1)
                    i.seek(0)
                    r2, w2 = compressor.compress_stream(i, o2, len(d))

                    o1.seek(0)
                    o2.seek(0)

                    a1, b1 = decompressor.decompress_stream(o1, d1)
                    a2, b2 = decompressor.decompress_stream(o2, d2)

                    d1.seek(0)
                    d2.seek(0)

                    assert len(d) == r1 == r2 == b1 == b2
                    assert w1 == a1
                    assert w2 == a2

                    assert d == d1.read() == d2.read()

    def test_trainer(self,
                     trainer: ZstandardTrainer,
                     any_non_readonly_data_provider: DataProvider,
                     any_non_readonly_dict_provider: DictProvider) -> None:
        assert trainer.dict is None

        # test process stream
        with pytest.raises(NotImplementedError):
            _ = trainer.process_stream(BytesIO(), BytesIO())

        # test process
        with pytest.raises(ValueError):
            _ = trainer.process()

        assert len(trainer.process(*[b"zz"] * 10000)) > 0

        # test train
        if any_non_readonly_data_provider.compressed:  # mock the unused dict provider
            any_non_readonly_data_provider.dict_provider.add("xxxx", b"", 1)

        j = any_non_readonly_data_provider.create(777)
        d = [j.create(f"{i:04}") for i in range(100)]

        for n, i in enumerate(d):
            with i.writer() as w:
                w.write(b"zz" * (n > 50))

        with pytest.raises(ValueError):
            _ = trainer.train(any_non_readonly_dict_provider)

        with pytest.raises(ValueError):  # too small samples
            _ = trainer.train(any_non_readonly_dict_provider, *d[:52])

        with pytest.raises(ZstdError):  # too small samples (zstd lib error)
            _ = trainer.train(any_non_readonly_dict_provider, *d[:53])

        dt = trainer.train(any_non_readonly_dict_provider, *d)
        assert len(dt) > 0
        assert trainer.dict is not None
        assert trainer.dict_name == "xxxx"
        assert any_non_readonly_dict_provider.get("xxxx") is trainer.dict
        assert trainer.dict.data == dt


class TestProcessorPool:
    """Test the processor pool."""

    def test_validity(self) -> None:
        """Test the validity processor pool."""

        _ = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 5, 5)
        _ = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 5, 6)

        with pytest.raises(ValueError):
            _ = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 5, 4)

        with pytest.raises(ValueError):
            _ = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 5, 0)

        _ = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 5, -1)
        _ = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 5, -2)
        _ = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 5, -5)

        with pytest.raises(ValueError):
            _ = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 5, -6)

    def test_fixed(self, dict_provider: DictProvider) -> None:
        """Test the fixed processor pool."""

        pool = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 2, 3)

        # init, full
        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 3
        assert len(pool._free) == 1

        proc1 = pool[None]  # get a processor

        assert proc1 is not None

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 2
        assert len(pool._free) == 1

        proc2 = pool[None]  # get all processors
        proc3 = pool[None]

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0
        assert len(pool._free) == 1

        assert proc2 is not proc1  # check not the same
        assert proc2 is not proc3  # check not the same
        assert proc1 is not proc3  # check not the same

        assert pool[None] is None  # no more processors

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0
        assert len(pool._free) == 1

        assert pool["xx"] is None  # no more processors

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0
        assert len(pool._free["xx"]) == 0
        assert len(pool._free) == 2

        assert pool.get_any() is None  # no more processors

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0
        assert len(pool._free["xx"]) == 0
        assert len(pool._free) == 2

        proc2.dict = dict_provider.add("xx", b"", 42)
        pool.release(proc1)  # release a processor of type None
        pool.release(proc2)  # release a processor of type xx

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 1
        assert len(pool._free["xx"]) == 1
        assert len(pool._free) == 2

        assert pool.get_any() is proc1  # get a processor (of type None, prefered)
        assert pool.get_any() is proc2  # get a processor (of type xx)
        pool.release(proc2)  # release a processor of type xx

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0
        assert len(pool._free["xx"]) == 1
        assert len(pool._free) == 2

        assert pool[None] is None  # no more processors
        assert pool["xx"] is proc2  # get a processor (of type xx)

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0
        assert len(pool._free["xx"]) == 0
        assert len(pool._free) == 2

        pool.release(proc2)
        assert pool.get_any() is proc2  # get a processor (of type xx)

        proc1.dict = dict_provider.add("yy", b"", 42)
        proc2.dict = dict_provider.add("zz", b"", 42)
        proc3.dict = dict_provider.add("rr", b"", 42)

        pool.release(proc1)  # release a processor of type yy
        pool.release(proc2)  # release a processor of type zz
        pool.release(proc3)  # release a processor of type rr

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0
        assert len(pool._free["xx"]) == 0
        assert len(pool._free["yy"]) == 1
        assert len(pool._free["zz"]) == 1
        assert len(pool._free["rr"]) == 1
        assert len(pool._free) == 5

    def test_variable(self, dict_provider: DictProvider) -> None:
        """Test the fixed processor pool."""

        pool = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 3, -3)

        # init, empty
        assert len(pool._pool) == 0
        assert len(pool._free[None]) == 0
        assert len(pool._free) == 1

        proc1 = pool[None]  # get a processor

        assert proc1 is not None

        assert len(pool._pool) == 3  # 3 processors created
        assert len(pool._free[None]) == 2
        assert len(pool._free) == 1

        proc2 = pool[None]  # get all processors
        proc3 = pool[None]

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0  # no more processors
        assert len(pool._free) == 1

        assert proc2 is not proc1  # check not the same
        assert proc2 is not proc3  # check not the same
        assert proc1 is not proc3  # check not the same

        assert pool[None] is None  # no more processors

        assert len(pool._pool) == 3
        assert len(pool._free[None]) == 0
        assert len(pool._free) == 1

        assert pool["xx"] is None

        assert len(pool._pool) == 6  # 3 processors created
        assert len(pool._free[None]) == 3
        assert len(pool._free["xx"]) == 0
        assert len(pool._free) == 2

        proc4 = pool.get_any()
        proc5 = pool.get_any()

        assert proc4 is not None
        assert proc5 is not None

        proc4.dict = dict_provider.add("xx", b"", 42)
        proc5.dict = dict_provider.add("yy", b"", 42)

        assert len(pool._pool) == 6
        assert len(pool._free[None]) == 1
        assert len(pool._free["xx"]) == 0
        assert len(pool._free) == 2

        pool.release(proc4)
        pool.release(proc5)

        assert len(pool._pool) == 6
        assert len(pool._free[None]) == 1
        assert len(pool._free["xx"]) == 1
        assert len(pool._free["yy"]) == 1
        assert len(pool._free) == 3

        proc6 = pool.get_any()

        assert len(pool._pool) == 6
        assert len(pool._free[None]) == 0
        assert len(pool._free["xx"]) == 1
        assert len(pool._free["yy"]) == 1
        assert len(pool._free) == 3

        proc7 = pool.get_any()
        assert proc7 is proc4 or proc7 is proc5
        pool.release(proc7)

        assert len(pool._pool) == 6
        assert len(pool._free[None]) == 0
        assert len(pool._free["xx"]) == 1
        assert len(pool._free["yy"]) == 1
        assert len(pool._free) == 3

        assert pool.get_any() is not None
        assert pool.get_any() is not None
        assert pool.get_any() is None

        assert len(pool._pool) == 6
        assert len(pool._free[None]) == 0
        assert len(pool._free["xx"]) == 0
        assert len(pool._free["yy"]) == 0
        assert len(pool._free) == 3

        assert pool[None] is not None

        assert len(pool._pool) == 9  # 3 processors created (yy update)
        assert len(pool._free[None]) == 2
        assert len(pool._free["xx"]) == 0
        assert len(pool._free["yy"]) == 0
        assert len(pool._free) == 3

        proc1.dict = dict_provider.add("zz", b"", 42)
        pool.release(proc1)

        assert len(pool._pool) == 9
        assert len(pool._free[None]) == 2
        assert len(pool._free["xx"]) == 0
        assert len(pool._free["yy"]) == 0
        assert len(pool._free["zz"]) == 1
        assert len(pool._free) == 4

        assert pool["zz"] is proc1

        assert len(pool._pool) == 9
        assert len(pool._free[None]) == 2
        assert len(pool._free["xx"]) == 0
        assert len(pool._free["yy"]) == 0
        assert len(pool._free["zz"]) == 0
        assert len(pool._free) == 4

        assert pool["zz"] is None

        assert len(pool._pool) == 12  # 3 processors created (zz update)
        assert len(pool._free[None]) == 5
        assert len(pool._free["xx"]) == 0
        assert len(pool._free["yy"]) == 0
        assert len(pool._free["zz"]) == 0
        assert len(pool._free) == 4

        assert pool["rr"] is None
        assert pool["oo"] is None

        assert len(pool._pool) == 18  # 6 processors created (rr, oo update)
        assert len(pool._free[None]) == 11
        assert len(pool._free["xx"]) == 0
        assert len(pool._free["yy"]) == 0
        assert len(pool._free["zz"]) == 0
        assert len(pool._free["rr"]) == 0
        assert len(pool._free["oo"]) == 0
        assert len(pool._free) == 6
