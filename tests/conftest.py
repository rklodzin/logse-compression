"""
Fixtures for tests
"""
import os
import tempfile
from collections.abc import Generator
from os.path import join
from pathlib import Path
from zipfile import ZipFile

import pytest
from zstandard import train_dictionary

from lhcbdirac_log import ZstandardCompressor, ZstandardDecompressor
from lhcbdirac_log import DataProvider, DictProvider, DictEntry
from lhcbdirac_log.providers.database import SQLDataProvider, SQLDictProvider, SQLDriver
from lhcbdirac_log.providers.filesystem import FSDataProvider, FSDictProvider
from lhcbdirac_log.providers.fsspec import RootDataProvider
from lhcbdirac_log.providers.memory import MemoryDataProvider, MemoryDictProvider
from lhcbdirac_log.zstd.pool import TaskQueue
from lhcbdirac_log import ZstandardTrainer


class ProviderConfig:
    """
    Configuration for the test providers
    """

    __slots__ = (
        "empty",
        "readonly",
        "compressed",
        "provider",
    )

    def __init__(self,
                 provider: type[DataProvider], *,
                 empty: bool = False,
                 readonly: bool = False,
                 compressed: bool = False):
        self.provider = provider
        self.empty = empty
        self.readonly = readonly
        self.compressed = compressed


configs = [ProviderConfig(FSDataProvider, empty=True, readonly=True, compressed=False),
           ProviderConfig(FSDataProvider, empty=True, readonly=True, compressed=True),
           ProviderConfig(FSDataProvider, empty=True, readonly=False, compressed=False),
           ProviderConfig(FSDataProvider, empty=True, readonly=False, compressed=True),
           ProviderConfig(FSDataProvider, empty=False, readonly=True, compressed=False),
           ProviderConfig(FSDataProvider, empty=False, readonly=True, compressed=True),
           ProviderConfig(FSDataProvider, empty=False, readonly=False, compressed=False),
           ProviderConfig(FSDataProvider, empty=False, readonly=False, compressed=True),

           ProviderConfig(MemoryDataProvider, empty=True, readonly=True, compressed=False),
           ProviderConfig(MemoryDataProvider, empty=True, readonly=True, compressed=True),
           ProviderConfig(MemoryDataProvider, empty=True, readonly=False, compressed=False),
           ProviderConfig(MemoryDataProvider, empty=True, readonly=False, compressed=True),
           ProviderConfig(MemoryDataProvider, empty=False, readonly=True, compressed=False),
           ProviderConfig(MemoryDataProvider, empty=False, readonly=True, compressed=True),
           ProviderConfig(MemoryDataProvider, empty=False, readonly=False, compressed=False),
           ProviderConfig(MemoryDataProvider, empty=False, readonly=False, compressed=True),

           ProviderConfig(SQLDataProvider, empty=False, readonly=False, compressed=True),
           ProviderConfig(SQLDataProvider, empty=False, readonly=True, compressed=True),
           ProviderConfig(SQLDataProvider, empty=True, readonly=False, compressed=True),
           ProviderConfig(SQLDataProvider, empty=True, readonly=True, compressed=True),

           ProviderConfig(RootDataProvider, empty=False, readonly=True, compressed=False),
           ProviderConfig(RootDataProvider, empty=True, readonly=True, compressed=False)]

dict_configs = [c for c in configs if c.compressed]
DATA = b'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP'
DICT = (
    train_dictionary(256, [b"==+=="] * 10),
    train_dictionary(256, [b"==+==-=="] * 10),
    train_dictionary(256, [b"==+==+=="] * 10)
)

DICT_DATA = tuple((d.as_bytes(), len(d.as_bytes()), d.dict_id()) for d in DICT)


@pytest.fixture
def bytes_data() -> bytes:
    """
    Provide some bytes data

    Returns:
        the data
    """

    return DATA


@pytest.fixture
def all_dict_data() -> tuple[tuple[bytes, int, int], ...]:
    """
    Provide some dictionary data

    Returns:
        the data
    """

    return DICT_DATA


@pytest.fixture(params=DICT_DATA)
def dict_data(request) -> tuple[bytes, int, int]:
    """
    Provide some dictionary data

    Returns:
        the data
    """

    return request.param


def fill_data_provider(pr: DataProvider):
    """
    Fill the data provider with some data

    Args:
        pr: the data provider
    """

    pr.create(64)
    j = pr.create(42)

    with j.create("data").writer():
        pass

    with j.create("data1").writer() as w:
        w.write(DATA)


def fill_dict_provider(pr: DictProvider):
    """
    Fill the dictionary provider with some data

    Args:
        pr: the dictionary provider
    """

    pr.add("data", DICT_DATA[0][0], DICT_DATA[0][2])
    pr.add("datax", DICT_DATA[1][0], DICT_DATA[1][2])


def filesystem_data_provider(empty: bool) -> Generator[str, None, None]:
    """
    Provides a filesystem data provider

    Args:
        empty: whether the provider should be empty

    Returns:
        the path to the filesystem data provider
    """

    with tempfile.TemporaryDirectory() as dirpath:
        if not empty:
            with FSDataProvider(Path(dirpath)) as pr:
                fill_data_provider(pr)

        yield dirpath


def filesystem_dict_provider(empty: bool) -> Generator[str, None, None]:
    """
    Provides a filesystem dict provider

    Args:
        empty: whether the provider should be empty

    Returns:
        the path to the filesystem dict provider
    """

    with tempfile.TemporaryDirectory() as dirpath:
        if not empty:
            with FSDictProvider(Path(dirpath)) as pr:
                fill_dict_provider(pr)
        yield dirpath


def memory_data_provider(empty: bool) -> Generator[str, None, None]:
    """
    Provides a memory data provider

    Args:
        empty: whether the provider should be empty

    Returns:
        the provider name in cache entry
    """

    name = os.urandom(8).hex()
    if not empty:
        with MemoryDataProvider(name) as pr:
            fill_data_provider(pr)

    yield name
    MemoryDataProvider.clear_cache(name)


def memory_dict_provider(empty: bool) -> Generator[str, None, None]:
    """
    Provides a memory dict provider

    Args:
        empty: whether the provider should be empty

    Returns:
        the provider name in cache entry
    """

    name = os.urandom(8).hex()
    if not empty:
        with MemoryDictProvider(name) as pr:
            fill_dict_provider(pr)

    yield name
    MemoryDictProvider.clear_cache(name)


def database_data_provider(empty: bool) -> Generator[str, None, None]:
    """
    Provides a database data provider

    Args:
        empty: whether the provider should be empty

    Returns:
        the provider name in cache entry
    """

    with tempfile.TemporaryDirectory() as dirpath:
        path = join(dirpath, "database.db")

        if not empty:
            with SQLDataProvider(SQLDriver.create(path)) as pr:
                fill_dict_provider(pr.dict_provider)
                fill_data_provider(pr)

        yield path


def database_dict_provider(empty: bool) -> Generator[str, None, None]:
    """
    Provides a database dict provider

    Args:
        empty: whether the provider should be empty

    Returns:
        the path to the database dict provider
    """

    with tempfile.TemporaryDirectory() as dirpath:
        path = join(dirpath, "database.db")

        if not empty:
            with SQLDictProvider(SQLDriver.create(path)) as pr:
                fill_dict_provider(pr)
        yield path


def fsspec_data_provider(empty: bool) -> Generator[str, None, None]:
    """
    Provides a fsspec data provider

    Args:
        empty: whether the provider should be empty

    Returns:
        the path to the fsspec data provider
    """

    with tempfile.TemporaryDirectory() as dirpath:
        if not empty:
            ZipFile(join(dirpath, "00000064.zip"), "w").close()
            z = ZipFile(join(dirpath, "00000042.zip"), "w")
            z.writestr("00000042/data", b"")
            z.writestr("00000042/data1", DATA)
            z.close()

        yield dirpath


def get_data_provider(config: ProviderConfig) -> Generator[DataProvider, None, None]:
    """
    Provide an data provider.

    Args:
        config: the configuration

    Returns:
        the data provider
    """

    if config.provider is SQLDataProvider:
        path = database_data_provider(config.empty)
        with SQLDataProvider(SQLDriver.create(next(path)), readonly=config.readonly) as pr:
            assert pr.readonly == config.readonly
            assert pr.compressed == config.compressed
            assert pr.empty == config.empty
            assert pr.dict_provider is not None
            yield pr
        for _ in path:  # pragma: no cover
            pass

    elif config.provider is RootDataProvider:
        path = fsspec_data_provider(config.empty)
        with RootDataProvider(next(path)) as pr:  # test with a local temporary directory
            # pr = RootDataProvider("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/logSE/lhcb/MC/2016/LOG/00211361/0000")
            assert pr.readonly == config.readonly  # failable
            assert pr.compressed == config.compressed
            assert pr.empty == config.empty
            assert pr.dict_provider is None
            yield pr
        for _ in path:  # pragma: no cover
            pass

    elif config.provider is FSDataProvider:
        path = filesystem_data_provider(config.empty)
        dpath = filesystem_dict_provider(config.empty)

        if config.compressed:
            with (FSDictProvider(Path(next(dpath)), readonly=config.readonly) as dp,
                  FSDataProvider(Path(next(path)), readonly=config.readonly, dict_provider=dp) as pr):
                assert dp.readonly == config.readonly
                assert dp.empty == config.empty
                assert pr.dict_provider is not None
                assert pr.readonly == config.readonly
                assert pr.compressed == config.compressed
                assert pr.empty == config.empty
                yield pr
        else:
            with FSDataProvider(Path(next(path)), readonly=config.readonly) as pr:
                assert pr.readonly == config.readonly
                assert pr.compressed == config.compressed
                assert pr.empty == config.empty
                assert pr.dict_provider is None
                yield pr
        for _ in path:  # pragma: no cover
            pass
        for _ in dpath:  # pragma: no cover
            pass

    elif config.provider is MemoryDataProvider:
        name = memory_data_provider(config.empty)
        dname = memory_dict_provider(config.empty)

        if config.compressed:
            with (MemoryDictProvider(next(dname), readonly=config.readonly) as dp,
                  MemoryDataProvider(next(name), readonly=config.readonly, dict_provider=dp) as pr):
                assert dp.readonly == config.readonly
                assert dp.empty == config.empty
                assert pr.dict_provider is not None
                assert pr.readonly == config.readonly
                assert pr.compressed == config.compressed
                assert pr.empty == config.empty
                yield pr
        else:
            with MemoryDataProvider(next(name), readonly=config.readonly) as pr:
                assert pr.readonly == config.readonly
                assert pr.compressed == config.compressed
                assert pr.empty == config.empty
                assert pr.dict_provider is None
                yield pr
        for _ in name:  # pragma: no cover
            pass
        for _ in dname:  # pragma: no cover
            pass


def get_dict_provider(config: ProviderConfig) -> Generator[DataProvider, None, None]:
    """
    Provide an dict provider.

    Args:
        config: the configuration

    Returns:
        the data provider
    """

    if config.provider is SQLDataProvider:
        path = database_dict_provider(config.empty)
        pr = SQLDictProvider(SQLDriver.create(next(path)), readonly=config.readonly)

    elif config.provider is FSDataProvider:
        path = filesystem_dict_provider(config.empty)
        pr = FSDictProvider(Path(next(path)), readonly=config.readonly)

    elif config.provider is MemoryDataProvider:
        path = memory_dict_provider(config.empty)
        pr = MemoryDictProvider(next(path), readonly=config.readonly)

    else:  # pragma: no cover
        msg = f"Invalid dictionary provider: {config.provider} ({config.empty}, {config.readonly}, {config.compressed})"
        raise ValueError(msg)

    with pr:
        assert pr.readonly == config.readonly
        assert pr.empty == config.empty
        yield pr
    for _ in path:  # pragma: no cover
        pass


@pytest.fixture(params=[c for c in configs if c.empty and not c.readonly])
def empty_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides an empty non-read-only data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if not c.empty and c.readonly])
def readonly_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides a non-empty read-only data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if c.empty and c.readonly])
def empty_readonly_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides an empty read-only data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if not c.empty and not c.readonly])
def data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides a non-empty non-read-only data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if c.empty])
def any_empty_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides a empty data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if not c.empty])
def any_non_empty_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides a non-empty data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if c.readonly])
def any_readonly_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides a readonly data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if not c.readonly])
def any_non_readonly_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides a non-readonly data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=configs)
def any_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides any data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if c.compressed and not c.readonly])
def compressed_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides a compressed non-read-only data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if c.compressed and not c.readonly and c.empty])
def empty_compressed_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides an empty, compressed, non-read-only data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if not c.compressed and not c.readonly])
def decompressed_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides a decompressed non-read-only data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


@pytest.fixture(params=[c for c in configs if not c.compressed and not c.readonly and c.empty])
def empty_decompressed_data_provider(request) -> Generator[DataProvider, None, None]:
    """
    Provides an empty, decompressed, non-read-only data provider

    Returns:
        the data provider
    """

    yield from get_data_provider(request.param)


alt_data_provider = data_provider
alt_empty_data_provider = empty_data_provider
alt_readonly_data_provider = readonly_data_provider
alt_empty_readonly_data_provider = empty_readonly_data_provider
alt_any_empty_data_provider = any_empty_data_provider
alt_any_readonly_data_provider = any_readonly_data_provider
alt_any_non_empty_data_provider = any_non_empty_data_provider
alt_any_non_readonly_data_provider = any_non_readonly_data_provider
alt_any_data_provider = any_data_provider
alt_compressed_data_provider = compressed_data_provider
alt_decompressed_data_provider = decompressed_data_provider
alt_empty_compressed_data_provider = empty_compressed_data_provider
alt_empty_decompressed_data_provider = empty_decompressed_data_provider


@pytest.fixture(params=[c for c in dict_configs if c.empty and not c.readonly])
def empty_dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides an empty dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


@pytest.fixture(params=[c for c in dict_configs if not c.empty and c.readonly])
def readonly_dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides a non-empty read-only dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


@pytest.fixture(params=[c for c in dict_configs if c.empty and c.readonly])
def empty_readonly_dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides an empty read-only dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


@pytest.fixture(params=[c for c in dict_configs if not c.empty and not c.readonly])
def dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides a non-empty non-read-only dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


@pytest.fixture(params=[c for c in dict_configs if c.empty])
def any_empty_dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides a empty dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


@pytest.fixture(params=[c for c in dict_configs if not c.empty])
def any_non_empty_dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides a non-empty dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


@pytest.fixture(params=[c for c in dict_configs if c.readonly])
def any_readonly_dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides a readonly dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


@pytest.fixture(params=[c for c in dict_configs if not c.readonly])
def any_non_readonly_dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides a non-readonly dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


@pytest.fixture(params=dict_configs)
def any_dict_provider(request) -> Generator[DictProvider, None, None]:
    """
    Provides any dictionary provider

    Returns:
        the dictionary provider
    """

    yield from get_dict_provider(request.param)


alt_dict_provider = dict_provider
alt_empty_dict_provider = empty_dict_provider
alt_readonly_dict_provider = readonly_dict_provider
alt_empty_readonly_dict_provider = empty_readonly_dict_provider
alt_any_empty_dict_provider = any_empty_dict_provider
alt_any_readonly_dict_provider = any_readonly_dict_provider
alt_any_non_empty_dict_provider = any_non_empty_dict_provider
alt_any_non_readonly_dict_provider = any_non_readonly_dict_provider
alt_any_dict_provider = any_dict_provider


@pytest.fixture
def dict_entry(empty_dict_provider: DictProvider, dict_data: tuple[bytes, int, int]) -> DictEntry:
    """Provides a dictionary entry.

    Returns:
        the dictionary entry
    """

    return empty_dict_provider.add("test", dict_data[0], dict_data[2])


@pytest.fixture
def compressor() -> ZstandardCompressor:
    """
    Provides a compressor

    Returns:
        the compressor
    """

    return ZstandardCompressor()


@pytest.fixture
def decompressor() -> ZstandardDecompressor:
    """
    Provides a decompressor

    Returns:
        the decompressor
    """

    return ZstandardDecompressor()


@pytest.fixture
def trainer() -> ZstandardTrainer:
    """
    Provides a trainer

    Returns:
        the trainer
    """

    return ZstandardTrainer()


@pytest.fixture(params=[ZstandardCompressor, ZstandardDecompressor, ZstandardTrainer])
def processor(request) -> ZstandardCompressor:
    """
    Provides a processor

    Returns:
        the processor
    """

    return request.param()


@pytest.fixture
def compressor_with_dict(dict_entry: DictEntry) -> ZstandardCompressor:
    """
    Provides a compressor with a dictionary

    Returns:
        the compressor
    """

    return ZstandardCompressor(dict_entry=dict_entry)


@pytest.fixture(params=[ZstandardCompressor, ZstandardDecompressor, ZstandardTrainer])
def processor_with_dict(request, dict_entry: DictEntry) -> ZstandardCompressor:
    """
    Provides a processor with a dictionary

    Returns:
        the processor
    """

    return request.param(dict_entry=dict_entry)


@pytest.fixture
def data_sample() -> tuple[tuple[bytes, ...], ...]:
    """
    Provides a data sample

    Returns:
        the data sample
    """

    data0 = ()  # none
    data1 = b"test",  # single
    data2 = b"",  # single empty
    data3 = b"test", b"test2", b"test3"  # many all non-empty
    data4 = b"test", b"", b"test2", b"", b"test3"  # many some empty
    data5 = b"", b"", b""  # many all empty

    return data0, data1, data2, data3, data4, data5


@pytest.fixture
def task_queue(dict_provider: DictProvider) -> TaskQueue:
    """
    Provides a task queue

    Returns:
        the task queue
    """

    return TaskQueue(1, dict_provider)
