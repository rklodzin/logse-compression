"""Implementation of the data provider layer for fsspec supported (remote) filesystems.

This module provides the fsspec implementation of FSspec Driver class.

Classes:
    - RootDriver: FSspec driver implementation.
"""

import fsspec
from fsspec.asyn import AsyncFileSystem

__all__ = ("RootDriver",)


class RootDriver:
    """[Internal] FSspec root driver."""

    __slots__ = (
        "_fs",
        "_path",
        "_root_url",
    )

    def __init__(self, root_url: str) -> None:
        """Initialize the FSspec driver.

        Args:
            root_url: the url, including the protocol
        """
        self._root_url = root_url
        fs, path = fsspec.core.url_to_fs(root_url)
        self._fs: AsyncFileSystem = fs
        self._path: str = path

    @property
    def fs(self) -> AsyncFileSystem:
        """Get the filesystem.

        Returns:
            the filesystem
        """
        return self._fs

    @property
    def path(self) -> str:
        """Get the path.

        Returns:
            the path
        """
        return self._path
