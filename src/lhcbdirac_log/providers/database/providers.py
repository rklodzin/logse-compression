"""Implementation of the data provider layer for SQL databases.

This module provides the SQLite database implementation of providers classes for the data provider layer.

Classes:
    - SQLDataProvider: SQL Database data provider implementation.
    - SQLDictProvider: SQL Database dictionary provider implementation.
"""

from collections.abc import Generator
from typing import override

from ...config import DEFAULT_CONFIG, Config
from ..base import DataProvider, DictExistsError, DictProvider, JobNotExistsError
from .accessors import SQLDictEntry, SQLJobEntry
from .driver import SQLDriver

__all__ = (
    "SQLDataProvider",
    "SQLDictProvider",
)


class SQLDictProvider(DictProvider[SQLDictEntry]):
    """SQL dictionary provider implementation."""

    __slots__ = ("_driver",)

    @override
    def close(self) -> None:
        self._driver.close()

    @property
    def driver(self) -> SQLDriver:
        """Get the SQL driver.

        Returns:
            the SQL driver
        """
        return self._driver

    def __init__(self, driver: SQLDriver, config: Config = DEFAULT_CONFIG, *, readonly: bool = False) -> None:
        """Initialize the dictionary provider.

        Args:
            driver: the SQL driver
            config: the configuration to use for precomputing the dictionaries (default: DEFAULT_CONFIG)
            readonly: whether the provider is read-only (default: False)
        """
        self._driver = driver
        super().__init__(config, readonly=readonly)

    @override
    def _load_invalid(self) -> set[str]:
        return self._driver.get_invalid_dicts()

    @override
    def _mark_invalid(self, name: str) -> None:
        self._driver.save_invalid_dict(name)

    @override
    def _load(self, name: str) -> SQLDictEntry:
        if not self._driver.check_dict(name):
            raise ValueError(name)

        return SQLDictEntry(self._driver, name, self._config)

    @override
    def _add(self, name: str, data: bytes, zstd_id: int) -> SQLDictEntry:
        if self._driver.check_dict(name):
            raise DictExistsError(name)

        return SQLDictEntry(self._driver, name, self._config, data, zstd_id)

    @override
    def _iter_all(self) -> Generator[str, None, None]:
        yield from self._driver.get_dicts()

    @override
    def _delete(self, name: str) -> None:
        self._driver.delete_dict(name)

    @override
    @property
    def size(self) -> int:
        return self._driver.get_all_dict_size()


class SQLDataProvider(DataProvider[SQLJobEntry]):
    """SQL data provider implementation.

    Only supports the new compressed (Zstandard) format.
    """

    __slots__ = ("_driver",)

    @override
    def open(self) -> None:
        self._dict_provider.open()

    @override
    def close(self) -> None:
        self._dict_provider.close()

    @property
    def driver(self) -> SQLDriver:
        """Get the SQL driver.

        Returns:
            the SQL driver
        """
        return self._driver

    def __init__(self, driver: SQLDriver | None = None, dict_provider: SQLDictProvider | None = None, *, readonly: bool = False) -> None:
        """Initialize the data provider.

        Args:
            driver: the SQL driver, use the `dict_provider`'s driver if None, or an in-memory SQLite database if `dict_provider` is None too
            dict_provider: the dictionary provider, if None, a new one will be created
            readonly: indicate weather the provider is read-only or not (default: False)

        Raises:
            ValueError: if the specified dictionary provider uses a different SQL driver
        """
        if dict_provider is None:
            if driver is None:
                driver = SQLDriver.create()

            dict_provider = SQLDictProvider(driver, readonly=readonly)

        elif driver is None:
            driver = dict_provider.driver

        elif dict_provider.driver is not driver:
            msg = "The dictionary provider must use the same SQL driver"
            raise ValueError(msg)

        self._driver = driver
        super().__init__(dict_provider, readonly=readonly)

    @override
    def _get(self, job: int, *, create: bool = False) -> SQLJobEntry:
        return SQLJobEntry(self._driver, job, readonly=self._readonly, create=create)

    @override
    def _create(self, job: int, *, exists_ok: bool = False) -> SQLJobEntry:
        return SQLJobEntry(self._driver, job, readonly=self._readonly, exists_ok=exists_ok)

    @override
    def jobs(self) -> Generator[int, None, None]:
        yield from self._driver.get_jobs()

    @override
    def _delete(self, job: int, *, force: bool = False) -> None:
        if not self._driver.check_job(job):
            raise JobNotExistsError(job)

        self._driver.delete_job(job, force=force)

    @override
    @property
    def data_size(self) -> int:  # optimizes the default implementation
        return self._driver.get_all_data_size()

    @override
    @property
    def size(self) -> int:  # optimizes the default implementation
        return self._driver.get_db_size()
