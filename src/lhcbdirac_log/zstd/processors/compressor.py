"""This module provides the Zstandard compressor wrapper class.

Classes:
    - ZstandardCompressor: Zstandard compressor wrapper class.
"""

from collections.abc import Iterator
from typing import BinaryIO, override

from zstandard import ZstdCompressor

from .processor import ZstandardProcessor

__all__ = ("ZstandardCompressor",)


class ZstandardCompressor(ZstandardProcessor[ZstdCompressor]):
    """Wrapper for Zstandard compressor.

    Performs data compression using Zstandard, featuring extra functionality:
        - zstd-context management (auto compressor reinstantiation)
        - switchable dictionary

    Notes:
        - not thread-safe
        - dictionary switching requires reinstantiation of the internal context processor (slow)
    """

    __slots__ = ()

    @override
    def _new(self) -> ZstdCompressor:
        return ZstdCompressor(dict_data=None if self._dict is None else self._dict.dict, compression_params=self._config.params)

    @override
    def process(self, *data: bytes | bytearray | memoryview) -> bytes | Iterator[bytes]:
        if len(data) == 1:
            return bytes(self.processor.compress(data[0])) if data[0] else b""
        return (bytes(self.processor.compress(i)) if i else b"" for i in data)

    @override
    def process_stream(
        self,
        instream: BinaryIO,
        outstream: BinaryIO,
        insize: int = -1,
    ) -> tuple[int, int]:
        r: tuple[int, int] = self.processor.copy_stream(
            instream,
            outstream,
            insize,
            self._config.stream_read_buffer,
            self._config.stream_write_buffer,
        )
        return r  # fix mypy error on untyped zstdlib

    compress = process
    compress_stream = process_stream
