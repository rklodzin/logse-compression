"""This module provides the TaskQueue class for the zstd module.

This class is used to manage the task queue and the training queues.

Classes:
    - TaskQueue: a proxy queue managing tasks and their dispatching to training queues, if necessary.
"""

from __future__ import annotations

from queue import Queue
from threading import RLock
from typing import override

from ...config import DEFAULT_CONFIG, Config
from ...providers import DictNotExistsError, DictProvider
from ..processors import ZstandardTrainer
from .task import ProcessingTask, Task, TrainingTask

__all__ = ("TaskQueue",)


class TaskQueue(Queue[Task | None]):
    """[Internal] A proxy queue managing tasks and their dispatching to training queues, if necessary.

    Just putting tasks and it will automatically put training tasks, if necessary, when enough tasks
    are available as a training sample. Then, the trainer will requeue the tasks to the real task queue
    to be finally processed by the workers.
    """

    __slots__ = (
        "_lock",
        "_dict_provider",
        "_available",
        "_pending_tasks",
        "_trainpool",
        "_train_queue_size",
        "_pushed",
        "_rejected",
        "_training",
        "_config",
    )

    def __init__(
        self,
        workers: int,
        dict_provider: DictProvider,
        config: Config = DEFAULT_CONFIG,
    ) -> None:
        """Initialize the task queue.

        Args:
            workers: the number of workers available or 0 to disable the training system
            dict_provider: the dict provider
            config: the configuration to use for training (default: DEFAULT_CONFIG)
        """
        super().__init__()

        self._trainpool = Queue[ZstandardTrainer]()
        self._pending_tasks: dict[str, list[ProcessingTask]] = {}
        self._dict_provider = dict_provider
        self._train_queue_size = config.train_queue_size
        self._pushed = set[str]()  # names of dict queues pushed for training
        self._available = set[str]()  # names of trained dicts (including failure)
        self._lock = RLock()
        self._rejected = list[tuple[Task, Exception]]()  # rejected tasks
        self._training = workers > 0
        self._config = config

        for _ in range(workers):
            self._trainpool.put(ZstandardTrainer(config))

    @property
    def rejected(self) -> list[tuple[Task, Exception]]:
        """Get the rejected tasks.

        Rejected tasks are tasks that failed to be processed.

        Returns:
            the rejected tasks
        """
        return self._rejected

    def _mark_pushed(self, dict_name: str) -> None:
        """[Internal] Mark a dict as pushed for training.

        Args:
            dict_name: the dict name
        """
        self._pushed.add(dict_name)

    def _mark_available(self, dict_name: str) -> None:
        """[Internal] Mark a dict as available (trained or even invalid).

        Args:
            dict_name: the dict name
        """
        self._available.add(dict_name)

    def reject(self, task: Task, exc: Exception) -> None:
        """Mark a task as rejected.

        Args:
            task: the rejected task
            exc: the exception raised
        """
        self._rejected.append((task, exc))

    def _requeue(self, name: str) -> None:
        """[Internal] Requeue ready processing tasks to the main queue, from a finished training task.

        Args:
            name: the training task name

        Raises:
            KeyError: if there is no pending task with the given name (should not happen)
        """
        with self._lock:
            tasks = self._pending_tasks.pop(name)

            self._mark_available(name)

            while tasks:
                super().put(tasks.pop())

    @override
    def put(self, item: Task | None, block: bool = True, timeout: float | None = None) -> None:
        """Put a task in the task queue.

        Args:
            item: the task
            block: if True, blocks until the task is added (default: True)
            timeout: the timeout in seconds (default: None)

        Notes:
            - if the task is a processing task, and the training system is enabled,
              it will be added to the training queue if necessary dict is not available
        """
        if self._training and isinstance(item, ProcessingTask):
            with self._lock:
                dict_name = item.dict_name

                if dict_name not in self._available:
                    try:  # Check if the dict exists
                        self._dict_provider.get(dict_name, invalid_ok=True, missing_ok=False)
                        self._mark_available(dict_name)

                    except DictNotExistsError:  # Dict not found: add the task to the trainer
                        tasks = self._pending_tasks.setdefault(dict_name, [])
                        tasks.append(item)

                        if len(tasks) == self._train_queue_size:  # == to not restart a running training
                            self._mark_pushed(dict_name)
                            d = TrainingTask(self._trainpool, self._requeue, self._dict_provider, dict_name, tasks)
                            super().put(d, block, timeout)
                        return

                super().put(item, block, timeout)
        else:
            super().put(item, block, timeout)

    def flush(self) -> None:
        """Flush all remaining incomplete training queues to training tasks.

        Must be called after all tasks are added to the queue.

        Notes:
            - do nothing if the training system is disabled
        """
        with self._lock:
            for name in set(self._pending_tasks) - self._pushed:
                self._mark_pushed(name)
                super().put(TrainingTask(self._trainpool, self._requeue, self._dict_provider, name, self._pending_tasks[name]))
