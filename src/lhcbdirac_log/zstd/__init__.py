"""This package provides easy zstd processing and a thread pool system for parallel processing.

Classes:
    - CompressorPool: a thread pool managing data compressors.
    - DecompressorPool: a thread pool managing data decompressors.
    - ThreadPool: an abstract thread pool managing workers and their tasks.
    - ZstandardCompressor: a data compressor.
    - ZstandardDecompressor: a data decompressor.
    - ZstandardProcessor: an abstract data processor.
    - ZstandardTrainer: a dictionary trainer.
"""

from .pool import CompressorPool, DecompressorPool, ThreadPool
from .processors import ZstandardCompressor, ZstandardDecompressor, ZstandardProcessor, ZstandardTrainer

__all__ = (
    "CompressorPool",
    "DecompressorPool",
    "ThreadPool",
    "ZstandardCompressor",
    "ZstandardDecompressor",
    "ZstandardProcessor",
    "ZstandardTrainer",
)
