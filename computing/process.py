"""
Grid job script
"""
import json
import time
import os

from lhcbdirac_log import CompressorPool
from lhcbdirac_log.config import Config, DEFAULT_CONFIG
from lhcbdirac_log.providers.fsspec import RootDataProvider
# from lhcbdirac_log.providers.fsspec.driver import RootDriver
from lhcbdirac_log.providers.database import SQLDriver, SQLDataProvider
from lhcbdirac_log.zstd.pool import ProcessingTask

if __name__ == '__main__':
    path = os.environ.get("LOG_SRC_PATH")
    workers = int(os.environ.get("LOG_PROCESSING_WORKER", 1))
    token = os.environ.get("TOKEN")
    prod, subprod = path.split("/")[-2:]

    print("start processing", path, "with", workers, "workers to", f'{subprod}.db')

    DEFAULT_CONFIG.train_threads = workers
    DEFAULT_CONFIG.train_notifications = 1

    with RootDataProvider(f"root://eoslhcb.cern.ch/{path}") as src:

        with SQLDataProvider(SQLDriver.create(f'{subprod}.db')) as dst:

            pool = CompressorPool(dst.dict_provider, workers=workers)
            st = time.time()

            for job in src:
                out = dst.create(job.job)
                out.update_info(job.info)

                for data in job:
                    pool.add(data, out.create(data.name))

            pool.flush()
            pool.stop()
            pool.join()

            output = {
                "time": time.time() - st,  # processing time (s)
                "rejected": {}  # rejected data (name: error)
            }

            print("done in", output["time"], "s")

            if pool.rejected:
                print(f"rejected data ({len(pool.rejected)}):")

                for t, e in pool.rejected:
                    if isinstance(t, ProcessingTask):
                        output["rejected"][name := f"{t.data_in.job:08}/{t.data_in.name}"] = str(e)
                        print(f"'{name}': {e}")
                    else:
                        output["rejected"][str(t)] = str(e)
                        print(t, ":", e)
            else:
                print("no rejected data")

            output["size_in"] = szi = src.size  # size (zip format)
            output["size_out"] = szo = dst.size  # size (db format)
            output["ratio"] = ratio = szi / szo  # compression ratio (zip/db)
            output["saved"] = saved = szi - szo  # saved storage size (zip - db)

            print(f" input storage size: {szi:,} B")
            print(f"output storage size: {szo:,} B")
            print(f"compression ratio: {ratio:.2} (saved storage size: {saved:,} B)")

    print("uploading to EOS...")

    r = os.system(f'xrdcp {subprod}.db "root://eoslhcb.cern.ch/{path.replace("logSE", "logArchive")}.db?xrd.wantprot=unix&authz={token}"')
    output["uploaded"] = not r  # uploaded to EOS (true if successful)

    json.dump(output, open(f'output.json', 'w'))

    print("code", r)

    print("exit")
    exit(r)
