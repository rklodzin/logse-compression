"""
This example demonstrates how to copy a sub-production to another.
"""

from lhcbdirac_log.providers.filesystem import FSDataProvider


def main() -> None:
    """
    This example demonstrates how to copy a sub-production to another.
    """

    with (FSDataProvider("./prod_1") as src,  # open the source
          FSDataProvider("./prod_2") as dst):  # open the destination

        # manually copying the jobs and theirs data
        for job in src:  # iterate over the jobs in the source
            out = dst.create(job.job)  # create a new job in the destination
            out.update_info(job.info)

            for data in job:  # iterate over the data in the job

                with (out.create(data.name).writer() as writer,  # create the destination data
                      data.reader() as reader):  # read the data from the source
                    writer.write(reader.read())  # write the data to the destination

        # automatically copying
        src.transfer(dst)  # transfer the source to the destination (also transfers zstd-dict for new format)

        # automatically limited copying
        src.transfer(dst, limit=500)  # transfer 500 jobs from the source to the destination


if __name__ == '__main__':
    main()
