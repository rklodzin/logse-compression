"""This package provides zstd parallel processing capability.

Classes:
    - CompressorPool: a thread pool managing data compressors.
    - DecompressorPool: a thread pool managing data decompressors.
    - ThreadPool: an abstract thread pool managing workers and their tasks.
    - ProcessorPool: a pool of zstd processors, managing their assignments.
    - TaskQueue: a proxy queue managing tasks and their dispatching to training queues, if necessary.
    - Task: a task to process data.
    - TrainingTask: a task to train a dictionary.
    - ProcessingTask: a task to process data.
    - Worker: a worker processing tasks.
"""

from .pool import CompressorPool, DecompressorPool, ThreadPool
from .processor import ProcessorPool
from .queue import TaskQueue
from .task import ProcessingTask, Task, TrainingTask
from .worker import Worker

__all__ = (
    "CompressorPool",
    "DecompressorPool",
    "ThreadPool",
    "ProcessorPool",
    "TaskQueue",
    "Task",
    "TrainingTask",
    "ProcessingTask",
    "Worker",
)
