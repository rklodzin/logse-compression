"""
Test the zstd task module.
"""

import pytest

from lhcbdirac_log import ZstandardCompressor, ZstandardDecompressor, DataProvider
from lhcbdirac_log.zstd.pool import ProcessorPool, ProcessingTask, TrainingTask, TaskQueue


class TestTask:
    """Test the task module."""

    def test_processing_task(self,
                             compressed_data_provider: DataProvider,
                             decompressed_data_provider: DataProvider,
                             all_dict_data: tuple[tuple[bytes, int, int], ...]
                             ) -> None:
        """Test the processing task class."""

        cpool = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 1, 1)
        dpool = ProcessorPool[ZstandardDecompressor](ZstandardDecompressor, 1, 1)

        compressed_data_provider.dict_provider.mark_invalid("fx")

        for i in range(2):

            cj = compressed_data_provider.create(99)
            dj = decompressed_data_provider.create(99)

            c1 = cj.create("f1")
            c2 = cj.create("f2")
            c3 = cj.create("f3")

            d1 = dj.create("f1")
            d2 = dj.create("f2")
            d3 = dj.create("f3")

            z1 = dj.create("z1")
            z2 = dj.create("z2")

            with d1.writer() as w:
                w.write(x1 := b"")

            with d2.writer() as w:
                w.write(x2 := b"hello world" * 8_000_000)

            with d3.writer() as w:
                w.write(b"a" * compressed_data_provider.dict_provider.config.max_data_size)

            de = compressed_data_provider.dict_provider.get(d1.dict_name, invalid_ok=True, missing_ok=True)

            if i == 0:
                assert de is None
            else:
                assert de is not None

            config = compressed_data_provider.dict_provider.config
            pt = ProcessingTask(cpool, d1, c1, compressed_data_provider.dict_provider, config)
            assert pt.data_in is d1
            assert pt.data_out is c1

            pt.run()
            ProcessingTask(cpool, d2, c2, compressed_data_provider.dict_provider, config).run()

            with pytest.raises(ValueError):
                ProcessingTask(cpool, d3, c3, compressed_data_provider.dict_provider, config).run()

            assert len([i for u in cpool._free.values() for i in u]) == 1  # processor is free

            ProcessingTask(dpool, c1, z1, compressed_data_provider.dict_provider, config).run()
            ProcessingTask(dpool, c2, z2, compressed_data_provider.dict_provider, config).run()

            assert len([i for u in dpool._free.values() for i in u]) == 1  # processor is free

            assert not c3.exists

            with z1.reader() as r:
                assert r.read() == x1

            with z2.reader() as r:
                assert r.read() == x2

            # test no free processor (should never happen)
            tr = cpool.get_any()

            with pytest.raises(RuntimeError):
                ProcessingTask(cpool, d1, c1, compressed_data_provider.dict_provider, config).run()

            cpool.release(tr)

            # retry all tests with valid dict
            if i == 0:
                compressed_data_provider.clear(force=True)
                decompressed_data_provider.clear(force=True)
                compressed_data_provider.dict_provider.clear()
                compressed_data_provider.dict_provider._invalid.clear()
                compressed_data_provider.dict_provider.add("fx", all_dict_data[0][0], all_dict_data[0][2])

    def test_training_task(self,
                           compressed_data_provider: DataProvider,
                           decompressed_data_provider: DataProvider,
                           all_dict_data: tuple[tuple[bytes, int, int], ...]
                           ) -> None:
        """Test the training task class."""

        r = []

        compressed_data_provider.clear(force=True)
        compressed_data_provider.dict_provider.clear()

        j = decompressed_data_provider.create(99)
        d1 = j.create("f1")
        d2 = j.create("f2")
        d3 = j.create("f3")
        d4 = j.create("f4")
        d5 = j.create("f5")
        d6 = j.create("f6")
        d7 = j.create("f7")
        d8 = j.create("f8")

        with d1.writer() as w:
            w.write(b"")

        with d2.writer() as w:
            w.write(b"==+==")

        with d3.writer() as w:
            w.write(b"==+==-")

        with d4.writer() as w:
            w.write(b"==-==-")

        with d5.writer() as w:
            w.write(b"==-==-")

        with d6.writer() as w:
            w.write(b"==-==+")

        with d7.writer() as w:
            w.write(b"==/==")

        with d8.writer() as w:
            w.write(b"====++")

        task_queue = TaskQueue(1, compressed_data_provider.dict_provider)
        cpool = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 1, 1)

        config = compressed_data_provider.dict_provider.config

        TrainingTask(task_queue._trainpool,
                     lambda x: r.append(x),
                     compressed_data_provider.dict_provider,
                     "fx",
                     [
                            ProcessingTask(cpool, d1, d1, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d2, d2, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d3, d3, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d4, d4, compressed_data_provider.dict_provider, config),
                     ]).run()

        assert r == ["fx"]
        assert task_queue._trainpool.qsize() == 1
        assert len(compressed_data_provider.dict_provider) == 0
        assert compressed_data_provider.dict_provider._invalid == {"fx"}

        compressed_data_provider.dict_provider._invalid.clear()

        TrainingTask(task_queue._trainpool,
                     lambda x: r.append(x),
                     compressed_data_provider.dict_provider,
                     "fx",
                     [
                            ProcessingTask(cpool, d1, d1, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d2, d2, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d3, d3, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d4, d4, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d5, d5, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d6, d6, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d7, d7, compressed_data_provider.dict_provider, config),
                            ProcessingTask(cpool, d8, d8, compressed_data_provider.dict_provider, config),
                     ]).run()

        assert r == ["fx"] * 2
        assert task_queue._trainpool.qsize() == 1
        assert len(compressed_data_provider.dict_provider) == 1
        assert len(compressed_data_provider.dict_provider._invalid) == 0
