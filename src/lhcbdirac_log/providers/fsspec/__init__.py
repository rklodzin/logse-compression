"""Implementation of the data provider layer for fsspec supported (remote) filesystems.

This implementation only supports the old uncompressed (means ZIP) format.
Thus, Zstandard related features are not implemented, including the dictionary provider.

Since the purpose of this library is to migrate from the old format to the new one,
creating new job/data entries with this provider is not allowed nor implemented.
Thus, this implementation only supports the read-only mode.

Classes:
    - RootDataEntry: fsspec data entry implementation
    - RootJobEntry: fsspec job entry implementation
    - RootDataProvider: fsspec data provider implementation

Notes:
    - classes are prefixed with `Root` because these are mainly
      intended to be used with the CERN's EOS filesystem using the XRootD protocol
"""

from .accessors import RootDataEntry, RootJobEntry
from .providers import RootDataProvider

__all__ = (
    "RootDataEntry",
    "RootDataProvider",
    "RootJobEntry",
)
