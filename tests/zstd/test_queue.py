"""
Test the task queue module.
"""

from lhcbdirac_log import ZstandardCompressor, DataProvider, DictProvider
from lhcbdirac_log.zstd.pool import TaskQueue, ProcessingTask, ProcessorPool, TrainingTask


class TestTaskQueue:
    def test_queue_valid(self,
                         task_queue: TaskQueue,
                         decompressed_data_provider: DataProvider,
                         dict_provider: DictProvider):

        assert task_queue.rejected == []
        t = task_queue._train_queue_size
        pool = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 1, 1)

        ds = []
        ts = []

        j = decompressed_data_provider.create(99)

        assert task_queue.qsize() == 0  # no tasks

        for i in range(t - 1):
            ds.append(d := j.create(f"d{i:04}"))
            ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        assert task_queue.qsize() == 0  # pending tasks in training queue

        ds.append(d := j.create(f"d{t - 1:04}"))
        ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
        task_queue.put(p)

        assert task_queue.qsize() == 1  # training task

        tt = task_queue.get()
        assert isinstance(tt, TrainingTask)
        assert tt._tasks == ts
        assert tt._name == "dxxxx"
        task_queue.put(tt)

        for i in range(t, t + 10):  # more tasks
            ds.append(d := j.create(f"d{i:04}"))
            ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        assert task_queue.qsize() == 1  # training task

        task_queue.get()
        dict_provider.add("dxxxx", b"", 42)  # mock training
        tt._callback(tt._name)

        assert task_queue.qsize() == t + 10  # processing tasks

        for i in range(t + 10, t + 20):  # more tasks
            ds.append(d := j.create(f"d{i:04}"))
            ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        assert task_queue.qsize() == t + 20  # processing tasks

    def test_queue_invalid(self,
                           task_queue: TaskQueue,
                           decompressed_data_provider: DataProvider,
                           dict_provider: DictProvider):

        assert task_queue.rejected == []
        t = task_queue._train_queue_size
        pool = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 1, 1)

        ds = []
        ts = []

        j = decompressed_data_provider.create(99)

        assert task_queue.qsize() == 0  # no tasks

        for i in range(t - 1):
            ds.append(d := j.create(f"d{i:04}"))
            ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        assert task_queue.qsize() == 0  # pending tasks in training queue

        ds.append(d := j.create(f"d{t - 1:04}"))
        ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
        task_queue.put(p)

        assert task_queue.qsize() == 1  # training task

        tt = task_queue.get()
        assert isinstance(tt, TrainingTask)
        assert tt._tasks == ts
        assert tt._name == "dxxxx"
        task_queue.put(tt)

        for i in range(t, t + 10):  # more tasks
            ds.append(d := j.create(f"d{i:04}"))
            ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        assert tt._tasks == ts  # contains new tasks too
        assert task_queue.qsize() == 1  # training task

        task_queue.get()
        dict_provider.mark_invalid('dxxxx')  # mock (failed) training
        tt._callback(tt._name)

        assert task_queue.qsize() == t + 10  # processing tasks

        for i in range(t + 10, t + 20):  # more tasks
            ds.append(d := j.create(f"d{i:04}"))
            ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        assert task_queue.qsize() == t + 20  # processing tasks

    def test_queue_multi(self,
                         task_queue: TaskQueue,
                         decompressed_data_provider: DataProvider,
                         dict_provider: DictProvider):

        assert task_queue.rejected == []
        t = task_queue._train_queue_size
        pool = ProcessorPool[ZstandardCompressor](ZstandardCompressor, 1, 1)

        ds = []
        ts = []
        ats = []
        bts = []

        j = decompressed_data_provider.create(99)
        dict_provider.mark_invalid('ixxxx')

        assert task_queue.qsize() == 0  # no tasks

        for i in range(t - 1):
            ds.append(d := j.create(f"d{i:04}"))
            ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        for i in range(10):
            ds.append(d := j.create(f"i{i:04}"))
            p = ProcessingTask(pool, d, d, dict_provider, dict_provider.config)
            task_queue.put(p)

        assert task_queue.qsize() == 10  # pending tasks in training queue + 10 processing tasks

        ds.append(d := j.create(f"d{t - 1:04}"))
        ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
        task_queue.put(p)

        assert task_queue.qsize() == 11  # training task

        for i in range(t + 100):
            ds.append(d := j.create(f"a{i:04}"))
            ats.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        for i in range(10, 15):
            ds.append(d := j.create(f"i{i:04}"))
            p = ProcessingTask(pool, d, d, dict_provider, dict_provider.config)
            task_queue.put(p)

        for i in range(t + 50):
            ds.append(d := j.create(f"b{i:04}"))
            bts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        assert task_queue.qsize() == 18  # training task

        q = []
        for i in range(10):
            q.append(task_queue.get())
            assert isinstance(q[-1], ProcessingTask)

        q.append(tt := task_queue.get())
        assert isinstance(tt, TrainingTask)
        assert tt._tasks == ts
        assert tt._name == "dxxxx"

        q.append(tt := task_queue.get())
        assert isinstance(tt, TrainingTask)
        assert tt._tasks == ats
        assert tt._name == "axxxx"

        for i in range(5):
            q.append(task_queue.get())
            assert isinstance(q[-1], ProcessingTask)

        q.append(tt := task_queue.get())
        assert isinstance(tt, TrainingTask)
        assert tt._tasks == bts
        assert tt._name == "bxxxx"

        for i in range(t, t + 10):  # more tasks
            ds.append(d := j.create(f"d{i:04}"))
            ts.append(p := ProcessingTask(pool, d, d, dict_provider, dict_provider.config))
            task_queue.put(p)

        assert task_queue.qsize() == 0  # training task

        task_queue.flush()  # no effect here

        assert task_queue.qsize() == 0  # training task

        task_queue._pushed.remove('axxxx')
        task_queue._pushed.remove('bxxxx')

        for i in range(t - 10):
            ds.append(d := j.create(f"c{i:04}"))
            p = ProcessingTask(pool, d, d, dict_provider, dict_provider.config)
            task_queue.put(p)

        assert task_queue.qsize() == 0  # training task

        task_queue.flush()

        assert task_queue.qsize() == 3  # training task

        assert len(task_queue.rejected) == 0
        task_queue.reject(task_queue.get(), ValueError("test"))

        assert task_queue.qsize() == 2
        assert len(task_queue.rejected) == 1
