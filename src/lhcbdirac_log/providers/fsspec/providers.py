"""Implementation of the data provider layer for fsspec supported (remote) filesystems.

This module provides the fsspec implementation of providers classes for the data provider layer.

Classes:
    - RootDataProvider: Fsspec data provider implementation.
"""

from collections.abc import Generator
from typing import override

from ..base import DataProvider
from .accessors import RootJobEntry
from .driver import RootDriver

__all__ = ("RootDataProvider",)


class RootDataProvider(DataProvider[RootJobEntry]):
    """FSspec data provider implementation.

    Only supports the old uncompressed (means ZIP) format.
    Only supports read-only mode.
    """

    __slots__ = ("_driver",)

    @property
    def driver(self) -> RootDriver:
        """Get the root driver.

        Returns:
            the root driver
        """
        return self._driver

    def __init__(self, url: str) -> None:
        """Initialize the data provider.

        Args:
            url: the root url
        """
        self._driver = RootDriver(url)
        super().__init__(readonly=True)

    @override
    def _get(self, job: int, *, create: bool = False) -> RootJobEntry:
        return RootJobEntry(self._driver, job)

    @override
    def _create(self, job: int, *, exists_ok: bool = False) -> RootJobEntry:  # pragma: no cover
        pass

    @override
    def jobs(self) -> Generator[int, None, None]:
        return (int(i[-12:-4]) for i in self._driver.fs.listdir(self._driver.path, detail=False) if i[-12:-4].isdecimal())

    @override
    def _delete(self, job: int, *, force: bool = False) -> None:  # pragma: no cover
        pass
