"""
This example demontrates how to do a full migration to the old ZIP format to the new Zstandard+SQLite format of a sub-production.
"""

from lhcbdirac_log import CompressorPool
from lhcbdirac_log.providers.database import SQLDriver, SQLDataProvider
from lhcbdirac_log.providers.fsspec import RootDataProvider


def main() -> None:
    """
    This example demontrates how to do a full migration to the old ZIP format to the new Zstandard+SQLite format of a sub-production.
    """

    url = "root://xxxx.xxxx.xxxx//xxxxxxxx"  # the URL targetting a distant input sub-production
    path = "~/xxxxxxxx/xxxxxxxx_xxxx.db"  # the path to the local output SQLite database file

    with RootDataProvider(url) as src:

        with SQLDataProvider(SQLDriver.create(path)) as dst:

            pool = CompressorPool(dst.dict_provider)

            for job in src:
                out = dst.create(job.job)  # create a new job
                out.update_info(job.info)  # update the job metadata

                for data in job:  # iterate over the data
                    pool.add(data, out.create(data.name))  # add the data to the pool

            pool.flush()  # flush the remaining training queues
            pool.stop()  # ask the pool to stop
            pool.join()  # join the pool

            print("jobs rejected:", pool.rejected)  # print rejected jobs and the error (if any)
            print("src size:", src.data_size, src.size)  # print the source size
            print("dst size:", dst.data_size, dst.size)  # print the destination size


if __name__ == '__main__':
    main()
