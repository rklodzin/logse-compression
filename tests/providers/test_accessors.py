"""
Testing accessors.
"""
import os
from io import UnsupportedOperation
from os import urandom

import pytest

from lhcbdirac_log import DataProvider, DataNotExistsError, DataExistsError, ReadOnlyError, DictProvider, \
    DictNotExistsError
from lhcbdirac_log.providers.base.accessors import NamedEntry, JobEntry
from lhcbdirac_log.providers.database import SQLDataProvider
from lhcbdirac_log.providers.memory import MemoryDataIO


def test_filename_to_dictname() -> None:
    assert NamedEntry.filename_to_dictname("data") == "data"
    assert NamedEntry.filename_to_dictname("9data1465") == "xdataxxxx"
    assert NamedEntry.filename_to_dictname("0123456789") == "x" * 10


class TestJobEntry:
    """Test JobEntry class."""

    def test_get0(self, any_non_empty_data_provider: DataProvider):
        job = any_non_empty_data_provider.get(64)

        with pytest.raises(DataNotExistsError):  # get non-existing data
            job.get("data")

        job = any_non_empty_data_provider.get(42)

        assert job.get("data").name == "data"  # get existing data

    def test_get1(self, readonly_data_provider: DataProvider):
        job = readonly_data_provider.get(42)

        assert job.get("data").name == "data"

        with pytest.raises(ReadOnlyError):
            job.get("data", create=True)  # existing data

        with pytest.raises(ReadOnlyError):
            job.get("data0", create=True)  # non-existing data

    def test_get2(self, data_provider: DataProvider):
        job = data_provider.get(64)

        assert job.empty

        data = job.get("data", create=True)  # create

        assert job.empty

        with data.writer() as w:  # effectively create the data (lazy creation)
            w.write(b"")

        assert not job.empty

        assert data.name == "data"
        assert data.name == job.get("data").name
        assert data.name == job.get("data", create=True).name
        assert data.name == job.create("data", exists_ok=True).name

        with pytest.raises(DataExistsError):  # already exists
            job.create("data")

        with pytest.raises(DataNotExistsError):  # not exists
            job.get("data2")

    def test_create0(self, data_provider: DataProvider):
        job = data_provider.get(64)

        assert job.empty
        data = job.create("data")  # create non-existing data
        assert job.empty

        with data.writer() as w:
            w.write(b"")

        assert not job.empty

        assert data.name == "data"
        assert data.name == job.get("data").name
        assert data.name == job.get("data", create=True).name
        assert data.name == job.create("data", exists_ok=True).name

        with pytest.raises(DataExistsError):  # get existing data
            job.create("data")

        data = job.create("data1", exists_ok=True)  # create non-existing data with exists_ok
        with data.writer() as w:
            w.write(b"")

        assert data.name == "data1"
        assert data.name == job.get("data1").name
        assert data.name == job.get("data1", create=True).name
        assert data.name == job.create("data1", exists_ok=True).name

        with pytest.raises(DataExistsError):  # get existing data
            job.create("data1")

    def test_create1(self, readonly_data_provider: DataProvider):
        job = readonly_data_provider.get(42)

        assert job.get("data").name == "data"
        with pytest.raises(DataNotExistsError):
            job.get("data0")

        with pytest.raises(ReadOnlyError):
            job.create("data")  # existing data

        with pytest.raises(ReadOnlyError):
            job.create("data", exists_ok=True)  # existing data

        with pytest.raises(ReadOnlyError):
            job.create("data0")  # non-existing data

        with pytest.raises(ReadOnlyError):
            job.create("data0", exists_ok=True)  # non-existing data

    def test_exists(self, data_provider: DataProvider) -> None:
        job = data_provider.get(64)

        # check lazy creation
        d = job.create("data")
        n = job.create("data")

        assert not d.exists
        assert not n.exists

        with d.writer() as w:  # effectively create the data
            w.write(b"")

        assert d.exists
        assert n.exists

        with pytest.raises(DataExistsError):
            job.create("data")

    def test_delete0(self, readonly_data_provider: DataProvider):
        job = readonly_data_provider.get(42)
        data = job.get("data")

        assert data.exists

        with pytest.raises(ReadOnlyError):
            job.delete("data")  # existing data

        assert data.exists

        with pytest.raises(ReadOnlyError):
            job.delete("data0")  # non-existing data

        assert data.exists

    def test_delete1(self, data_provider: DataProvider):
        job = data_provider.get(42)

        # delete non-existing job
        with pytest.raises(DataNotExistsError):
            job.delete("data0")

        data = job.get("data")
        assert data.exists

        job.delete("data")

        assert not data.exists

        with pytest.raises(DataNotExistsError):
            job.get("data")

        with pytest.raises(DataNotExistsError):
            job.delete("data")

    def test_default_delete0(self, readonly_data_provider: DataProvider):
        job = readonly_data_provider.get(42)
        data = job.get("data")

        assert data.exists

        with pytest.raises(ReadOnlyError):
            JobEntry.delete(job, "data")  # existing data

        assert data.exists

        with pytest.raises(ReadOnlyError):
            JobEntry.delete(job, "data0")  # non-existing data

        assert data.exists

    def test_default_delete1(self, data_provider: DataProvider):
        job = data_provider.get(42)

        # delete non-existing job
        with pytest.raises(DataNotExistsError):
            JobEntry.delete(job, "data0")

        data = job.get("data")
        assert data.exists

        JobEntry.delete(job, "data")

        assert not data.exists

        with pytest.raises(DataNotExistsError):
            job.get("data")

        with pytest.raises(DataNotExistsError):
            JobEntry.delete(job, "data")

    def test_clear0(self, readonly_data_provider: DataProvider):
        job = readonly_data_provider.get(64)

        assert job.empty

        with pytest.raises(ReadOnlyError):  # clear no data
            job.clear()

        assert job.empty

        job = readonly_data_provider.get(42)

        assert not job.empty

        with pytest.raises(ReadOnlyError):  # clear data
            job.clear()

        assert not job.empty

    def test_clear1(self, data_provider: DataProvider):
        job = data_provider.get(64)

        assert job.empty

        job.clear()  # clear no data

        assert job.empty

        job = data_provider.get(42)

        assert not job.empty

        job.clear()  # clear data

        assert job.empty

    def test_files0(self, any_non_empty_data_provider: DataProvider):
        job = any_non_empty_data_provider.get(64)
        assert set(job.files()) == set()
        assert {i.name for i in job} == set()
        assert len(job) == 0
        assert job.empty

        job = any_non_empty_data_provider.get(42)
        assert set(job.files()) == {"data", "data1"}
        assert {i.name for i in job} == {"data", "data1"}
        assert len(job) == 2
        assert not job.empty

    def test_files1(self, data_provider: DataProvider):
        job = data_provider.get(64)
        assert set(job.files()) == set()
        assert {i.name for i in job} == set()
        assert len(job) == 0
        assert job.empty

        with job.create("data").writer() as w:
            w.write(b"")
        assert set(job.files()) == {"data"}
        assert {i.name for i in job} == {"data"}
        assert len(job) == 1
        assert not job.empty

        with job.create("data2").writer() as w:
            w.write(b"")

        assert not job.create("data3").exists  # lazy init

        assert set(job.files()) == {"data2", "data"}
        assert {i.name for i in job} == {"data2", "data"}
        assert len(job) == 2
        assert not job.empty

        job.delete("data")
        assert set(job.files()) == {"data2"}
        assert {i.name for i in job} == {"data2"}
        assert len(job) == 1
        assert not job.empty

        job.delete("data2")
        assert set(job.files()) == set()
        assert {i.name for i in job} == set()
        assert len(job) == 0
        assert job.empty

    def test_data_size0(self, any_non_empty_data_provider: DataProvider):
        job = any_non_empty_data_provider.get(64)
        assert JobEntry.data_size.fget(job) == 0  # check default impl
        assert job.data_size == 0

        job = any_non_empty_data_provider.get(42)
        assert JobEntry.data_size.fget(job) == 42  # check default impl
        assert job.data_size == 42
        assert job.job_size != 0

    def test_data_size1(self, data_provider: DataProvider, bytes_data: bytes):
        job = data_provider.get(64)
        assert JobEntry.data_size.fget(job) == 0  # check default impl
        assert job.data_size == 0

        with job.create("data").writer() as w:
            w.write(bytes_data)

        assert JobEntry.data_size.fget(job) == 42  # check default impl
        assert job.data_size == 42
        assert job.job_size != 0

        with job.create("data2").writer() as w:
            w.write(b"x" * 84)

        assert JobEntry.data_size.fget(job) == 126  # check default impl
        assert job.data_size == 126
        assert job.job_size != 0

        job.delete("data")
        assert JobEntry.data_size.fget(job) == 84  # check default impl
        assert job.data_size == 84
        assert job.job_size != 0

        job.delete("data2")
        assert JobEntry.data_size.fget(job) == 0  # check default impl
        assert job.data_size == 0

    def test_update_info0(self, readonly_data_provider: DataProvider) -> None:
        job = readonly_data_provider.get(64)

        with pytest.raises(ReadOnlyError):
            job.update_info(None)

    def test_update_info1(self, decompressed_data_provider: DataProvider) -> None:
        job = decompressed_data_provider.get(64, create=True)

        with pytest.raises(RuntimeError):
            job.update_info(None)

    def test_info0(self, decompressed_data_provider: DataProvider) -> None:

        jinfo_h = b"""/Job/Arguments = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/BookkeepingLFNs = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/CPUTime = xxxxxxxxxxxx
/DIRACSetup = xxxxxxxx-xxxxxxxxxxx
/DebugLFNs =xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/Executable = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/InputDataModule = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/InputSandbox = xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
"""
        dirac_id = b"/JobID = 999000999\n"
        jinfo_f = b"""/JobRequirements = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/JobType = xxxxxxxxxxxxxxxxxx
/LogFilePath = xxxxxxxxxxxxxxxx
/LogLevel = xxxxxxxxx"""

        success = b"""<?xml version="1.0" encoding="UTF-8"?>

<summary xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xsi:noNamespaceSchemaLocation="$XMLSUMMARYBASEROOT/xml/XMLSummary.xsd">
    <success>True</success>
    <step>finalize</step>
    <usage>
        <stat unit="KB" useOf="MemoryMaximum">xxxxxxxxxxxx</stat>
    </usage>
    <input />
    <output>
        <file GUID="xxxxxxxxxxxxxxxxxxxxxxxxxxxx" name="xxxxxxxxxxxxxxxxxxxx" status="full">xxx</file>
    </output>
    <counters>"""
        failure = success.replace(b"True", b"False")

        job = decompressed_data_provider.create(99)
        assert job.info.dirac_id is None
        assert job.info.success is None

        with SQLDataProvider() as sql:

            n = 100

            for dirac in (b"", dirac_id):
                for jsuccess in (b"", success):

                    job = decompressed_data_provider.create(n)
                    with job.create("job.info").writer() as w:
                        w.write(jinfo_h)
                        w.write(dirac)
                        w.write(jinfo_f)

                    with job.create("summaryxxxxxxxxx_xxxxx_xxx_8.xml").writer() as w:
                        w.write(failure)

                    with job.create("summaryxxxxxxxxx_xxxxx_xxx_15.xml").writer() as w:
                        w.write(jsuccess)

                    assert job.info.dirac_id == (999000999 if dirac else None)
                    assert job.info.success == (True if jsuccess else None)

                    j = sql.create(n)

                    assert j.info.dirac_id is None
                    assert j.info.success is None

                    if n & 1:  # try update on uninit info
                        sql.delete(n)
                        j = sql.create(n)

                    j.update_info(job.info)
                    j._info = None

                    assert j.info.dirac_id == job.info.dirac_id
                    assert j.info.success == job.info.success

                    j.update_info()
                    j._info = None

                    assert j.info.dirac_id == job.info.dirac_id
                    assert j.info.success == job.info.success

                    j.update_info()  # do nothing
                    j._info = None
                    j.update_info()  # do nothing

                    assert j.info.dirac_id == job.info.dirac_id
                    assert j.info.success == job.info.success

                    n += 1


class TestDataEntry:
    """Test DataEntry class."""

    def test_job(self, any_non_empty_data_provider: DataProvider) -> None:
        job = any_non_empty_data_provider.get(42)
        assert job.get("data").job == job.job == 42

    def test_name(self, any_non_empty_data_provider: DataProvider) -> None:
        job = any_non_empty_data_provider.get(42)
        assert job.get("data").name == "data"
        assert job.get("data1").name == "data1"

    def test_size0(self, any_non_empty_data_provider: DataProvider) -> None:
        job = any_non_empty_data_provider.get(42)
        d = job.get("data")
        n = job.get("data1")

        assert d.size == 0  # empty data
        assert n.size == 42  # non-empty data

    def test_size1(self, data_provider: DataProvider, bytes_data: bytes) -> None:
        job = data_provider.get(42)
        d = job.get("data1")
        x = job.create("data2")

        assert d.size == 42  # non-empty data
        assert x.size == 0  # non-existing data

        with x.writer() as w, d.writer() as o:
            w.write(bytes_data)  # create non-empty data
            o.write(b"x")  # update non-empty data

        assert d.size == 1  # non-empty data
        assert x.size == 42  # non-empty data

        d.delete()
        assert d.size == 0  # empty data

        with x.writer() as w:
            w.write(b"")  # explicit empty data

        assert x.size == 0  # empty data

        with x.writer() as w:
            w.write(b"x" * 64)

        assert x.size == 64  # non-empty data

        with x.writer():
            pass  # implicit empty data

        assert x.size == 0  # empty data

    def test_exists0(self, any_non_empty_data_provider: DataProvider) -> None:
        job = any_non_empty_data_provider.get(42)
        d = job.get("data")
        n = job.get("data1")

        assert d.exists
        assert n.exists

    def test_exists1(self, data_provider: DataProvider) -> None:
        job = data_provider.get(42)
        d = job.get("data")
        n = job.get("data1")
        x = job.create("data2")

        assert d.exists
        assert n.exists
        assert not x.exists

        with d.writer(), n.writer(), x.writer():
            pass

        assert d.exists  # empty data still exists
        assert n.exists  # empty data still exists
        assert x.exists  # empty created data exists

        d.delete()
        n.delete()
        x.delete()

        assert not d.exists
        assert not n.exists
        assert not x.exists

    def test_delete0(self, readonly_data_provider: DataProvider) -> None:
        job = readonly_data_provider.get(42)
        d = job.get("data")

        assert d.exists

        with pytest.raises(ReadOnlyError):
            d.delete()

        assert d.exists

    def test_delete1(self, data_provider: DataProvider) -> None:
        job = data_provider.get(42)
        d = job.get("data")
        n = job.get("data1")
        x = job.create("data2")

        assert d.exists
        assert n.exists
        assert not x.exists

        d.delete()  # delete empty existing data
        n.delete()  # delete non-empty existing data

        assert not d.exists
        assert not n.exists
        assert not x.exists

        with pytest.raises(DataNotExistsError):
            d.delete()  # delete no longer existing data

        with pytest.raises(DataNotExistsError):
            x.delete()  # delete non-existing data

        assert not d.exists
        assert not n.exists
        assert not x.exists

        with d.writer(), x.writer():
            pass

        assert d.exists
        assert not n.exists
        assert x.exists

        d.delete()  # delete recreated data
        x.delete()  # delete created data

        assert not d.exists
        assert not n.exists
        assert not x.exists

    def test_io0(self, any_non_empty_data_provider: DataProvider, bytes_data: bytes) -> None:
        job = any_non_empty_data_provider.get(42)
        d = job.get("data")
        n = job.get("data1")

        with d.reader() as r:
            assert r.read() == b""  # read empty data

        with n.reader() as r:
            assert r.read() == bytes_data  # read non-empty data

    def test_io1(self, data_provider: DataProvider) -> None:
        job = data_provider.get(42)
        _ = job.get("data")
        n = job.get("data1")
        u = job.get("data1")
        x = job.create("data2")

        assert n is not u  # clone

        with pytest.raises(DataNotExistsError):
            x.reader()  # read non-existing data

        # create empty data
        with n.writer() as w, x.writer():
            w.write(b"")  # x: implicit | n: explicit

        with n.reader() as r, x.reader() as i, u.reader() as c:
            assert r.read() == b""  # read (updated) empty data
            assert i.read() == b""  # read (created) empty data
            assert c.read() == b""  # read 'n' data

        # update data
        with n.writer() as w, x.writer() as o:
            w.write(y := urandom(42))
            o.write(z := urandom(81))

        with n.reader() as r, x.reader() as i, u.reader() as c:
            assert r.read() == y  # updated data
            assert i.read() == z  # created data
            assert c.read() == y  # read 'n' data

        assert n.size == 42
        assert x.size == 81
        assert u.size == 42

        assert n.exists
        assert x.exists
        assert u.exists

        n.delete()
        x.delete()

        assert not n.exists
        assert not x.exists
        assert not u.exists

        with pytest.raises(DataNotExistsError):
            u.delete()  # delete no longer existing data

        with pytest.raises(DataNotExistsError):
            n.reader()  # read no longer existing data

        with pytest.raises(DataNotExistsError):
            x.reader()  # read no longer existing data

    def test_io2(self, compressed_data_provider: DataProvider) -> None:

        if isinstance(compressed_data_provider, SQLDataProvider):
            compressed_data_provider.dict_provider.mark_invalid("dxxxx")
            s = compressed_data_provider.create(99).create("d0000").writer()
        else:
            s = MemoryDataIO("aaa", bytearray(), read_mode=False)

        for m in (MemoryDataIO("aaa", bytearray()), s):
            with m:
                with pytest.raises(UnsupportedOperation):
                    _ = next(m)

                with pytest.raises(UnsupportedOperation):
                    m.readline()

                with pytest.raises(UnsupportedOperation):
                    m.readlines()

                with pytest.raises(UnsupportedOperation):
                    m.writelines([])

                if 'r' in m.mode:
                    assert m.seek(0) == 0
                else:
                    with pytest.raises(OSError):
                        m.seek(0)

                assert m.name in repr(m)

    def test_reader0(self, any_non_empty_data_provider: DataProvider, bytes_data: bytes) -> None:
        job = any_non_empty_data_provider.get(42)
        d = job.get("data1")

        with d.reader() as r:
            assert not r.closed
            assert r.seekable()
            assert r.readable()
            assert not r.writable()
            assert "r" in r.mode
            assert r.name.endswith(d.name)

            # read
            assert r.tell() == 0
            assert r.read(0) == b""
            assert r.tell() == 0
            assert r.read(4) == bytes_data[:4]
            assert r.tell() == 4
            assert r.read(5) == bytes_data[4:9]
            assert r.tell() == 9
            assert r.read() == bytes_data[9:]
            assert r.tell() == 42
            assert r.read() == b""
            assert r.tell() == 42
            assert r.read(-1) == b""
            assert r.tell() == 42

            # read all (default)
            assert r.seek(0, os.SEEK_SET) == 0
            assert r.tell() == 0
            assert r.read() == bytes_data
            assert r.tell() == 42
            assert r.read() == b""
            assert r.tell() == 42

            # read all (negative)
            assert r.seek(0, os.SEEK_SET) == 0
            assert r.tell() == 0
            assert r.read(-1) == bytes_data
            assert r.tell() == 42
            assert r.read(-1) == b""
            assert r.tell() == 42

            # seek set
            assert r.seek(0, os.SEEK_SET) == 0
            assert r.tell() == 0
            assert r.seek(5, os.SEEK_SET) == 5
            assert r.tell() == 5

            strict = True

            try:
                r.seek(-1, os.SEEK_SET)
            except OSError:
                assert r.tell() == 5
            else:
                strict = False
                assert r.tell() == 0  # ZipFile round negative seek to 0
                assert r.seek(5, os.SEEK_SET) == 5

            if strict:  # seek overflow
                assert r.seek(100, os.SEEK_SET) == 100
            else:  # zipfile round down seek to size
                assert r.seek(100, os.SEEK_SET) == 42

            assert r.seek(5, os.SEEK_SET) == 5

            # seek cur
            assert r.seek(0, os.SEEK_CUR) == 5
            assert r.tell() == 5
            assert r.seek(5, os.SEEK_CUR) == 10
            assert r.tell() == 10
            assert r.seek(-5, os.SEEK_CUR) == 5
            assert r.tell() == 5

            try:
                r.seek(-6, os.SEEK_CUR)
            except OSError:
                assert r.tell() == 5
            else:
                assert r.tell() == 0  # ZipFile round negative seek to 0
                assert r.seek(5, os.SEEK_SET) == 5

            if strict:  # seek overflow
                assert r.seek(100, os.SEEK_CUR) == 105
            else:  # zipfile round down seek to size
                assert r.seek(100, os.SEEK_CUR) == 42

            # seek end
            assert r.seek(0, os.SEEK_END) == 42
            assert r.tell() == 42
            if strict:  # seek overflow
                assert r.seek(5, os.SEEK_END) == 47
                assert r.tell() == 47
            else:  # zipfile round down seek to size
                assert r.seek(5, os.SEEK_END) == 42
                assert r.tell() == 42
            assert r.seek(-5, os.SEEK_END) == 37
            assert r.tell() == 37

            try:
                r.seek(-43, os.SEEK_END)
            except OSError:
                assert r.tell() == 37
            else:
                assert r.tell() == 0  # ZipFile round negative seek to 0
                assert r.seek(37, os.SEEK_SET) == 37

            assert r.seek(-42, os.SEEK_END) == 0
            assert r.tell() == 0

            with pytest.raises(UnsupportedOperation):
                r.write(b"")

            # with pytest.raises(ValueError):  # python 3.12.5 breaking change (works below)
            #     r.seek(0, 3)

        assert r.closed
        assert "r" in r.mode
        assert r.name.endswith(d.name)

        assert not r.writable()

        with pytest.raises(ValueError):
            r.readable()

        with pytest.raises(ValueError):
            r.read()

        with pytest.raises(ValueError):
            r.seekable()

        with pytest.raises(ValueError):
            r.tell()

        with pytest.raises(ValueError):
            r.seek(0, 0)

        with d.reader() as r, d.reader() as y:
            assert r is not y
            assert r.read() == y.read() == bytes_data

    def test_reader1(self, data_provider: DataProvider):
        job = data_provider.get(42)
        d = job.create("aaa")

        with pytest.raises(DataNotExistsError):
            _ = d.reader()

    def test_writer0(self, readonly_data_provider: DataProvider) -> None:
        job = readonly_data_provider.get(42)
        d = job.get("data")
        n = job.get("data1")

        with pytest.raises(ReadOnlyError):
            d.writer()  # read-only empty data

        with pytest.raises(ReadOnlyError):
            n.writer()  # read-only non-empty data

    def test_writer1(self, data_provider: DataProvider, bytes_data: bytes) -> None:
        job = data_provider.get(42)
        d = job.get("data")  # empty data
        n = job.get("data1")  # non-empty data
        x = job.create("data2")  # non-existing data

        for y in (d, n, x):
            with y.writer():
                pass  # implicit empty data

            with y.reader() as r:
                assert r.read() == b""

            with y.writer() as w:
                w.write(bytes_data)  # non-empty data

            with y.reader() as r:
                assert r.seek(0, os.SEEK_END) == len(bytes_data)
                r.seek(0, os.SEEK_SET)
                assert r.read() == bytes_data

            with y.writer() as w:
                w.write(b"")  # explicit empty data

            with y.reader() as r:
                assert r.read() == b""

            with y.writer() as w:
                w.write(bytes_data)  # non-empty data (overwrite after)

            # non-empty chunked data
            with y.writer() as w:
                w.write(bytes_data)
                w.write(b"+++")
                w.write(bytes_data[::-1])

            with y.reader() as r:
                assert r.read(42) == bytes_data
                assert r.read(3) == b"+++"
                assert r.read() == bytes_data[::-1]

            # non-empty chunked data with flush
            with y.writer() as w:
                w.write(bytes_data)
                w.flush()
                w.write(b"---")
                w.write(bytes_data[::-1])

            with y.reader() as r:
                assert r.read(42) == bytes_data
                assert r.read(3) == b"---"
                assert r.read() == bytes_data[::-1]

            # writer file-like obj behavior
            with y.writer() as w:
                assert not w.closed
                assert not w.readable()
                assert w.writable()
                assert w.mode == "wb"
                assert w.name.endswith(y.name)

                assert w.tell() == 0
                assert w.write(b"x" * 8) == 8
                assert w.tell() == 8

                with pytest.raises(UnsupportedOperation):
                    w.read()

            assert w.closed
            assert w.mode == "wb"
            assert w.name.endswith(y.name)

            assert not w.readable()

            with pytest.raises(ValueError):
                w.writable()

            with pytest.raises(ValueError):
                w.write(b"")

            with pytest.raises(ValueError):
                w.tell()


class TestDictEntry:

    def test_name(self, any_non_empty_dict_provider: DictProvider) -> None:
        d = any_non_empty_dict_provider["data"]
        n = any_non_empty_dict_provider["datax"]

        assert d.name == "data"
        assert n.name == "datax"

        assert d.name == d.dict_name
        assert n.name == n.dict_name

    def test_size(self, any_non_empty_dict_provider: DictProvider,
                  all_dict_data: tuple[tuple[bytes, int, int]]
                  ) -> None:
        d = any_non_empty_dict_provider["data"]
        n = any_non_empty_dict_provider["datax"]

        assert d.size == all_dict_data[0][1]
        assert n.size == all_dict_data[1][1]

        assert not d.is_loaded  # size call doesn't load data
        assert not n.is_loaded  # size call doesn't load data

    def test_exists(self, any_non_empty_dict_provider: DictProvider) -> None:
        d = any_non_empty_dict_provider["data"]
        n = any_non_empty_dict_provider["datax"]

        assert d.exists
        assert n.exists

        assert not d.is_loaded  # exists call doesn't load data
        assert not n.is_loaded  # exists call doesn't load data

    def test_data(self, any_non_empty_dict_provider: DictProvider,
                  all_dict_data: tuple[tuple[bytes, int, int]]) -> None:
        d = any_non_empty_dict_provider["data"]
        n = any_non_empty_dict_provider["datax"]

        assert not d.is_loaded
        assert not n.is_loaded

        assert d.data == all_dict_data[0][0]
        assert n.data == all_dict_data[1][0]

        assert d.is_loaded
        assert n.is_loaded

    def test_dict(self, any_non_empty_dict_provider: DictProvider,
                  all_dict_data: tuple[tuple[bytes, int, int]]) -> None:
        d = any_non_empty_dict_provider["data"]
        n = any_non_empty_dict_provider["datax"]

        assert d.dict is d.dict  # cached
        assert n.dict is n.dict  # cached

        assert d.dict.dict_id() == d.zstd_id == all_dict_data[0][2]
        assert n.dict.dict_id() == n.zstd_id == all_dict_data[1][2]

        assert d.dict.as_bytes() == d.data == all_dict_data[0][0]
        assert n.dict.as_bytes() == n.data == all_dict_data[1][0]

        assert d.size == all_dict_data[0][1]
        assert n.size == all_dict_data[1][1]

        assert d.is_loaded
        assert n.is_loaded

    def test_delete(self, dict_provider: DictProvider,
                    all_dict_data: tuple[tuple[bytes, int, int]]) -> None:
        d = dict_provider["data"]
        n = dict_provider["datax"]

        assert d.exists
        assert n.exists

        assert n.data == all_dict_data[1][0]
        del dict_provider["data"]  # delete dict
        del dict_provider["datax"]  # delete loaded dict

        assert not d.exists
        assert not n.exists

        assert not d.is_loaded
        assert n.is_loaded

        assert n.data == all_dict_data[1][0]
        assert n.dict.dict_id() == all_dict_data[1][2]

        assert d.size == 0
        assert n.size == 0

        with pytest.raises(DictNotExistsError):
            _ = d.data  # deleted data

        with pytest.raises(DictNotExistsError):
            _ = d.dict  # crash even before trying to load the dict
