"""Base module for the data provider layer of the library.

This defines entry base classes for data and dict access / IO operations.

Classes:
    - Entry: Base entry class, provide an abstraction for the entry concept.
    - NamedEntry: Base named entry class, provide an abstraction for the named entry concept.
    - DataEntry: Base data entry class, provide an abstraction layer for data access / IO operations.
    - DictEntry: Base dict entry class, provide an abstraction layer for dict access.
    - JobEntry: Base job entry class, provide an abstraction layer for job access.
    - JobInfo: Metadata for a job entry.
"""

from abc import ABC, abstractmethod
from collections.abc import Generator, Iterator
from dataclasses import dataclass
from typing import BinaryIO, final, override

from zstandard import DICT_TYPE_FULLDICT, ZstdCompressionDict

from ...config import Config
from .exceptions import DataNotExistsError, ReadOnlyError

__all__ = (
    "Entry",
    "NamedEntry",
    "DataEntry",
    "DictEntry",
    "JobEntry",
    "JobInfo",
)


class Entry(ABC):
    """[Internal] Base entry class, provide an abstraction for the entry concept.

    This represents any type of entry, the entry can be read-only.
    """

    __slots__ = ("_readonly",)

    def __init__(self, *, readonly: bool) -> None:
        """[Internal] Initialize the entry.

        Args:
            readonly: make the entry read-only
        """
        self._readonly = readonly

    @property
    @final
    def readonly(self) -> bool:
        """Check if the entry is read-only.

        Returns:
            True if the entry is read-only, False otherwise
        """
        return self._readonly


class NamedEntry(Entry, ABC):
    """[Internal] Base named entry class, provide an abstraction for the named entry concept.

    This represents a named entry, identified by a string.
    """

    __slots__ = ("_name",)

    def __init__(self, name: str, *, readonly: bool) -> None:
        """[Internal] Initialize the entry.

        Args:
            name: the entry name
            readonly: make the entry read-only
        """
        super().__init__(readonly=readonly)

        self._name = name

    @staticmethod
    @final
    def filename_to_dictname(name: str) -> str:
        """Get the dict name associated to a file name.

        Args:
            name: the file name

        Returns:
            the dict name

        Notes:
            - implementation detail: replace digits with 'x'
        """
        return "".join("x" if i.isdecimal() else i for i in name)

    @property
    @final
    def name(self) -> str:
        """Get the entry name.

        Returns:
            the entry name
        """
        return self._name

    @property
    @abstractmethod
    def dict_name(self) -> str:
        """Get the dict name.

        Returns:
            the dict name
        """


class DataEntry(NamedEntry, ABC):
    """Base data entry class, provide an abstraction layer for data access / IO operations.

    This represents a data entry through a data provider,
    the entry's data can be read/write through file-like object obtained from the main reader/writer methods
    """

    __slots__ = (
        "_compressed",
        "_job",
    )

    def __init__(self, name: str, job: int, *, compressed: bool, readonly: bool) -> None:
        """[Internal] Initialize the data entry.

        Args:
            name: the data name
            job: the job id
            compressed: indicate that the underlying data is compressed (in Zstandard)
            readonly: indicate weather the data is read-only or not

        Notes:
            - instantiation alone has no effects on the provider, the data will be created on first write
        """
        self._job = job
        self._compressed = compressed
        super().__init__(name, readonly=readonly)

    @property
    @final
    def compressed(self) -> bool:
        """Check if the underlying data is compressed or not (in Zstandard).

        Returns:
            True if the underlying data is compressed, False otherwise
        """
        return self._compressed

    @property
    @final
    def size(self) -> int:
        """Get the stored data size.

        This is the size of the stored data, the same as the read data from the reader.

        Returns:
            the data size or 0 if the entry not exists

        Notes:
            - zero-size may indicate that the data exists but is empty
            - compressed size for compressed entry, uncompressed size for uncompressed entry
        """
        return self._size() or 0

    @property
    @final
    def exists(self) -> bool:
        """Check if the data exists.

        Returns:
            True if the data exists, False otherwise
        """
        return self._size() is not None

    @property
    @final
    def job(self) -> int:
        """Get the job id.

        Returns:
            the job id
        """
        return self._job

    @property
    @override
    @final
    def dict_name(self) -> str:
        """Get the dict name.

        Returns:
            the dict name
        """
        return self.filename_to_dictname(self._name)

    @abstractmethod
    def _reader(self) -> BinaryIO:
        """[Internal] Get a data reader as a file-like object.

        Returns:
            a data reader

        Notes:
            - the caller is responsible of the reader's lifecycle
            - each call returns a new reader
            - may not support concurrent readers and / or writers
        """

    @final
    def reader(self) -> BinaryIO:
        """Get a data reader as a file-like object.

        Returns:
            a data reader

        Raises:
            DataNotExistsError: if the data does not exist

        Notes:
            - the caller is responsible of the reader's lifecycle
            - each call returns a new reader
            - may not support concurrent readers and / or writers
        """
        if not self.exists:
            raise DataNotExistsError(self._name)

        return self._reader()

    @abstractmethod
    def _writer(self) -> BinaryIO:
        """[Internal] Get a data writer as a file-like object.

        Returns:
            a data writer

        Notes:
            - the caller is responsible of the writer's lifecycle
            - each call returns a new writer
            - may not support concurrent readers and / or writers
        """

    @final
    def writer(self) -> BinaryIO:
        """Get a data writer as a file-like object.

        Returns:
            a data writer

        Raises:
            ReadOnlyError: if the data is read-only

        Notes:
            - the caller is responsible of the writer's lifecycle
            - each call returns a new writer
            - may not support concurrent readers and / or writers
        """
        if self._readonly:
            msg = f"Data '{self._name}' is read-only"
            raise ReadOnlyError(msg)

        return self._writer()

    @abstractmethod
    def _size(self) -> int | None:
        """[Internal] Get the stored data size.

        Returns:
            the stored data size or None if the data does not exist
        """

    @abstractmethod
    def _delete(self) -> None:
        """[Internal] Delete the data.

        Raises:
            DataNotExistsError: if the data does not exist
        """

    @final
    def delete(self) -> None:
        """Delete the data.

        Raises:
            DataNotExistsError: if the data does not exist
            ReadOnlyError: if the data is read-only
        """
        if self._readonly:
            msg = f"Data '{self._name}' is read-only"
            raise ReadOnlyError(msg)

        self._delete()


class DictEntry(NamedEntry, ABC):
    """Base dict entry class, provide an abstraction layer for dict access.

    This represents a dict entry, an instance implies the dict exists and can be accessed (except after manual delete).

    Delete operation must be done through the associated provider,
    as dict entry instances are considered read-only and may not be used after delete operation.
    """

    __slots__ = (
        "_config",
        "_data",
        "_dict",
        "_zstd_id",
    )

    def __init__(self, name: str, config: Config, data: bytes | None = None, zstd_id: int | None = None) -> None:
        """[Internal] Initialize the dict entry.

        Args:
            name: the dict name
            config: the configuration to use for precomputing the dictionary
            data: the dict data (create a new dict if not None)
            zstd_id: the zstd dictionary id (None for unknown)
        """
        super().__init__(name, readonly=True)
        self._data = data
        self._dict: ZstdCompressionDict | None = None
        self._config = config
        self._zstd_id = zstd_id

        if self._data is not None:
            self._save()

    @property
    @override
    @final
    def dict_name(self) -> str:
        """Get the dict name.

        Returns:
            the dict name

        Notes:
            - alias to the dict entry name
        """
        return self._name

    @property
    @final
    def data(self) -> bytes:
        """Get the dict data.

        Returns:
            the dict data

        Raises:
            DictNotExistsError: if the dict does not exist (may never be raised under normal usages)

        Notes:
            - the data is loaded on first access (lazy loading)
        """
        if self._data is None:
            self._data = self._load_data()

        return self._data

    @property
    @abstractmethod
    def size(self) -> int:
        """Get the dict size.

        Returns:
            the dict size or 0 if the dict does not exist
        """

    @property
    @abstractmethod
    def exists(self) -> bool:
        """Check if the dict exists.

        Returns:
            True if the dict exists, False otherwise
        """

    @property
    @final
    def dict(self) -> ZstdCompressionDict:
        """Get the zstd-dict object (precomputed for shared usaged).

        Returns:
            the zstd-dict object

        Raises:
            DictNotExistsError: if the dict does not exist (may never be raised under normal usages)
        """
        if self._dict is None:
            self._load_dict()

        return self._dict

    @property
    @final
    def zstd_id(self) -> int:
        """Get the zstd dictionary id.

        Returns:
            the zstd dictionary id

        Raises:
            DictNotExistsError: if the dict does not exist (may never be raised under normal usages)
        """
        if self._zstd_id is None:
            self._zstd_id = self.dict.dict_id()

        return self._zstd_id

    @property
    @final
    def is_loaded(self) -> bool:
        """Check if the dict is loaded.

        Returns:
            True if the dict is loaded, False otherwise
        """
        return self._data is not None

    @final
    def _load_dict(self) -> None:
        """[Internal] Load the dict from data."""
        self._dict = ZstdCompressionDict(self.data, DICT_TYPE_FULLDICT)
        self._dict.precompute_compress(compression_params=self._config.params)

    @abstractmethod
    def _load_data(self) -> bytes:
        """[Internal] Get the dict's data.

        Returns:
            the dict's data

        Raises:
            DictNotExistsError: if the dict does not exist
        """

    @abstractmethod
    def _save(self) -> None:
        """[Internal] Save the dict data / create the dict entry.

        Notes:
            - the behavior is undefined if the dict already exists (may raise an error or overwrite)
        """


@dataclass
class JobInfo:
    """Metadata for a job entry.

    Unset attributes are set to None.

    Attributes:
        dirac_id: the job dirac id
        success: the job success status
    """

    __slots__ = (
        "dirac_id",
        "success",
    )

    dirac_id: int | None
    success: bool | None


class JobEntry[E: DataEntry](Entry, ABC):
    """Base job entry class, provide an abstraction layer for job access.

    This represents a job entry, managing the job's data entries.
    Job metadata can be accessed on uncompressed jobs.
    For compressed jobs, the implementation must handle metadata saving and loading.
    """

    __slots__ = (
        "_compressed",
        "_job",
        "_info",
    )

    def __init__(self, job: int, *, compressed: bool, readonly: bool) -> None:
        """[Internal] Initialize the job entry.

        Args:
            job: the job id
            compressed: indicate whether the underlying data is compressed or not (in Zstandard)
            readonly: indicate weather the job is read-only or not
        """
        super().__init__(readonly=readonly)

        self._info = None
        self._job = job
        self._compressed = compressed

    @property
    @final
    def job(self) -> int:
        """Get the job id.

        Returns:
            the job id
        """
        return self._job

    @property
    @final
    def compressed(self) -> bool:
        """Check if the underlying data is compressed or not (in Zstandard).

        Returns:
            True if the underlying data is compressed, False otherwise
        """
        return self._compressed

    @property
    def empty(self) -> bool:
        """Check if the job is empty.

        Returns:
            True if the job is empty, False otherwise
        """
        return not any(True for _ in self.files())

    @property
    def info(self) -> JobInfo:
        """Get the job's metadata.

        Returns:
            the job's metadata

        Notes:
            - call update_info to save changes if any
        """
        if self._info is None:
            self._info = self._load_info()

        return self._info

    @staticmethod
    def _readlines(f: BinaryIO, lines: list[bytes], n: int = 1024) -> bool:
        """[Internal] Read lines from a file.

        Args:
            f: the file to read from
            lines: the list to store the lines in
            n: the number of bytes to read at once

        Returns:
            False if EOF is reached, True otherwise
        """
        read = True
        while len(lines) <= 1 and read:
            data = f.read(n)

            if len(data) < n:  # EOF
                read = False

            st, *end = data.splitlines()
            if not lines or lines[-1].endswith(b"\n"):
                lines.append(st)
            else:
                lines[-1] += st
            lines.extend(end)

        return read

    @staticmethod
    def _sub_id(x: str) -> int:
        """[Internal] Get the sub id from the file name.

        Args:
            x: the file name

        Returns:
            the sub id
        """
        try:
            return int(x.rsplit("_", 1)[-1][:-4])
        except ValueError:  # pragma: no cover
            return 0

    def _get_dirac_id(self) -> int | None:
        """[Internal] Get the job's DIRAC ID."""
        try:
            with self.get("job.info").reader() as f:
                read = True
                lines: list[bytes] = []

                while read:
                    read = self._readlines(f, lines)

                    while len(lines) > read:
                        line = lines.pop(0)

                        if line.startswith(b"/JobID"):
                            return int(line.split(b"=")[1].strip())
        except (DataNotExistsError, ValueError):
            pass

    def _get_success(self) -> bool | None:
        """[Internal] Get the job's success status.

        Returns:
            the job's success status
        """
        try:
            file = max((i for i in self.files() if i.startswith("summary") and i.endswith(".xml")), key=self._sub_id)

            with self.get(file).reader() as f:
                read = True
                lines = []

                while read:
                    read = self._readlines(f, lines)

                    while len(lines) > read:
                        line = lines.pop(0)

                        try:
                            return line[line.index(b"<success>") + 9] in b"Tt"
                        except ValueError:
                            continue

        except (DataNotExistsError, ValueError):
            pass
        return None

    def _load_info(self) -> JobInfo:
        """[Internal] Load the job's metadata.

        Returns:
            the job's metadata
        """
        info = JobInfo(None, None)

        if not self._compressed:
            info.dirac_id = self._get_dirac_id()
            info.success = self._get_success()

        return info

    @abstractmethod
    def _update_info(self) -> None:
        """[Internal] Update the job's metadata.

        Notes:
            - implementation not handling saving metadata can leave this method empty
        """

    @final
    def update_info(self, info: JobInfo | None = None) -> None:
        """Update the job's metadata, for compressed job.

        Args:
            info: new metadata to copy from and save (or None to save the current metadata)

        Raises:
            RuntimeError: if called on non-compressed job
            ReadOnlyError: if the job is read-only

        Notes:
            - for non-compressed job, the metadata is loaded directly from the job's data
            - for compressed job, the metadata is not accessible, and so, must be saved by the provider (if support is intended)
        """
        if self._readonly:
            msg = f"Job '{self._job}' is read-only"
            raise ReadOnlyError(msg)

        if not self._compressed:
            msg = f"Job '{self._job}' is not compressed"
            raise RuntimeError(msg)

        if info is None:
            if self._info is None:
                self._info = self._load_info()
        elif self._info is None:  # copy the info
            self._info = JobInfo(info.dirac_id, info.success)
        else:  # transfer the info
            self._info.dirac_id = info.dirac_id
            self._info.success = info.success

        self._update_info()

    @abstractmethod
    def _get(self, name: str, *, create: bool = False) -> E:
        """[Internal] Get a data entry.

        Args:
            name: the data name
            create: if True, create the data if it does not exist (default is False)

        Returns:
            the data entry

        Raises:
            DataNotExistsError: if the data does not exist and create is False

        Notes:
            - the entry, if newly created, will not exist until data is written
        """

    @final
    def get(self, name: str, *, create: bool = False) -> E:
        """Get a data entry.

        Args:
            name: the data name
            create: if True, create the data if it does not exist (default is False)

        Returns:
            the data entry

        Raises:
            DataNotExistsError: if the data does not exist and create is False
            ReadOnlyError: if the job is read-only and create is True

        Notes:
            - the entry, if newly created, will not exist until data is written
        """
        if create and self._readonly:
            msg = f"Job '{self._job}' is read-only"
            raise ReadOnlyError(msg)

        return self._get(name, create=create)

    @abstractmethod
    def _create(self, name: str, *, exists_ok: bool = False) -> E:
        """[Internal] Create a data entry.

        Args:
            name: the data name
            exists_ok: if True, ignore the error if the data already exists (default is False)

        Returns:
            the data entry

        Raises:
            DataExistsError: if the data already exists and exists_ok is False

        Notes:
            - the entry, if newly created, will not exist until data is written
        """

    @final
    def create(self, name: str, *, exists_ok: bool = False) -> E:
        """Create a data entry.

        Args:
            name: the data name
            exists_ok: if True, ignore the error if the data already exists (default is False)

        Returns:
            the data entry

        Raises:
            DataExistsError: if the data already exists and exists_ok is False
            ReadOnlyError: if the job is read-only

        Notes:
            - the entry, if newly created, will not exist until data is written
        """
        if self._readonly:
            msg = f"Job '{self._job}' is read-only"
            raise ReadOnlyError(msg)

        return self._create(name, exists_ok=exists_ok)

    def delete(self, name: str) -> None:
        """Delete a data entry.

        Args:
            name: the data name

        Raises:
            DataNotExistsError: if the data does not exist
            ReadOnlyError: if the job is read-only
        """
        if self._readonly:
            msg = f"Job '{self._job}' is read-only"
            raise ReadOnlyError(msg)

        self.get(name).delete()

    def clear(self) -> None:
        """Clear all data entries.

        Raises:
            ReadOnlyError: if the job is read-only
        """
        if self._readonly:
            msg = f"Job '{self._job}' is read-only"
            raise ReadOnlyError(msg)

        for i in self:
            i.delete()

    @abstractmethod
    def files(self) -> Generator[str, None, None]:
        """Get a generator of the job's files.

        Returns:
            a generator of the job's file names
        """

    @property
    def data_size(self) -> int:
        """Get all stored data size.

        Returns:
            the sum of all the job's data sizes

        Notes:
            - see DataEntry.size for more details
        """
        return sum(i.size for i in self)

    @property
    def job_size(self) -> int:
        """Get the stored job size.

        Returns:
            the stored job size

        Notes:
            - may include additional overheads compared to data_size
            - may represent better the 'on-disk' size
            - may not be necessarily upper or lower than data_size
        """
        return self.data_size

    @final
    def __iter__(self) -> Iterator[E]:
        """Iterate over all the job's data entries.

        Returns:
            a generator of the job's data entries
        """
        return (self.get(i) for i in self.files())

    def __len__(self) -> int:
        """Get the number of data entries.

        Returns:
            the number of data entries
        """
        return sum(1 for _ in self.files())
