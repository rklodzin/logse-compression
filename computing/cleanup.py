import gzip
import json
import os
import traceback
from typing import Generator

from fsspec.asyn import AsyncFileSystem
import fsspec
from os.path import join, basename
import shutil
import tarfile, zipfile

PATH = ("LHCb", "MC")
BASE = "/eos/lhcb/grid/prod/lhcb/logSE/lhcb"
token = "zteos64:MDAwMDAyNTh4nOOaysTFXFReIdBzt3crmxRLTlJyphJTlakGo5Gyfmp-sX5ORnKSfnpRZop-QVF-CoSbk5_uWJSckVmWasHopZJoZGlmmWicqmtimmSka2iYmqZrmWRkqJtokpRmYGhuaWZqnBi0jjHarShTwas0R8HQUsHQ0MrYwsrIQMHIwMgkVqE0M8XK0NTE3MQyGmR_rEI6SMDE3CC6yjRWoSQzJTWvxAoko2dkZGhiYmBkZWRsaOSQU1GQU1psaWKmkJeYmwpWoJCSZ6UAdGiJVXZRkqlCYkGBlUJGfjFQN0ytXnJqUZ5ecoZCSn5uYmaeFYybnppvZWBqaKxQXJqSb2UopBBxbammJqtz0V3pJ_ulgyxc5X63T733Y8WPxXsmXGhh3PlDajTksIacwuxDfS-ZAYhnv8Y%3d"


def move(fs: AsyncFileSystem, src: str, dst: str):
    with fs.open(dst, "wb") as d:
        with fs.open(src, "rb") as s:
            shutil.copyfileobj(s, d)

    fs.rm(src)


def fix_zip(fs: AsyncFileSystem, src: str, dstfolder: str = "") -> bool:
    y = basename(src)[:-4]

    if not y[-8:].isdecimal() or len(y) not in (8, 17):  # not xxxxxxxx.zip or xxxxxxxx_xxxxxxxx.zip
        print("FAILED: cannot fix unknown zip", src)
        return False

    z = join(dstfolder, y[-8:] + ".zip")

    if z != src:  # relevant to fix

        if fs.exists(z):  # if zip already exists

            if fs.checksum(z) == fs.checksum(src):
                print("zip already exists", z)
                print("removing", src)
                fs.rm(src)
            else:
                print("FAILED: zip already exists but different", src, z)
                return False
        else:
            print("moving", src, "to", z)
            move(fs, src, z)

    return True


def fix_tar(fs: AsyncFileSystem, src: str, dstfolder: str = "") -> bool:
    y = basename(src)[:-4]

    if not y[-8:].isdecimal() or len(y) not in (8, 17):  # not xxxxxxxx.tar or xxxxxxxx_xxxxxxxx.tar
        print("FAILED: cannot fix unknown tar", src)
        return False

    if fs.isfile(z := join(dstfolder, y[-8:] + ".zip")):  # if zip already exist remove tar
        print("removing as zip already exists", src)

    else:  # fix tar to zip
        with fs.open(src, "rb") as srcf:

            if srcf.read(2) == b"PK":  # AHAH YOU'RE A ZIP!
                srcf.close()
                move(fs, src, z)  # rename to zip
                return True

            else:  # convert to zip
                srcf.seek(0)
                try:
                    with fs.open(z, "wb") as dst:
                        with tarfile.open(fileobj=srcf) as srct:
                            with zipfile.ZipFile(dst, "w") as dstf:
                                for f in srct.getnames():
                                    with srct.extractfile(f) as s:
                                        dstf.writestr(f"{y[-8:]}/{f}", s.read())
                except Exception as e:
                    traceback.print_exc()
                    print("FAILED: error while converting tar to zip", src, e)
                    return False

    fs.rm(src)
    return True


def fix_file(fs: AsyncFileSystem, src: str, dstfolder: str = "") -> bool:
    y = basename(src)
    x = y.split(".")[0]

    if not x[-8:].isdecimal() or len(x) not in (8, 17):  # not xxxxxxxx or xxxxxxxx_xxxxxxxx
        print("FAILED: cannot fix unknown file", src)
        return False

    z = join(dstfolder, x[-8:] + ".zip")

    if fs.isfile(z):  # if zip already exist remove file
        print("removing as zip already exist", src)
        fs.rm(src)
        return True

    with fs.open(src, "rb") as srcf:

        if srcf.read(2) == b"PK":  # AHAH YOU'RE A ZIP!
            srcf.close()
            move(fs, src, z)  # rename to zip

        else:
            print("FAILED: unknown file", src)
            return False

    return True


def fs_rmtree(fs: AsyncFileSystem, path: str):
    c = list(fs.listdir(path, detail=False))

    for i in c:
        x = join(path, i)

        if fs.isdir(x):
            fs_rmtree(fs, x)
        else:
            fs.rm(x)

    fs.rmdir(path)


def fix_dir(fs: AsyncFileSystem, src: str, dstfolder: str = "") -> bool:
    y = basename(src)

    if len(y) not in (8, 17) or not y[-8:].isdecimal():  # cannot fix (unable to know how to get the jobID)
        print("FAILED: cannot fix unknown dir", src)
        return False

    z = join(dstfolder, y[-8:] + ".zip")

    if fs.isfile(z):  # if zip already exist remove dir
        print("removing as zip already exist", src)
        fs_rmtree(fs, src)
        return True

    c = list(fs.listdir(src, detail=False))
    if c:
        n = join(src, c[0])

    else:
        print("removing empty dir", src)
        fs.rmdir(src)
        return True

    if len(c) == 1 and (n.endswith((".zip", ".tar")) or '.' not in c[0]):  # 1 file which is a zip or tar or no ext
        u = basename(n).split(".")[0]

        if u[-8:] != y[-8:]:
            print("FAILED: inconsistent job id", n)
            return False

        if z.endswith(".tar"):  # fix tar to zip
            r = fix_tar(fs, n, dstfolder)

        elif z.endswith(".zip"):  # move to parent
            r = fix_zip(fs, n, dstfolder)

        else:  # unknown file
            r = fix_file(fs, n, dstfolder)

        if r:
            print("removing", src)
            fs.rmdir(src)

        return r

    # dezipped job
    with fs.open(z, "wb") as dst:

        with zipfile.ZipFile(dst, "w") as dstf:

            for f in c:

                if fs.isdir(join(src, f)):
                    print("FAILED: unknown dir in dir", src, f)
                    return False

                with fs.open(join(src, f), "rb") as s:

                    if f.endswith((".gz", ".gzip")):
                        df = f"{y[-8:]}/{f.rsplit(".", 1)[0]}"
                        dt = gzip.decompress(s.read())
                    else:
                        df = f"{y[-8:]}/{f}"
                        dt = s.read()
                    dstf.writestr(df, dt)

    fs_rmtree(fs, src)

    return True


class Prod:
    def __init__(self, path: str, fs: AsyncFileSystem):
        self.path = path
        self.fs = fs

    def subprods(self) -> Generator[str, None, None]:

        for i in self.fs.listdir(self.path, detail=False):

            if self.fs.isdir(x := join(self.path, i)) and len(i) == 4 and i.isdecimal():
                yield SubProd(x, self.fs)

    def is_malformed(self, recursive: bool = True) -> bool:

        ls = self.fs.listdir(self.path, detail=False)
        ls.sort(key=len, reverse=True)  # longest first, may put files before dirs, faster

        for i in ls:

            if self.fs.isdir(x := join(self.path, i)) and len(i) == 4 and i.isdecimal():
                if recursive and SubProd(x, self.fs).is_malformed():
                    return True
            else:
                return True
        return False

    def _check_subprod(self, x: str, end: int | None = None) -> str:
        y = basename(x)[:end]

        if len(y) not in (8, 17) or not y[-8:].isdecimal():  # cannot fix (unable to know how to get the jobID)
            print("FAILED: cannot fix unknown dir", x)
            return ""

        subprod = join(self.path, y[-8:-4])
        if not self.fs.isdir(subprod):  # create subprod if not exist
            try:
                self.fs.mkdir(subprod)
            except Exception as e:
                traceback.print_exc()
                print(f"FAILED: to create ({e})", subprod)
                return ""

        return subprod

    def fix(self):
        r = True

        for i in self.fs.listdir(self.path, detail=False):

            if self.fs.isdir(x := join(self.path, i)):

                if len(i) == 4 and i.isdecimal():
                    r &= SubProd(x, self.fs).fix()
                else:
                    subprod = self._check_subprod(x)

                    if not subprod:
                        r = False
                        continue

                    r &= fix_dir(self.fs, x, subprod)

            elif self.fs.isfile(x):

                if x.endswith((".tar", ".zip")) or '.' not in i:
                    subprod = self._check_subprod(x, -4)

                    if not subprod:
                        r = False
                        continue

                    if x.endswith(".tar"):
                        r &= fix_tar(self.fs, x, subprod)
                    elif x.endswith(".zip"):
                        r &= fix_zip(self.fs, x, subprod)
                    else:
                        r &= fix_file(self.fs, x, subprod)
                else:
                    print("FAILED: cannot fix unknown file", x)
                    r = False
            else:
                print("FAILED: unknown obj", x)
                r = False
        return r


class SubProd:
    def __init__(self, path: str, fs: AsyncFileSystem):
        self.path = path
        self.fs = fs

    def is_malformed(self, verbose: bool = True) -> bool:

        for i in self.fs.listdir(self.path, detail=False):
            x = join(self.path, i)

            if not x.endswith(".zip"):
                if verbose:
                    print("unknown file", x)
                return True

            if not (k := basename(x)[:-4]).isdecimal() or len(k) != 8:
                if verbose:
                    print("badly named file", x)
                return True

            if not self.fs.isfile(x):
                if verbose:
                    print("unknown dir", x)
                return True

        return False

    def fix(self) -> bool:
        r = True

        for i in self.fs.listdir(self.path, detail=False):
            x = join(self.path, i)

            if self.fs.isfile(x):  # check if bad file

                if x.endswith(".tar"):  # fix tar to zip
                    r &= fix_tar(self.fs, x, self.path)

                elif x.endswith(".zip"):  # fix zip
                    r &= fix_zip(self.fs, x, self.path)

                elif '.' not in i:  # fix file
                    r &= fix_file(self.fs, x, self.path)

                else:
                    r = False
                    print("FAILED: cannot fix unknown file", x)

            elif self.fs.isdir(x):
                r &= fix_dir(self.fs, x, self.path)

            else:
                r = False
                print("FAILED: unknown obj", x)

        return r


class FS:

    def __init__(self, path: str = BASE):
        fs: AsyncFileSystem
        fs, self.path = fsspec.core.url_to_fs(f"root://eoslhcb.cern.ch/{path}?xrd.wantprot=unix&authz={token}")
        self.fs = fs

    def prods(self) -> Generator[Prod, None, None]:

        for p in PATH:

            for i in self.fs.listdir(x := join(self.path, p), detail=False):

                for u in self.fs.listdir(y := join(x, i, "LOG"), detail=False):

                    yield Prod(join(y, u), self.fs)


    def fetch_invalid(self):
        with open("invalid.json", "r") as f:
            invalid = json.load(f)

        assert isinstance(invalid, list)

        end = invalid[-1]
        start = False

        r = list(self.prods())
        x = 0

        for n, p in enumerate(r):
            if p.path == "/eos/lhcb/grid/prod/lhcb/logSE/lhcb/MC/expected-2024.Q1.2/LOG/00218035":
                start = True
                continue

            print(n, p.path)
            if start and p.is_malformed():
                invalid.append(p.path)
                x += 1
                if x > 0:
                    x = 0
                    with open("invalid.json", "w") as f:
                        json.dump(invalid, f)


if __name__ == '__main__':
    fs = FS()

    # Get the list of prods
    #print("list prod...")
    #p = set(fs.list_prod())
    #print(f"found {len(p)} prods")
    #print(list(p)[:10])

    # Get the list of malformed prods
    #print("check malformed...")
    #m = []
    #for i in p:
    #    if fs.is_malformed(i):
    #        m.append(i)
    #print(f"found {len(m)} malformed prods")

    p = Prod('/eos/lhcb/grid/prod/lhcb/logSE/lhcb/MC/2017/LOG/00172175', fs.fs)

    import code
    code.interact(local=locals())
