"""This module provides the Zstandard decompressor wrapper class.

Classes:
    - ZstandardDecompressor: Zstandard decompressor wrapper class.
"""

from collections.abc import Iterator
from typing import BinaryIO, override

from zstandard import ZstdDecompressor

from .processor import ZstandardProcessor

__all__ = ("ZstandardDecompressor",)


class ZstandardDecompressor(ZstandardProcessor[ZstdDecompressor]):
    """Wrapper for Zstandard decompressor.

    Performs data decompression using Zstandard, featuring extra functionality:
        - zstd-context management (auto decompressor reinstantiation)
        - switchable dictionary

    Notes:
        - not thread-safe
        - dictionary switching requires reinstantiation of the internal context processor (slow)
    """

    __slots__ = ()

    @override
    def _new(self) -> ZstdDecompressor:
        return ZstdDecompressor(dict_data=None if self._dict is None else self._dict.dict)

    @override
    def process(self, *data: bytes | bytearray | memoryview) -> bytes | Iterator[bytes]:
        if len(data) == 1:
            return bytes(self.processor.decompress(data[0])) if data[0] else b""
        return (bytes(self.processor.decompress(i)) if i else b"" for i in data)

    @override
    def process_stream(
        self,
        instream: BinaryIO,
        outstream: BinaryIO,
        insize: int = -1,
    ) -> tuple[int, int]:
        """Process the provided stream.

        Args:
            instream: the stream to process
            outstream: the stream to write the processed data to
            insize: ignored

        Returns:
            a tuple with the number of bytes read and written
        """
        r: tuple[int, int] = self.processor.copy_stream(instream, outstream, self._config.stream_read_buffer, self._config.stream_write_buffer)
        return r

    decompress = process
    decompress_stream = process_stream
