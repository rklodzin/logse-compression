"""
Testing providers.
"""

import pytest

from lhcbdirac_log import (
    DataExistsError,
    DataProvider,
    DictExistsError,
    DictInvalidError,
    DictNotExistsError,
    DictProvider,
    JobExistsError,
    JobNotExistsError, ReadOnlyError, )
from lhcbdirac_log.providers.database import SQLDataProvider, SQLDictProvider, SQLDriver, SQLDataIO
from lhcbdirac_log.providers.filesystem import FSDictEntry, FSDataProvider, FSDictProvider
from lhcbdirac_log.providers.fsspec import RootDataProvider
from lhcbdirac_log.providers.memory import MemoryDataProvider, MemoryDictProvider


class TestDataProvider:

    def test_mem_clear(self) -> None:
        MemoryDataProvider("aaa")
        MemoryDataProvider("bbb")
        MemoryDataProvider("ccc")

        assert MemoryDataProvider.CACHE == {"aaa": {}, "bbb": {}, "ccc": {}}
        MemoryDataProvider.clear_cache("aaa")
        assert MemoryDataProvider.CACHE == {"bbb": {}, "ccc": {}}
        MemoryDataProvider.clear_cache()
        assert MemoryDataProvider.CACHE == {}

    def test_sql_init(self) -> None:
        # nothing
        d = SQLDataProvider()
        x = d.dict_provider
        r = d.driver

        assert r is not None
        assert isinstance(x, SQLDictProvider)
        assert x.driver is r

        # with dict provider
        x = SQLDictProvider(r)
        d = SQLDataProvider(dict_provider=x)
        r = d.driver

        assert r is not None
        assert isinstance(x, SQLDictProvider)
        assert x.driver is r

        # with dict provider and bad driver
        x = SQLDictProvider(SQLDriver.create())
        with pytest.raises(ValueError):
            _ = SQLDataProvider(r, x)

    def test_sql_integrity(self) -> None:
        d = SQLDataProvider()

        with pytest.raises(DictNotExistsError):
            with d.create(42).create("data").writer() as w:
                pass

    def test_sql_overflow(self) -> None:
        d = SQLDataProvider()
        d.dict_provider.mark_invalid("data")
        with d.create(42).create("data").writer() as w:
            w: SQLDataIO
            w._write_buffer_size = 1 << 20
            with pytest.raises(BufferError):
                w.write(b"x" * (1 << 20))

    def test_getter(self, any_non_empty_data_provider: DataProvider):  # for coverage
        if isinstance(any_non_empty_data_provider, RootDataProvider | SQLDataProvider):
            assert any_non_empty_data_provider.driver is not None
            r = any_non_empty_data_provider.get(42)
            assert r.driver is not None

            d = any_non_empty_data_provider.dict_provider
            if isinstance(d, SQLDictProvider):
                assert d.driver is not None

        if isinstance(any_non_empty_data_provider, FSDataProvider):
            assert any_non_empty_data_provider.folder is not None

            d = any_non_empty_data_provider.dict_provider
            if isinstance(d, FSDictProvider):
                assert d.folder is not None

    def test_get0(self, any_readonly_data_provider: DataProvider):
        with pytest.raises(ReadOnlyError):  # get non-existing with create
            any_readonly_data_provider.get(0, create=True)

        with pytest.raises(JobNotExistsError):  # get non-existing job
            any_readonly_data_provider.get(0)

    def test_get1(self, readonly_data_provider: DataProvider):
        with pytest.raises(ReadOnlyError):  # get existing with create
            readonly_data_provider.get(42, create=True)

        j = readonly_data_provider.get(42)  # get existing job
        assert readonly_data_provider[42].job == 42  # __getitem__
        assert j.job == 42
        assert j.readonly

    def test_get2(self, empty_data_provider: DataProvider):
        with pytest.raises(JobNotExistsError):  # get non-existing job
            empty_data_provider.get(42)

        j = empty_data_provider.get(42, create=True)  # create
        assert j.job == 42
        assert j.job == empty_data_provider.get(42).job
        assert j.job == empty_data_provider.get(42, create=True).job
        assert j.job == empty_data_provider.create(42, exists_ok=True).job

        with pytest.raises(JobExistsError):  # already exists
            empty_data_provider.create(42)

        with pytest.raises(JobNotExistsError):  # not exists
            empty_data_provider.get(24)

    def test_create0(self, any_readonly_data_provider: DataProvider):
        with pytest.raises(ReadOnlyError):  # non-existing job
            any_readonly_data_provider.create(24)

    def test_create1(self, readonly_data_provider: DataProvider):
        with pytest.raises(ReadOnlyError):  # existing job
            readonly_data_provider.create(42)

    def test_create2(self, empty_data_provider: DataProvider):
        j = empty_data_provider.create(42)  # create non-existing job
        n = empty_data_provider.create(24, exists_ok=True)  # create non-existing job

        with pytest.raises(JobExistsError):  # already exists
            empty_data_provider.create(42)

        with pytest.raises(JobExistsError):  # already exists
            empty_data_provider.create(24)

        c = empty_data_provider.create(42, exists_ok=True)  # exists ok
        g = empty_data_provider.get(42)  # exists
        v = empty_data_provider.get(42, create=True)  # exists
        assert j.job == c.job == g.job == v.job == 42

        x = empty_data_provider.create(24, exists_ok=True)  # exists ok
        y = empty_data_provider.get(24)  # exists
        z = empty_data_provider.get(24, create=True)  # exists
        assert n.job == x.job == y.job == z.job == 24

    def test_delete0(self, any_readonly_data_provider: DataProvider):
        # non-existing job
        with pytest.raises(ReadOnlyError):
            any_readonly_data_provider.delete(24)

        with pytest.raises(ReadOnlyError):
            any_readonly_data_provider.delete(24, force=True)

    def test_delete1(self, readonly_data_provider: DataProvider):
        # existing job non-empty
        with pytest.raises(ReadOnlyError):
            readonly_data_provider.delete(42)

        with pytest.raises(ReadOnlyError):
            readonly_data_provider.delete(42, force=True)

        # existing job empty
        with pytest.raises(ReadOnlyError):
            readonly_data_provider.delete(64)

        with pytest.raises(ReadOnlyError):
            readonly_data_provider.delete(64, force=True)

    def test_delete2(self, empty_data_provider: DataProvider, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        # delete non-existing job
        with pytest.raises(JobNotExistsError):
            empty_data_provider.delete(42)

        with pytest.raises(JobNotExistsError):
            empty_data_provider.delete(42, force=True)

        if empty_data_provider.compressed:
            empty_data_provider.dict_provider.add("data", all_dict_data[0][0], all_dict_data[0][2])

        for force in (False, True):
            # delete existing empty job
            assert empty_data_provider.create(42).job == 42

            empty_data_provider.delete(42, force=force)

            with pytest.raises(JobNotExistsError):
                empty_data_provider.get(42)

            with pytest.raises(JobNotExistsError):
                empty_data_provider.delete(42)

            with pytest.raises(JobNotExistsError):
                empty_data_provider.delete(42, force=True)

            # delete existing non-empty job
            j = empty_data_provider.create(42)

            d = j.create("data")
            with d.writer() as w:
                w.write(b"")

            with pytest.raises(DataExistsError):
                empty_data_provider.delete(42)

            assert empty_data_provider.get(42).job == 42

            empty_data_provider.delete(42, force=True)

            with pytest.raises(JobNotExistsError):
                empty_data_provider.get(42)

            with pytest.raises(JobNotExistsError):
                empty_data_provider.delete(42)

            with pytest.raises(JobNotExistsError):
                empty_data_provider.delete(42, force=True)

    def test_jobs0(self, empty_readonly_data_provider: DataProvider):
        assert set(empty_readonly_data_provider.jobs()) == set()
        assert {i.job for i in empty_readonly_data_provider} == set()
        assert len(empty_readonly_data_provider) == 0
        assert empty_readonly_data_provider.empty

    def test_jobs1(self, readonly_data_provider: DataProvider):
        assert set(readonly_data_provider.jobs()) == {42, 64}
        assert {i.job for i in readonly_data_provider} == {42, 64}
        assert len(readonly_data_provider) == 2
        assert not readonly_data_provider.empty

    def test_jobs2(self, empty_data_provider: DataProvider):
        assert set(empty_data_provider.jobs()) == set()
        assert {i.job for i in empty_data_provider} == set()
        assert len(empty_data_provider) == 0
        assert empty_data_provider.empty

        empty_data_provider.create(42)
        assert set(empty_data_provider.jobs()) == {42}
        assert {i.job for i in empty_data_provider} == {42}
        assert len(empty_data_provider) == 1
        assert not empty_data_provider.empty

        empty_data_provider.create(24)
        assert set(empty_data_provider.jobs()) == {42, 24}
        assert {i.job for i in empty_data_provider} == {42, 24}
        assert len(empty_data_provider) == 2
        assert not empty_data_provider.empty

        empty_data_provider.delete(42)
        assert set(empty_data_provider.jobs()) == {24}
        assert {i.job for i in empty_data_provider} == {24}
        assert len(empty_data_provider) == 1
        assert not empty_data_provider.empty

        empty_data_provider.delete(24)
        assert set(empty_data_provider.jobs()) == set()
        assert {i.job for i in empty_data_provider} == set()
        assert len(empty_data_provider) == 0
        assert empty_data_provider.empty

    def test_clear0(self, any_readonly_data_provider: DataProvider):
        with pytest.raises(ReadOnlyError):
            any_readonly_data_provider.clear()

        with pytest.raises(ReadOnlyError):
            any_readonly_data_provider.clear(force=True)

    def test_clear1(self, empty_data_provider: DataProvider, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        # clear no jobs
        empty_data_provider.clear()
        assert empty_data_provider.empty

        empty_data_provider.clear(force=True)
        assert empty_data_provider.empty

        # clear empty jobs
        empty_data_provider.create(42)
        empty_data_provider.create(24)
        empty_data_provider.clear()
        assert empty_data_provider.empty

        empty_data_provider.create(42)
        empty_data_provider.create(24)
        empty_data_provider.clear(force=True)
        assert empty_data_provider.empty

        # clear non-empty jobs
        j = empty_data_provider.create(42)

        if empty_data_provider.compressed:
            empty_data_provider.dict_provider.add("data", all_dict_data[0][0], all_dict_data[0][2])

        d = j.create("data")
        with d.writer() as w:
            w.write(b"")

        with pytest.raises(DataExistsError):
            empty_data_provider.clear()

        assert not empty_data_provider.empty

        empty_data_provider.clear(force=True)
        assert empty_data_provider.empty

    def test_data_size0(self, empty_readonly_data_provider: DataProvider):
        assert empty_readonly_data_provider.data_size == 0

    def test_data_size1(self, any_non_empty_data_provider: DataProvider):
        assert any_non_empty_data_provider.data_size == 42
        assert any_non_empty_data_provider.size != 0

    def test_data_size2(self, empty_data_provider: DataProvider, bytes_data: bytes, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        assert empty_data_provider.data_size == 0

        j = empty_data_provider.create(42)
        assert empty_data_provider.data_size == 0

        if empty_data_provider.compressed:
            empty_data_provider.dict_provider.add("data", all_dict_data[0][0], all_dict_data[0][2])
            empty_data_provider.dict_provider.add("datax", all_dict_data[1][0], all_dict_data[1][2])

        d = j.create("data")
        with d.writer() as w:
            w.write(b"")

        d = j.create("data2")
        with d.writer() as w:
            w.write(bytes_data)

        assert empty_data_provider.data_size == 42
        assert empty_data_provider.size != 0

        empty_data_provider.clear(force=True)

        assert empty_data_provider.data_size == 0

    def test_transfer0(self, any_data_provider: DataProvider,
                       any_readonly_data_provider: DataProvider):
        if any_data_provider.compressed == any_readonly_data_provider.compressed:
            with pytest.raises(ReadOnlyError):
                any_data_provider.transfer(any_readonly_data_provider)

    def test_transfer1(self,
                       empty_data_provider: DataProvider,
                       alt_empty_data_provider: DataProvider,
                       bytes_data: bytes,
                       all_dict_data: tuple[tuple[bytes, int, int], ...]
                       ):
        # populate the data provider
        j = empty_data_provider.create(42)

        if empty_data_provider.compressed:
            empty_data_provider.dict_provider.add("datax", all_dict_data[0][0], all_dict_data[0][2])
            empty_data_provider.dict_provider.mark_invalid("dxxx")

        with j.create("data0").writer() as w:
            w.write(bytes_data)

        with j.create("data1").writer() as w:
            w.write(b"aaa")

        with j.create("d042").writer() as w:
            w.write(b"")

        empty_data_provider.create(24)

        empty_data_provider.transfer(empty_data_provider)  # do nothing
        assert set(empty_data_provider.jobs()) == {42, 24}
        assert empty_data_provider.data_size == 45

        if empty_data_provider.compressed != alt_empty_data_provider.compressed:
            with pytest.raises(ValueError):
                empty_data_provider.transfer(alt_empty_data_provider)

        else:
            if empty_data_provider.compressed:
                empty_data_provider.dict_provider.add("bbb", all_dict_data[1][0], all_dict_data[1][2])
                empty_data_provider.dict_provider.add("ccc", all_dict_data[2][0], all_dict_data[2][2])
                assert set(empty_data_provider.dict_provider) == {"bbb", "ccc", "datax"}
                assert empty_data_provider.dict_provider.size == sum(d[1] for d in all_dict_data)

            empty_data_provider.transfer(alt_empty_data_provider)

            assert set(empty_data_provider.jobs()) == {42, 24}
            assert set(empty_data_provider.get(42).files()) == {"data0", "data1", "d042"}
            assert empty_data_provider.data_size == 45

            assert set(alt_empty_data_provider.jobs()) == {42, 24}
            assert set(alt_empty_data_provider.get(42).files()) == {"data0", "data1", "d042"}
            assert alt_empty_data_provider.data_size == 45

            if empty_data_provider.compressed:
                assert set(empty_data_provider.dict_provider) == {"bbb", "ccc", "datax"}
                assert empty_data_provider.dict_provider.size == sum(d[1] for d in all_dict_data)

                assert set(alt_empty_data_provider.dict_provider) == {"bbb", "ccc", "datax"}
                assert alt_empty_data_provider.dict_provider.size == sum(d[1] for d in all_dict_data)

                with pytest.raises(DictExistsError):
                    empty_data_provider.transfer(alt_empty_data_provider)

                alt_empty_data_provider.clear(force=True)
                alt_empty_data_provider.dict_provider.clear()
                alt_empty_data_provider.create(42)

            with pytest.raises(JobExistsError):
                empty_data_provider.transfer(alt_empty_data_provider)

    def test_transfer2(self,
                       empty_data_provider: DataProvider,
                       alt_empty_data_provider: DataProvider,
                       ) -> None:

        if empty_data_provider.compressed != alt_empty_data_provider.compressed:
            with pytest.raises(ValueError):
                empty_data_provider.transfer(alt_empty_data_provider)

        else:
            for i in range(10):
                empty_data_provider.create(i)

            assert empty_data_provider.transfer(alt_empty_data_provider, limit=5) == 5
            assert len(set(alt_empty_data_provider.jobs()))


class TestDictProvider:
    def test_mem_clear(self) -> None:
        MemoryDictProvider("aaa")
        MemoryDictProvider("bbb")
        MemoryDictProvider("ccc")

        assert MemoryDictProvider.CACHE == {"aaa": {}, "bbb": {}, "ccc": {}}
        MemoryDictProvider.clear_cache("aaa")
        assert MemoryDictProvider.CACHE == {"bbb": {}, "ccc": {}}
        MemoryDictProvider.clear_cache()
        assert MemoryDictProvider.CACHE == {}

    def test_add0(self, any_readonly_dict_provider: DictProvider, dict_data: tuple[bytes, int, int]):
        # test add readonly
        assert not any_readonly_dict_provider.is_loaded("aaa")

        with pytest.raises(ReadOnlyError):
            any_readonly_dict_provider.add("aaa", dict_data[0], dict_data[2])

        assert not any_readonly_dict_provider.is_loaded("aaa")

    def test_add1(self, dict_provider: DictProvider, dict_data: tuple[bytes, int, int]):
        # test add existing
        assert not dict_provider.is_loaded("datax")

        with pytest.raises(DictExistsError):
            dict_provider.add("datax", dict_data[0], dict_data[2])

        assert not dict_provider.is_loaded("datax")

    def test_add2(self, readonly_dict_provider: DictProvider, dict_data: tuple[bytes, int, int]):
        # test add existing
        assert not readonly_dict_provider.is_loaded("datax")

        with pytest.raises(ReadOnlyError):
            readonly_dict_provider.add("datax", dict_data[0], dict_data[2])

        assert not readonly_dict_provider.is_loaded("datax")

    def test_add3(self, empty_dict_provider: DictProvider, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        # test add non-existing
        assert not empty_dict_provider.is_loaded("aaa")
        empty_dict_provider.add("aaa", all_dict_data[0][0], all_dict_data[0][2])
        assert empty_dict_provider.is_loaded("aaa")

        with pytest.raises(DictExistsError):
            empty_dict_provider.add("aaa", all_dict_data[0][0], all_dict_data[0][2])

        # test add invalid dict
        empty_dict_provider.mark_invalid("iii")
        assert empty_dict_provider.is_invalid("iii")

        empty_dict_provider.add("iii", all_dict_data[1][0], all_dict_data[1][2])
        assert not empty_dict_provider.is_invalid("iii")

        # test add missing dict
        empty_dict_provider.mark_missing('mmm')
        assert empty_dict_provider.is_missing("mmm")

        empty_dict_provider.add("mmm", all_dict_data[2][0], all_dict_data[2][2])
        assert not empty_dict_provider.is_missing("mmm")

    def test_flag0(self, any_non_readonly_dict_provider: DictProvider, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        assert not any_non_readonly_dict_provider.is_loaded("xxx")
        assert not any_non_readonly_dict_provider.is_missing("xxx")
        assert not any_non_readonly_dict_provider.is_invalid("xxx")

        any_non_readonly_dict_provider.add("aaa", all_dict_data[0][0], all_dict_data[0][2])

        any_non_readonly_dict_provider.mark_missing("mmm")  # call on unflagged
        assert any_non_readonly_dict_provider.is_missing("mmm")
        assert not any_non_readonly_dict_provider.is_invalid("mmm")
        assert not any_non_readonly_dict_provider.is_loaded("mmm")

        any_non_readonly_dict_provider.mark_invalid("iii")  # call on unflagged
        assert not any_non_readonly_dict_provider.is_missing("iii")
        assert any_non_readonly_dict_provider.is_invalid("iii")
        assert not any_non_readonly_dict_provider.is_loaded("iii")

        any_non_readonly_dict_provider.mark_invalid("mmm")  # call on missing
        assert not any_non_readonly_dict_provider.is_missing("mmm")
        assert any_non_readonly_dict_provider.is_invalid("mmm")
        assert not any_non_readonly_dict_provider.is_loaded("mmm")

        with pytest.raises(DictInvalidError):
            any_non_readonly_dict_provider.mark_missing("iii")  # call on invalid
        assert not any_non_readonly_dict_provider.is_missing("iii")
        assert any_non_readonly_dict_provider.is_invalid("iii")
        assert not any_non_readonly_dict_provider.is_loaded("iii")

        with pytest.raises(DictExistsError):
            any_non_readonly_dict_provider.mark_missing("aaa")  # call on loaded
        assert not any_non_readonly_dict_provider.is_missing("aaa")
        assert not any_non_readonly_dict_provider.is_invalid("aaa")
        assert any_non_readonly_dict_provider.is_loaded("aaa")

        with pytest.raises(DictExistsError):
            any_non_readonly_dict_provider.mark_invalid("aaa")  # call on loaded
        assert not any_non_readonly_dict_provider.is_missing("aaa")
        assert not any_non_readonly_dict_provider.is_invalid("aaa")
        assert any_non_readonly_dict_provider.is_loaded("aaa")

        any_non_readonly_dict_provider.mark_missing("MMM")
        any_non_readonly_dict_provider.mark_invalid("III")

        any_non_readonly_dict_provider.add("MMM", all_dict_data[1][0], all_dict_data[1][2])  # test unflag on add
        any_non_readonly_dict_provider.add("III", all_dict_data[2][0], all_dict_data[2][2])  # test unflag on add

        assert not any_non_readonly_dict_provider.is_missing("MMM")
        assert not any_non_readonly_dict_provider.is_invalid("MMM")
        assert any_non_readonly_dict_provider.is_loaded("MMM")

        assert not any_non_readonly_dict_provider.is_missing("III")
        assert not any_non_readonly_dict_provider.is_invalid("III")
        assert any_non_readonly_dict_provider.is_loaded("III")

    def test_flag1(self, any_non_readonly_dict_provider: DictProvider) -> None:
        m = any_non_readonly_dict_provider.add("mmm", b"", 42)
        i = any_non_readonly_dict_provider.add("iii", b"", 42)

        any_non_readonly_dict_provider.delete('mmm')
        any_non_readonly_dict_provider.delete('iii')

        # test mark_* unload, may never happen in normal situations
        any_non_readonly_dict_provider._dicts['mmm'] = m
        any_non_readonly_dict_provider._dicts['iii'] = i

        assert any_non_readonly_dict_provider['mmm'] is m
        assert any_non_readonly_dict_provider['iii'] is i

        assert any_non_readonly_dict_provider.is_loaded('mmm')
        assert any_non_readonly_dict_provider.is_loaded('iii')
        assert not any_non_readonly_dict_provider.is_missing('mmm')
        assert not any_non_readonly_dict_provider.is_missing('iii')
        assert not any_non_readonly_dict_provider.is_invalid('mmm')
        assert not any_non_readonly_dict_provider.is_invalid('iii')

        any_non_readonly_dict_provider.mark_invalid('iii')
        any_non_readonly_dict_provider.mark_missing('mmm')

        assert not any_non_readonly_dict_provider.is_loaded('mmm')
        assert not any_non_readonly_dict_provider.is_loaded('iii')
        assert any_non_readonly_dict_provider.is_missing('mmm')
        assert not any_non_readonly_dict_provider.is_missing('iii')
        assert not any_non_readonly_dict_provider.is_invalid('mmm')
        assert any_non_readonly_dict_provider.is_invalid('iii')

    def test_delete0(self, readonly_dict_provider: DictProvider):
        with pytest.raises(ReadOnlyError):
            readonly_dict_provider.delete("datax")  # delete non-existing

    def test_delete1(self, empty_readonly_dict_provider: DictProvider):
        with pytest.raises(ReadOnlyError):
            empty_readonly_dict_provider.delete("datax")  # delete non-existing

    def test_delete2(self, empty_dict_provider: DictProvider, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        with pytest.raises(DictNotExistsError):
            empty_dict_provider.delete("aaa")  # delete non-existing

        empty_dict_provider.add("aaa", all_dict_data[0][0], all_dict_data[0][2])
        empty_dict_provider.add("xxx", all_dict_data[1][0], all_dict_data[1][2])

        del empty_dict_provider._dicts["xxx"]  # force uncaching: simulate unloaded dict
        assert not empty_dict_provider.is_loaded("xxx")

        empty_dict_provider.delete("aaa")  # delete existing
        empty_dict_provider.delete("xxx")  # delete existing not loaded

        assert not empty_dict_provider.is_loaded("aaa")
        assert not empty_dict_provider.is_loaded("xxx")

        with pytest.raises(DictNotExistsError):
            empty_dict_provider.delete("aaa")  # delete no longer existing

        with pytest.raises(DictNotExistsError):
            empty_dict_provider.delete("xxx")  # delete no longer existing

        # using del[] alias
        with pytest.raises(DictNotExistsError):
            del empty_dict_provider["bbb"]  # delete non-existing

        empty_dict_provider.add("bbb", all_dict_data[2][0], all_dict_data[2][2])
        empty_dict_provider.add("yyy", all_dict_data[0][0], all_dict_data[0][2])

        del empty_dict_provider._dicts["yyy"]  # force uncaching: simulate unloaded dict
        assert not empty_dict_provider.is_loaded("yyy")

        del empty_dict_provider["bbb"]  # delete existing
        del empty_dict_provider["yyy"]  # delete existing not loaded

        assert not empty_dict_provider.is_loaded("bbb")
        assert not empty_dict_provider.is_loaded("yyy")

        with pytest.raises(DictNotExistsError):
            del empty_dict_provider["bbb"]  # delete no longer existing

        with pytest.raises(DictNotExistsError):
            del empty_dict_provider["yyy"]  # delete no longer existing

    def test_clear0(self, any_readonly_dict_provider: DictProvider):
        with pytest.raises(ReadOnlyError):
            any_readonly_dict_provider.clear()  # empty or not

    def test_clear1(self, any_non_readonly_dict_provider: DictProvider, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        any_non_readonly_dict_provider.clear()  # clear empty
        assert any_non_readonly_dict_provider.empty

        any_non_readonly_dict_provider.add("aaa", all_dict_data[0][0], all_dict_data[0][2])
        any_non_readonly_dict_provider.add("bbb", all_dict_data[1][0], all_dict_data[1][2])
        any_non_readonly_dict_provider.add("ccc", all_dict_data[2][0], all_dict_data[2][2])
        assert not any_non_readonly_dict_provider.empty

        any_non_readonly_dict_provider.clear()  # clear non-empty
        assert any_non_readonly_dict_provider.empty

        assert not any_non_readonly_dict_provider.is_loaded("aaa")
        assert not any_non_readonly_dict_provider.is_loaded("bbb")
        assert not any_non_readonly_dict_provider.is_loaded("ccc")

    def test_size0(self, any_empty_dict_provider: DictProvider):
        assert any_empty_dict_provider.size == 0
        assert DictProvider.size.fget(any_empty_dict_provider) == 0  # check default impl

    def test_size1(self, any_non_empty_dict_provider: DictProvider,
                   all_dict_data: tuple[tuple[bytes, int, int], ...]) -> None:
        assert any_non_empty_dict_provider.size == all_dict_data[0][1] + all_dict_data[1][1]
        assert DictProvider.size.fget(any_non_empty_dict_provider) == all_dict_data[0][1] + all_dict_data[1][1]  # check default impl

    def test_size2(self, empty_dict_provider: DictProvider, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        assert empty_dict_provider.size == 0
        assert DictProvider.size.fget(empty_dict_provider) == 0  # check default impl

        empty_dict_provider.add("aaa", all_dict_data[0][0], all_dict_data[0][2])
        empty_dict_provider.add("bbb", all_dict_data[1][0], all_dict_data[1][2])
        empty_dict_provider.add("ccc", all_dict_data[2][0], all_dict_data[2][2])

        assert empty_dict_provider.size == sum(d[1] for d in all_dict_data)
        assert DictProvider.size.fget(empty_dict_provider) == sum(d[1] for d in all_dict_data)  # check default impl

        empty_dict_provider.clear()

        assert empty_dict_provider.size == 0
        assert DictProvider.size.fget(empty_dict_provider) == 0  # check default impl

    def test_iter0(self, any_empty_dict_provider: DictProvider):
        assert set(any_empty_dict_provider.iter()) == set()
        assert set(any_empty_dict_provider.iter(loaded_only=True)) == set()
        assert set(any_empty_dict_provider) == set()
        assert len(any_empty_dict_provider) == 0
        assert any_empty_dict_provider.empty

    def test_iter1(self, any_non_empty_dict_provider: DictProvider):
        assert set(any_non_empty_dict_provider.iter()) == {"data", "datax"}
        assert set(any_non_empty_dict_provider.iter(loaded_only=True)) == set()
        assert set(any_non_empty_dict_provider) == {"data", "datax"}
        assert len(any_non_empty_dict_provider) == 2
        assert not any_non_empty_dict_provider.empty
        _ = any_non_empty_dict_provider["data"]  # load
        assert set(any_non_empty_dict_provider.iter(loaded_only=True)) == {"data"}
        _ = any_non_empty_dict_provider["datax"]  # load
        assert set(any_non_empty_dict_provider.iter(loaded_only=True)) == {"data", "datax"}

    def test_iter2(self, empty_dict_provider: DictProvider, all_dict_data: tuple[tuple[bytes, int, int], ...]):
        empty_dict_provider.add("aaa", all_dict_data[0][0], all_dict_data[0][2])
        assert set(empty_dict_provider.iter()) == {"aaa"}
        assert set(empty_dict_provider.iter(loaded_only=True)) == {"aaa"}
        assert set(empty_dict_provider) == {"aaa"}
        assert len(empty_dict_provider) == 1
        assert not empty_dict_provider.empty
        assert empty_dict_provider.size == all_dict_data[0][1]

        empty_dict_provider.add("bbb", all_dict_data[1][0], all_dict_data[1][2])
        assert set(empty_dict_provider.iter()) == {"aaa", "bbb"}
        assert set(empty_dict_provider.iter(loaded_only=True)) == {"aaa", "bbb"}
        assert set(empty_dict_provider) == {"aaa", "bbb"}
        assert len(empty_dict_provider) == 2
        assert not empty_dict_provider.empty
        assert empty_dict_provider.size == all_dict_data[0][1] + all_dict_data[1][1]

        assert empty_dict_provider.is_loaded("bbb")
        del empty_dict_provider._dicts["bbb"]  # simulate unloaded dict
        assert not empty_dict_provider.is_loaded("bbb")

        assert set(empty_dict_provider.iter()) == {"aaa", "bbb"}
        assert set(empty_dict_provider.iter(loaded_only=True)) == {"aaa"}
        assert set(empty_dict_provider) == {"aaa", "bbb"}
        assert len(empty_dict_provider) == 2
        assert not empty_dict_provider.empty
        assert not empty_dict_provider.is_loaded("bbb")
        assert empty_dict_provider.size == all_dict_data[0][1] + all_dict_data[1][1]

        empty_dict_provider.clear()

        assert set(empty_dict_provider.iter()) == set()
        assert set(empty_dict_provider.iter(loaded_only=True)) == set()
        assert set(empty_dict_provider) == set()
        assert len(empty_dict_provider) == 0
        assert empty_dict_provider.empty
        assert empty_dict_provider.size == 0

    def test_getitem(self, any_non_empty_dict_provider: DictProvider):
        with pytest.raises(DictNotExistsError):
            _ = any_non_empty_dict_provider["xxx"]  # non-existing

        assert any_non_empty_dict_provider.is_missing("xxx")

        assert any_non_empty_dict_provider["datax"].name == "datax"  # existing and loaded
        assert any_non_empty_dict_provider["datax"].name == "datax"  # existing and unloaded

        assert any_non_empty_dict_provider["datax"] is any_non_empty_dict_provider["datax"]  # cached

        any_non_empty_dict_provider.mark_missing("mmm")  # missing
        any_non_empty_dict_provider.mark_invalid("iii")  # invalid

        with pytest.raises(DictNotExistsError):
            _ = any_non_empty_dict_provider["mmm"]  # missing

        with pytest.raises(DictInvalidError):
            _ = any_non_empty_dict_provider["iii"]  # invalid

    def test_get00(self, any_non_empty_dict_provider: DictProvider):
        none = object.__new__(FSDictEntry)  # used as typed None
        none._name = "none"

        with pytest.raises(DictNotExistsError):
            _ = any_non_empty_dict_provider.get("xxx", none, invalid_ok=False, missing_ok=False)

        assert any_non_empty_dict_provider.is_missing("xxx")

        assert not any_non_empty_dict_provider.is_loaded("datax")
        assert (d := any_non_empty_dict_provider.get("datax", none, invalid_ok=False, missing_ok=False)) is not none  # existing and unloaded
        assert d.name == "datax"
        assert (d := any_non_empty_dict_provider.get("datax", none, invalid_ok=False, missing_ok=False)) is not none  # existing and loaded
        assert d.name == "datax"

        any_non_empty_dict_provider.mark_missing("mmm")  # missing
        any_non_empty_dict_provider.mark_invalid("iii")  # invalid

        with pytest.raises(DictNotExistsError):
            _ = any_non_empty_dict_provider.get("mmm", none, invalid_ok=False, missing_ok=False)

        with pytest.raises(DictInvalidError):
            _ = any_non_empty_dict_provider.get("iii", none, invalid_ok=False, missing_ok=False)

    def test_get01(self, any_non_empty_dict_provider: DictProvider):
        none = object.__new__(FSDictEntry)  # used as typed None
        none._name = "none"

        assert (d := any_non_empty_dict_provider.get("xxx", none, invalid_ok=False, missing_ok=True)) is none
        assert d.name == "none"
        assert any_non_empty_dict_provider.is_missing("xxx")

        assert (d := any_non_empty_dict_provider.get("datax", none, invalid_ok=False, missing_ok=True)) is not none  # existing and unloaded
        assert d.name == "datax"
        assert (d := any_non_empty_dict_provider.get("datax", none, invalid_ok=False, missing_ok=True)) is not none  # existing and loaded
        assert d.name == "datax"

        any_non_empty_dict_provider.mark_missing("mmm")  # missing
        any_non_empty_dict_provider.mark_invalid("iii")  # invalid

        assert (d := any_non_empty_dict_provider.get("mmm", none, invalid_ok=False, missing_ok=True)) is none
        assert d.name == "none"
        with pytest.raises(DictInvalidError):
            _ = any_non_empty_dict_provider.get("iii", none, invalid_ok=False, missing_ok=True)

    def test_get10(self, any_non_empty_dict_provider: DictProvider):
        none = object.__new__(FSDictEntry)  # used as typed None
        none._name = "none"

        with pytest.raises(DictNotExistsError):
            _ = any_non_empty_dict_provider.get("xxx", none, invalid_ok=True, missing_ok=False)  # non-existing
        assert any_non_empty_dict_provider.is_missing("xxx")

        assert (d := any_non_empty_dict_provider.get("datax", none, invalid_ok=True, missing_ok=False)) is not none  # existing and unloaded
        assert d.name == "datax"
        assert (d := any_non_empty_dict_provider.get("datax", none, invalid_ok=True, missing_ok=False)) is not none  # existing and loaded
        assert d.name == "datax"

        any_non_empty_dict_provider.mark_missing("mmm")  # missing
        any_non_empty_dict_provider.mark_invalid("iii")  # invalid

        with pytest.raises(DictNotExistsError):
            _ = any_non_empty_dict_provider.get("mmm", none, invalid_ok=True, missing_ok=False)

        assert (d := any_non_empty_dict_provider.get("iii", none, invalid_ok=True, missing_ok=False)) is none
        assert d.name == "none"

    def test_get11(self, any_non_empty_dict_provider: DictProvider):
        none = object.__new__(FSDictEntry)  # used as typed None
        none._name = "none"

        assert (d := any_non_empty_dict_provider.get("xxx", none, invalid_ok=True, missing_ok=True)) is none
        assert d.name == "none"
        assert any_non_empty_dict_provider.is_missing("xxx")

        assert (d := any_non_empty_dict_provider.get("datax", none, invalid_ok=True, missing_ok=True)) is not none  # existing and unloaded
        assert d.name == "datax"
        assert (d := any_non_empty_dict_provider.get("datax", none, invalid_ok=True, missing_ok=True)) is not none  # existing and loaded
        assert d.name == "datax"

        any_non_empty_dict_provider.mark_missing("mmm")  # missing
        any_non_empty_dict_provider.mark_invalid("iii")  # invalid

        assert (d := any_non_empty_dict_provider.get("mmm", none, invalid_ok=True, missing_ok=True)) is none
        assert d.name == "none"
        assert (d := any_non_empty_dict_provider.get("iii", none, invalid_ok=True, missing_ok=True)) is none
        assert d.name == "none"

    def test_transfer0(self, any_dict_provider: DictProvider, any_readonly_dict_provider: DictProvider):
        with pytest.raises(ReadOnlyError):
            any_dict_provider.transfer(any_readonly_dict_provider)

    def test_transfer(self,
                      empty_dict_provider: DictProvider,
                      alt_empty_dict_provider: DictProvider,
                      all_dict_data: tuple[tuple[bytes, int, int], ...]
                      ):
        empty_dict_provider.add("aaa", all_dict_data[0][0], all_dict_data[0][2])
        empty_dict_provider.add("bbb", all_dict_data[1][0], all_dict_data[1][2])
        empty_dict_provider.add("ccc", all_dict_data[2][0], all_dict_data[2][2])

        del empty_dict_provider._dicts["bbb"]  # force uncaching: simulate unloaded dict
        assert not empty_dict_provider.is_loaded("bbb")

        empty_dict_provider.mark_invalid("iii")
        empty_dict_provider.mark_missing("mmm")

        with pytest.raises(ValueError):
            empty_dict_provider.transfer(None)

        empty_dict_provider.transfer(empty_dict_provider)  # do nothing
        assert set(empty_dict_provider) == {"aaa", "bbb", "ccc"}
        assert not empty_dict_provider.is_loaded("bbb")

        empty_dict_provider.transfer(alt_empty_dict_provider)
        assert set(alt_empty_dict_provider) == {"aaa", "bbb", "ccc"}
        assert set(alt_empty_dict_provider.iter(loaded_only=True)) == set()
        assert set(empty_dict_provider.iter(loaded_only=True)) == set(empty_dict_provider) == {"aaa", "bbb", "ccc"}
        assert alt_empty_dict_provider.size == empty_dict_provider.size == sum(d[1] for d in all_dict_data)
        assert not alt_empty_dict_provider.is_missing("mmm")
        assert alt_empty_dict_provider.is_invalid("iii")  # invalid are transferred
        assert not alt_empty_dict_provider.empty
        assert not empty_dict_provider.empty

        with pytest.raises(DictExistsError):
            empty_dict_provider.transfer(alt_empty_dict_provider)
