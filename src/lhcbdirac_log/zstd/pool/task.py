"""This module provides the task classes for the zstd module.

These classes are used to define tasks that can be executed by workers.

Classes:
    - Task: an abstract task to be executed by a worker using a processor
    - ProcessingTask: a task, processing data from an input entry to an output entry
    - TrainingTask: a task, training a dictionary from a list of processing tasks
"""

from abc import ABC, abstractmethod
from collections.abc import Callable
from queue import Queue
from typing import override

from zstandard import ZstdError

from ...config import Config
from ...providers import DataEntry, DictProvider
from ..processors import ZstandardTrainer
from .processor import ProcessorPool

__all__ = (
    "Task",
    "ProcessingTask",
    "TrainingTask",
)


class Task[P](ABC):
    """[Internal] Abstract base class for tasks to be executed by a worker.

    Task execution is done using a processor, obtained when the task is running.
    """

    __slots__ = ("_procpool",)

    def __init__(self, procpool: P) -> None:
        """Initialize the task.

        Args:
            procpool: the processor pool, used to get a processor
        """
        self._procpool = procpool

    @abstractmethod
    def run(self) -> None:
        """Run the task.

        Notes:
            - the task should release the processor when done
            - the task may not be run again
            - may raise exceptions
        """


class ProcessingTask(Task[ProcessorPool]):
    """[Internal] Class for processing tasks.

    Such tasks are processing data from an input entry to an output entry,
    using a processor from the processor pool, and a dictionary from the dictionary provider.
    """

    __slots__ = (
        "_dict_provider",
        "_data_in",
        "_data_out",
        "_config",
    )

    @property
    def dict_name(self) -> str:
        """Get the dictionary name from the entries.

        Returns:
            the dictionary name
        """
        return self._data_in.dict_name

    @property
    def data_in(self) -> DataEntry:
        """Get the input data entry.

        Returns:
            the input data entry
        """
        return self._data_in

    @property
    def data_out(self) -> DataEntry:
        """Get the output data entry.

        Returns:
            the output data entry
        """
        return self._data_out

    def __init__(self, procpool: ProcessorPool, data_in: DataEntry, data_out: DataEntry, dict_provider: DictProvider, config: Config) -> None:
        """Initialize the task.

        Args:
            procpool: the processor pool
            data_in: the input data entry
            data_out: the output data entry
            dict_provider: the dictionary provider to get dictionaries from
            config: the configuration to use
        """
        super().__init__(procpool)
        self._dict_provider = dict_provider
        self._data_in = data_in
        self._data_out = data_out
        self._config = config

    @override
    def run(self) -> None:
        dict_entry = self._dict_provider.get(self._data_in.dict_name, invalid_ok=True, missing_ok=True)

        dict_name = None if dict_entry is None else dict_entry.dict_name

        if (processor := self._procpool[dict_name]) is None:  # Get a free processor with the required dictionary
            if (processor := self._procpool.get_any()) is None:  # Get a free processor with any dictionary
                msg = "No free processor available"
                raise RuntimeError(msg)  # No free, should never happen

            processor.dict = dict_entry  # assign the dictionary ("slow" operation)

        try:
            if (sz := self._data_in.size) >= self._config.max_data_size:
                msg = f"Data too large: {sz} >= {self._config.max_data_size}"
                raise ValueError(msg)

            with self._data_out.writer() as writer:
                if sz > 0:  # empty data is not processed -> out is empty too
                    with self._data_in.reader() as reader:
                        processor.process_stream(reader, writer)
        finally:
            self._procpool.release(processor)


class TrainingTask(Task[Queue[ZstandardTrainer]]):
    """[Internal] Class for training tasks.

    Such tasks are training a dictionary from a list of processing tasks.
    When done, these are reinjected in the task queue.
    """

    __slots__ = (
        "_tasks",
        "_name",
        "_callback",
        "_dict_provider",
    )

    def __init__(
        self, trainpool: Queue[ZstandardTrainer], callback: Callable[[str], None], dict_provider: DictProvider, name: str, tasks: list[ProcessingTask]
    ) -> None:
        """Initialize the task.

        Args:
            trainpool: the training pool, to get a trainer
            callback: the callback to call when training is done, called with the dictionary name
            dict_provider: the dictionary provider to save the trained dictionary to
            name: the name of the dictionary to train
            tasks: the tasks to take samples from
        """
        super().__init__(trainpool)
        self._name = name
        self._tasks = tasks
        self._callback = callback
        self._dict_provider = dict_provider

    @override
    def run(self) -> None:
        trainer = self._procpool.get()

        try:
            trainer.train(self._dict_provider, *(i.data_in for i in self._tasks))
        except (ValueError, ZstdError):
            self._dict_provider.mark_invalid(self._name)
        finally:
            trainer.dict = None
            self._procpool.put(trainer)
            self._callback(self._name)
