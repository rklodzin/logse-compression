The library can be installed directly via pip:
```bash
pip install lhcbdirac-log-converter
```

Or from the source code:
```bash
pip install git+https://gitlab.cern.ch/rklodzin/logse-compression.git
```

To support the FSspec provider, install using:
```bash
pip install lhcbdirac-log-converter[fsspec]
```

For development dependencies, install using:
```bash
pip install lhcbdirac-log-converter[dev]
```

For both, use:
```bash
pip install lhcbdirac-log-converter[all]
```
