"""Implementation of the data provider layer for the local filesystem.

This module provides the filesystem implementation of entries classes for the data provider layer

Classes:
    - FSDataEntry: Filesystem data entry implementation.
    - FSDictEntry: Filesystem dictionary entry implementation.
    - FSJobEntry: Filesystem job entry implementation.
"""

from collections.abc import Generator
from os import PathLike
from pathlib import Path
from typing import BinaryIO, override

from ...config import Config
from ..base import (
    DataEntry,
    DataExistsError,
    DataNotExistsError,
    DictEntry,
    DictNotExistsError,
    JobEntry,
    JobExistsError,
    JobNotExistsError,
    ReadOnlyError,
)

__all__ = (
    "FSDataEntry",
    "FSDictEntry",
    "FSJobEntry",
)


class FSDataEntry(DataEntry):
    """Filesystem data entry implementation.

    This implementation supports both the old uncompressed and new compressed (Zstandard) formats.
    """

    __slots__ = ("_path",)

    def __init__(self, path: PathLike[str] | str, job: int, *, compressed: bool, readonly: bool) -> None:
        """[Internal] Initialize the data entry.

        Args:
            path: the data file path
            job: the job id
            compressed: indicate whether the underlying data is compressed or not (in Zstandard)
            readonly: indicate weather the data is read-only or not
        """
        self._path = Path(path)
        super().__init__(self._path.name, job, compressed=compressed, readonly=readonly)

    @override
    def _reader(self) -> BinaryIO:
        try:
            return self._path.open("rb")
        except FileNotFoundError as err:  # pragma: no cover
            raise DataNotExistsError(self._path) from err

    @override
    def _writer(self) -> BinaryIO:
        return self._path.open("wb")

    @override
    def _size(self) -> int | None:
        try:
            return self._path.stat().st_size
        except FileNotFoundError:
            return None

    @override
    def _delete(self) -> None:
        try:
            self._path.unlink()
        except FileNotFoundError as err:
            raise DataNotExistsError(self._path) from err


class FSDictEntry(DictEntry):
    """Filesystem dictionary entry implementation."""

    __slots__ = ("_path",)

    def __init__(self, path: PathLike[str] | str, config: Config, data: bytes | None = None, zstd_id: int | None = None) -> None:
        """[Internal] Initialize the dictionary entry.

        Args:
            path: the dictionary file path
            config: the configuration to use for precomputing the dictionary
            data: the dictionary data (create a new dict if not None)
            zstd_id: the zstd dictionary id (None for unknown)
        """
        self._path = Path(path)
        super().__init__(self._path.name, config, data, zstd_id)

    @property
    @override
    def exists(self) -> bool:
        return self._path.is_file()

    @property
    @override
    def size(self) -> int:
        try:
            return self._path.stat().st_size
        except FileNotFoundError:
            return 0

    @override
    def _load_data(self) -> bytes:
        try:
            with self._path.open("rb") as file:
                return file.read()
        except FileNotFoundError as err:
            raise DictNotExistsError(self._path) from err

    @override
    def _save(self) -> None:
        with self._path.open("wb") as file:
            file.write(self._data)


class FSJobEntry(JobEntry[FSDataEntry]):
    """Filesystem job entry implementation.

    This implementation supports both the old uncompressed and new compressed (Zstandard) formats.
    """

    __slots__ = ("_folder",)

    def __init__(self, folder: Path, job: int, *, compressed: bool, readonly: bool, create: bool = True, exists_ok: bool = True) -> None:
        """[Internal] Initialize the job entry.

        Args:
            folder: the production folder path (parent of the job folders)
            job: the job id
            compressed: indicate whether the underlying data is compressed or not (in Zstandard)
            readonly: indicate weather the job is read-only or not
            create: create the job folder if it doesn't exist, otherwise raise an error
            exists_ok: ignore the error if the job folder already exists

        Raises:
            JobExistsError: if the job folder already exists and exists_ok is False
            JobNotExistsError: if the job folder doesn't exist and create is False
        """
        self._folder = folder / f"{job:08}"

        if self._folder.is_dir():
            if not exists_ok:
                raise JobExistsError(job)
        elif create:
            self._folder.mkdir(parents=True, exist_ok=True)
        else:
            raise JobNotExistsError(job)

        super().__init__(job, compressed=compressed, readonly=readonly)

    @property
    def folder(self) -> Path:
        """Get the job folder path.

        Returns:
            the job folder path
        """
        return self._folder

    @override
    def _get(self, name: str, *, create: bool = False) -> FSDataEntry:
        p = self._folder / name

        if not create and not p.is_file():
            raise DataNotExistsError(p)

        return FSDataEntry(p, self._job, compressed=self._compressed, readonly=self._readonly)

    @override
    def _create(self, name: str, *, exists_ok: bool = False) -> FSDataEntry:
        p = self._folder / name

        if p.is_file() and not exists_ok:
            raise DataExistsError(p)

        return FSDataEntry(p, self._job, compressed=self._compressed, readonly=self._readonly)

    @override
    def files(self) -> Generator[str, None, None]:
        yield from (p.name for p in self._folder.iterdir() if p.is_file())

    @property
    @override
    def data_size(self) -> int:  # optimizes the default implementation
        return sum((self._folder / f).stat().st_size for f in self.files())

    @override
    def delete(self, name: str) -> None:  # optimizes the default implementation
        if self._readonly:
            msg = f"Job '{self._job}' is read-only"
            raise ReadOnlyError(msg)

        p = self._folder / name

        try:
            p.unlink()
        except FileNotFoundError as err:
            raise DataNotExistsError(p) from err

    @override
    def _update_info(self) -> None:  # pragma: no cover
        pass
