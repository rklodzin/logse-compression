"""
Log conversion submission managment script
"""

import json
import os
from os.path import join
from pathlib import Path

import DIRAC
import fsspec
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise
from DIRAC.Interfaces.API.Dirac import Dirac
from DIRAC.Interfaces.API.Job import Job
from LHCbDIRAC.Interfaces.API.LHCbJob import LHCbJob
from LHCbDIRAC.TransformationSystem.Client.TransformationClient import TransformationClient
from fsspec.asyn import AsyncFileSystem

PATH = ("LHCb", "MC")

BASE = "/eos/lhcb/grid/prod/lhcb/logSE/lhcb/{}"
BASE_DB = "/eos/lhcb/grid/prod/lhcb/logArchive/lhcb/{}"

EOS = BASE.format("{}/LOG/{}")
EOS_full = EOS.format('{}/{}', '{:08d}/{:04d}')

DIRAC.initialize(log_level=DIRAC.LogLevel.ERROR)


class State:
    """
    Represents the state of the global conversion process
    """

    __slots__ = ("available", "running", "success", "failed", "dirac", "saved", "size_in", "size_out", "finalized")

    def __init__(self, path: str | None = None):
        try:
            with open(path or "state.json", "r") as f:
                state = json.load(f)
                self.available = set(state["available"])
                self.running = state["running"]
                self.success = state["success"]
                self.failed = state["failed"]
                self.finalized = set(state.get("finalized", set()))
        except FileNotFoundError:
            self.available = set()
            self.running = {}
            self.success = {}
            self.failed = {}
            self.finalized = set()

        self.saved = 0
        self.size_in = 0
        self.size_out = 0

        self.dirac = Dirac()

    def save(self, path: str | None = None) -> None:
        """
        Save the state to a file

        Args:
            path: path to the file (default: state.json)
        """

        with open(path or "state.json", "w") as f:
            json.dump({
                "available": list(self.available),
                "running": self.running,
                "success": self.success,
                "failed": self.failed,
                "finalized": self.finalized,
            }, f)

    @staticmethod
    def path_to_id(path: str) -> int:
        """
        Convert a path to a job id

        Args:
            path: the path to convert

        Returns:
            the job id
        """

        return int(path.split("/")[-2])

    def filter_available(self) -> None:
        """
        Filter the available paths to only keep the ones that are ready
        """

        print("fetching ready...")

        tc = TransformationClient()
        tids = returnValueOrRaise(tc.getTransformations({"Status": ["Archived"]}, columns=["TransformationID"]))
        ids = {x["TransformationID"] for x in tids}

        print("done with", len(ids), "ready")

        print("filtering availables...")

        self.available = set(filter(lambda x: self.path_to_id(x) in ids, self.available))

        print("done with", len(self.available), "available & ready")

    def fetch_available(self, filtering: bool = True):
        """
        Fetch the set of subprod available for conversion
        """

        r = self.available

        print("fetching availables...")

        for subpath in PATH:
            fs, path = fsspec.core.url_to_fs(f"root://eoslhcb.cern.ch/{BASE.format(subpath)}")
            fs: AsyncFileSystem

            for i in fs.listdir(path, detail=False):
                x = join(path, i, 'LOG')

                print("fetching", x, '...')

                for u in fs.listdir(x, detail=False):
                    y = join(x, u)

                    for n in fs.listdir(y, detail=False):
                        z = join(y, n)
                        if len(z) == 4:
                            r.add(z)

        r.difference_update(self.running.keys())
        r.difference_update(self.success.keys())
        r.difference_update(self.failed.keys())

        self.save()

        print("done with", len(r), "availables")

        if filtering:
            self.filter_available()

        return r

    def fetch_finalized(self) -> None:
        """
        Fetch subprod that are already converted
        """

        r = self.finalized

        for subpath in PATH:
            fs, path = fsspec.core.url_to_fs(f"root://eoslhcb.cern.ch/{BASE_DB.format(subpath)}")
            fs: AsyncFileSystem

            for i in fs.listdir(path, detail=False):
                x = join(path, i, 'LOG')

                print("fetching", x, '...')

                for u in fs.listdir(x, detail=False):
                    y = join(x, u)

                    for n in fs.listdir(y, detail=False):
                        z = join(y, n)
                        r.add(z.replace(".db", ""))


    def fetch_finished(self) -> None:
        """
        Fetch the status of the running jobs
        """

        s = self.success
        f = self.failed
        r = self.running

        if r:
            d = {ids: path for path, ids in r.items()}

            v = returnValueOrRaise(self.dirac.getJobStatus(list(d)))

            for ids, status in v.items():

                match status["Status"]:
                    case "Done":
                        s[d[ids]] = ids
                        r.pop(d[ids])
                    case "Failed":
                        f[d[ids]] = ids
                        r.pop(d[ids])
                    case _:
                        pass
            self.save()

    def print(self) -> None:
        """
        Print the state of the conversion process
        """

        print("Available:", len(self.available))
        print("Running:", len(self.running))
        print("Success:", len(self.success))
        print("Failed:", len(self.failed))
        print("Finalized:", len(self.finalized))

    def submit(self, path: str | None = None) -> int:  # eos full subprod path
        """
        Submit a job to process a given path or any ready if not specified

        Args:
            path: path to the subprod to process (must be ready)

        Returns:
            the job id
        """

        if path is None:
            path = self.available.pop()
        else:
            self.available.remove(path)

        prod, subprod = path.split("/")[-2:]
        pprod = f"{prod}/{subprod}"

        j = Job()
        j.setTag(["/cvmfs/lhcbdev.cern.ch/"])
        # j.setType('"DiracLogConvert"')
        j.setName(f'"DiracLogConvert - {pprod}"')
        print(f"submitting {path} as {pprod}")
        j.setJobGroup(f'"DiracLogConvert:{pprod}"')

        j.setExecutable('./run.sh')
        j.setInputSandbox(["process.py"])
        j.setCPUTime(24 * 3600)
        # j.setOutputData([f'{prod}_{subprod}.db'])

        j.setOutputSandbox(["output.json"])
        j.setDestination("LCG.CERN.cern")
        j.setBannedSites(
            [
                "LCG.NCBJ-CIS.pl",  # too slow
                "DIRAC.SDumont.br",  # Jobs get stuck in matched
                "DIRAC.MareNostrum.es",  # Not a normal site
                "LCG.Beijing.cn",  # Generally seems to misbehave, probably networking issues to CERN
            ]
        )

        j.setExecutionEnv({"LOG_SRC_PATH": path,
                           "LOG_PROCESSING_WORKER": 1,
                           "TOKEN": "zteos64:MDAwMDAyNTh4nOOaysTFXFReIdBzt3crmxRLTlJyphJTlakGo5Gyfmp-sX5ORnKSfnpRZop-QVF-CoSbk5_uWJSckVmWasHopZJoZGlmmWicqmtimmSka2iYmqZrmWRkqJtokpRmYGhuaWZqnBi0jjHarShTwas0R8HQUsHQ0MrYwsrIQMHIwMgkVqE0M8XK0NTE3MQyGmR_rEI6SMDE3CC6yjRWoSQzJTWvxAoko2dkZGhiYmBkZWRsaOSQU1GQU1psaWKmkJeYmwpWoJCSZ6UAdGiJVXZRkqlCYkGBlUJGfjFQN0ytXnJqUZ5ecoZCSn5uYmaeFYybnppvZWBqaKxQXJqSb2UopBBxbammJqtz0V3pJ_ulgyxc5X63T733Y8WPxXsmXGhh3PlDajTksIacwuxDfS-ZAYhnv8Y%3d",
                           })

        dirac_id = int(returnValueOrRaise(self.dirac.submitJob(j)))
        print(f"{path} submitted as {dirac_id}")
        self.running[path] = dirac_id

        self.save()

        return dirac_id

    def upd_all(self) -> None:
        self.saved = 0
        self.size_in = 0
        self.size_out = 0

        self.upd(list(self.success.values()))
        self.upd(list(self.failed.values()))

        print(f"Saved: {self.saved:,} B")
        print(f"Size in: {self.size_in:,} B")
        print(f"Size out: {self.size_out:,} B")

    def upd(self, idd: int | list[int]) -> None:
        v = returnValueOrRaise(self.dirac.getJobStatus(idd))

        for i in [idd] if isinstance(idd, int) else idd:
            if i not in v:
                print("error", i, v)
                continue

            if v[i]["Status"] in ("Done", "Failed"):
                if not os.path.exists(f"output/{i}"):
                    try:
                        f = returnValueOrRaise(self.dirac.getOutputSandbox(i, "output"))
                    except:
                        print("missing", i)
                        continue
                else:
                    f = os.listdir(f"output/{i}")

                if "output.json" in f:

                    with open(f"output/{i}/output.json", "r") as f:
                        d = json.load(f)

                        if d.get("uploaded", False) != (v[i]["Status"] == "Done"):
                            print("mismatch", d, v[i])

                        si, so, sa = d.get("size_in"), d.get("size_out"), d.get("saved")
                        if sa:
                            self.saved += sa
                            self.size_in += si
                            self.size_out += so


if __name__ == "__main__":
    s = State()

    print("State:")
    s.print()

    if not s.available:
        s.fetch_available()

    # print("fetching finished...")
    # s.fetch_finished()

    print("State:")
    s.print()

    import code
    code.interact(local=locals())
