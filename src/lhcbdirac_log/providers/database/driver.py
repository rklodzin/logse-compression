"""Implementation of the data provider layer for SQL databases.

This module provides the SQLite database implementation of the SQLite Driver class.

Classes:
    - SQLDriver: SQL Database driver implementation.
"""

from collections.abc import Generator
from pathlib import Path
from threading import RLock
from typing import Self

from sqlalchemy import (
    BLOB,
    Boolean,
    Column,
    Connection,
    Executable,
    ForeignKey,
    Integer,
    MetaData,
    String,
    Table,
    create_engine,
    delete,
    func,
    insert,
    select,
    text,
    update,
)
from sqlalchemy.engine import Engine
from sqlalchemy.exc import IntegrityError

from ..base import DataEntry, DataExistsError, DataNotExistsError, DictExistsError, DictNotExistsError, JobInfo


class SQLDriver:
    """[Internal] SQL low-level database driver for SQLite databases.

    This class provides the low-level database access used for all other classes of this implementation.

    This class must be instantiated manually but not used directly.
    Must be used through the SQLDataProvider and SQLDictProvider classes.
    """

    SQLITE_BLOB_LIMIT = 1_000_000_000

    __slots__ = (
        "_connection",
        "_data_table",
        "_dict_table",
        "_engine",
        "_job_table",
        "_lock",
        "_metadata",
    )

    @property
    def blob_limit(self) -> int:
        """Get the maximum size of a BLOB in the database.

        Returns:
            the maximum size of a BLOB in the database
        """
        return self.SQLITE_BLOB_LIMIT

    @property
    def engine(self) -> Engine:  # pragma: no cover
        """Get the database engine.

        Returns:
            the database engine
        """
        return self._engine

    @property
    def connection(self) -> Connection:
        """Get the database connection.

        Returns:
            the database connection
        """
        return self._connection

    @classmethod
    def create(cls, path: str | None = None) -> Self:
        """Create a new SQLite database driver instance attached to a SQLite database.

        Args:
            path: the path to the database file (new or existing), in-memory database by default (default is None)

        Returns:
            a new SQLManager instance
        """
        q = ":memory:" if path is None else str(Path(path).resolve())
        engine = create_engine(
            f"sqlite:///{q}",
            # , echo=True
        )

        return cls(engine)

    def __init__(self, engine: Engine) -> None:
        """Initialize a new SQLite database driver.

        Args:
            engine: the database engine

        Notes:
            - use the `create` class method to create a new driver from a database path

        Schema:
            - data: String name [PK], Integer job [PK, FK:job.id], Integer dict [FK:dict.id], BLOB data
            - dict: Integer id [PK], String name [UNIQUE], Integer zstd_id [NULLABLE], BLOB data [NULLABLE]
            - job: Integer id [PK], Integer dirac_id [NULLABLE], Boolean success [NULLABLE]
        """
        self._engine = engine
        self._connection = engine.connect()

        self._connection.execute(text("PRAGMA foreign_keys=ON"))
        self._connection.execute(text("PRAGMA synchronous=OFF"))
        self._connection.execute(text("PRAGMA cache_size=-131072"))  # 128 MiB
        self._connection.execute(text("PRAGMA temp_store=MEMORY"))
        self._connection.execute(text("PRAGMA locking_mode=EXCLUSIVE"))
        self._connection.execute(text("PRAGMA journal_mode=OFF"))

        self._lock = RLock()

        self._metadata = MetaData()

        self._data_table = Table(
            "data",
            self._metadata,
            Column("name", String, primary_key=True),
            Column("job", ForeignKey("job.id", ondelete="RESTRICT"), primary_key=True),
            Column("dict", ForeignKey("dict.id", ondelete="RESTRICT"), nullable=False),
            Column("data", BLOB, nullable=False),
        )

        self._dict_table = Table(
            "dict",
            self._metadata,
            Column("id", Integer, primary_key=True, autoincrement=True),
            Column("name", String, nullable=False, unique=True),  # name <=> id
            Column("zstd_id", Integer, nullable=True),  # NULL for invalid dict
            Column("data", BLOB, nullable=True),  # NULL for invalid dict
        )

        self._job_table = Table(
            "job",
            self._metadata,
            Column("id", Integer, primary_key=True),
            Column("dirac_id", Integer, nullable=True),
            Column("success", Boolean, nullable=True),
        )

        self._metadata.create_all(self._connection)

    def _check_dict(self, dict_name: str) -> tuple[int, bool]:
        """[Internal] Check if a dictionary exists in the database.

        Args:
            dict_name: the dictionary name

        Returns:
            the dict id if the dictionary exists, None otherwise, and a bool indicating if the dictionary is invalid
        """
        with self._lock:
            r = self.connection.execute(
                select(self._dict_table.c.id, self._dict_table.c.zstd_id).where(self._dict_table.c.name == dict_name)
            ).one_or_none()
            return (None, False) if r is None else (r.id, r.zstd_id is None)

    def check_dict(self, dict_name: str, *, invalid: bool = False) -> int | None:
        """Check if a dictionary exists in the database.

        Args:
            dict_name: the dictionary name
            invalid: if True, return the dict id even if the dictionary is invalid, otherwise return None (default is False)

        Returns:
            the dict id if the dictionary exists, None otherwise
        """
        r, i = self._check_dict(dict_name)
        return r if not i or invalid else None

    def check_data(self, job: int, file_name: str) -> bool:
        """Check if a file exists in the database.

        Args:
            job: the job id
            file_name: the file name

        Returns:
            True if the file exists, False otherwise
        """
        with self._lock:
            s = select(self._data_table.c.name).where(self._data_table.c.job == job, self._data_table.c.name == file_name)
            return self.connection.execute(s).one_or_none() is not None

    def check_job(self, job: int) -> bool:
        """Check if a job exists in the database.

        Args:
            job: the job id

        Returns:
            True if the job exists, False otherwise
        """
        with self._lock:
            return self.connection.execute(select(self._job_table.c.id).where(self._job_table.c.id == job)).one_or_none() is not None

    def load_dict(self, dict_name: str) -> tuple[bytes, int]:
        """Load a (non-invalid) dictionary from the database.

        Args:
            dict_name: the dictionary name

        Returns:
            a tuple of the dictionary data and its zstd ID

        Raises:
            DictNotExistsError: if the dictionary does not exist or is invalid
        """
        with self._lock:
            o = self.connection.execute(
                select(self._dict_table.c.data, self._dict_table.c.zstd_id).where(self._dict_table.c.name == dict_name)
            ).one_or_none()

            if o is not None and o.zstd_id is not None:
                return bytes(o.data), o.zstd_id

            msg = f"dictionary '{dict_name}' does not exist"
            raise DictNotExistsError(msg)

    def load_data(self, job: int, file_name: str) -> bytes:
        """Load a file from the database.

        Args:
            job: the job id
            file_name: the file name

        Returns:
            the file data or None if the file does not exist

        Raises:
            DataNotExistsError: if the file does not exist
        """
        with self._lock:
            s = select(self._data_table).where(self._data_table.c.job == job, self._data_table.c.name == file_name)
            o = self.connection.execute(s).one_or_none()

            if o is not None:
                return bytes(o.data)  # mypy cast

            msg = f"file {file_name} does not exist in job {job}"  # pragma: no cover
            raise DataNotExistsError(msg)  # pragma: no cover

    def load_job(self, job: int) -> JobInfo:
        """Load job metadata from the database.

        Args:
            job: the job id

        Returns:
            the job metadata
        """
        with self._lock:
            o = self.connection.execute(select(self._job_table).where(self._job_table.c.id == job)).one_or_none()
            return JobInfo(o.dirac_id, o.success)

    def save_invalid_dict(self, name: str) -> None:
        """Save an invalid dictionary to the database.

        Args:
            name: the dictionary name

        Notes:
            - forwards to save_dict with data=None and zstd_id=None
        """
        self.save_dict(name, None, None)

    def save_dict(self, name: str, data: bytes | None, zstd_id: int | None) -> None:
        """Save a dictionary to the database.

        Args:
            name: the dictionary name
            data: the dictionary data or None for invalid
            zstd_id: the zstd dictionary id or None for invalid

        Raises:
            ValueError: if any of the dictionary data or zstd id is None and not the other
            DictExistsError: if the (non-invalid) dictionary already exists

        Notes:
            - overwrites the dictionary if it already exists but is invalid
        """
        s: Executable

        if (data is None) != (zstd_id is None):  # pragma: no cover
            msg = "both data and zstd_id must be None or not None"
            raise ValueError(msg)

        with self._lock:
            d_id, inv = self._check_dict(name)
            if d_id is None:
                s = insert(self._dict_table).values(name=name, data=data, zstd_id=zstd_id)
            elif inv:
                s = update(self._dict_table).where(self._dict_table.c.id == d_id).values(data=data, zstd_id=zstd_id)
            else:  # pragma: no cover
                msg = f"dictionary {name} (id: {d_id}, zstd_id: {zstd_id}) already exists"
                raise DictExistsError(msg)

            conn = self.connection
            conn.execute(s)
            conn.commit()

    def save_data(
        self,
        job: int,
        file_name: str,
        data: bytes,
    ) -> None:
        """Save a file to the database.

        Args:
            job: the job id
            file_name: the file name
            data: the data to save

        Raises:
            DictNotExistsError: if the dictionary does not exist
        """
        s: Executable

        with self._lock:
            if self.check_data(job, file_name):
                s = update(self._data_table).where(self._data_table.c.job == job, self._data_table.c.name == file_name).values(data=data)
            else:
                dict_name = DataEntry.filename_to_dictname(file_name)
                d_id = self.check_dict(dict_name, invalid=True)

                if d_id is None:
                    msg = f"dictionary {dict_name} (for {file_name}) does not exist"
                    raise DictNotExistsError(msg)

                s = insert(self._data_table).values(name=file_name, job=job, dict=d_id, data=data)

            conn = self.connection
            conn.execute(s)
            conn.commit()

    def save_job(self, job: int, info: JobInfo | None = None) -> None:
        """Save/overwrite a job to the database.

        Args:
            job: the job id
            info: the job metadata (default is None)
        """
        s: Executable

        k = {} if info is None else {"dirac_id": info.dirac_id, "success": info.success}

        with self._lock:
            if not self.check_job(job):
                s = insert(self._job_table).values(id=job, **k)
            else:
                s = update(self._job_table).where(self._job_table.c.id == job).values(**k)

            conn = self.connection
            conn.execute(s)
            conn.commit()

    def delete_dict(self, dict_name: str) -> None:
        """Delete a dictionary from the database.

        Args:
            dict_name: the dictionary name

        Raises:
            DataExistsError: if the dictionary has associated files
            DictNotExistsError: if the dictionary does not exist
        """
        with self._lock:
            if not self.check_dict(dict_name):
                msg = f"dictionary {dict_name} does not exist"
                raise DictNotExistsError(msg)

            conn = self.connection
            try:
                conn.execute(delete(self._dict_table).where(self._dict_table.c.name == dict_name))
                conn.commit()
            except IntegrityError as err:
                conn.rollback()
                raise DataExistsError(dict_name) from err

    def delete_data(self, job: int, file_name: str) -> None:
        """Delete a file from the database.

        Args:
            job: the job id
            file_name: the file name

        Raises:
            DataNotExistsError: if the file does not exist
        """
        with self._lock:
            if not self.check_data(job, file_name):
                msg = f"file {file_name} does not exist in job {job}"
                raise DataNotExistsError(msg)

            s = delete(self._data_table).where(self._data_table.c.job == job, self._data_table.c.name == file_name)

            conn = self.connection
            conn.execute(s)
            conn.commit()

    def delete_job(self, job: int, *, force: bool = False) -> None:
        """Delete a job from the database.

        Args:
            job: the job id
            force: if True, delete the job and its associated files (default is False)

        Raises:
            DataExistsError: if the job has associated files and force is False
        """
        with self._lock:
            conn = self.connection
            try:
                if force:
                    conn.execute(delete(self._data_table).where(self._data_table.c.job == job))
                conn.execute(delete(self._job_table).where(self._job_table.c.id == job))
                conn.commit()
            except IntegrityError as err:
                conn.rollback()
                raise DataExistsError(job) from err

    def clear_job(self, job: int) -> None:
        """Clear all files from a job.

        Args:
            job: the job id
        """
        with self._lock:
            conn = self.connection
            conn.execute(delete(self._data_table).where(self._data_table.c.job == job))
            conn.commit()

    def get_invalid_dicts(self) -> set[str]:
        """Get all invalid dictionaries in the database.

        Returns:
            a set of dictionary names

        Notes:
            - may not be a huge set
        """
        with self._lock:
            return {o.name for o in self.connection.execute(select(self._dict_table.c.name).where(self._dict_table.c.zstd_id.is_(None)))}

    def get_dicts(self, *, invalid: bool = False) -> set[str]:
        """Get all dictionaries in the database.

        Args:
            invalid: if True, include invalid dictionaries (default is False)

        Returns:
            a set of dictionary names

        Notes:
            - may not be a huge set
        """
        s = select(self._dict_table.c.name)
        if not invalid:
            s = s.where(self._dict_table.c.zstd_id.isnot(None))

        return {o.name for o in self.connection.execute(s)}

    def get_files(self, job: int) -> set[str]:
        """Get all files in a job.

        Args:
            job: the job id

        Returns:
            a set of file names

        Notes:
            - may not be a huge set
        """
        s = select(self._data_table.c.name).where(self._data_table.c.job == job)
        return {o.name for o in self.connection.execute(s)}

    def get_jobs(self) -> Generator[int, None, None]:
        """Get all jobs in the database.

        Returns:
            a generator of job ids
        """
        for o in self.connection.execute(select(self._job_table.c.id)):
            yield o.id

    def get_data_size(self, job: int, file_name: str) -> int | None:
        """Get the size of a file in the database.

        Args:
            job: the job id
            file_name: the file name

        Returns:
            the size of the file, or None if the file does not exist
        """
        with self._lock:
            s = select(func.char_length(self._data_table.c.data)).where(self._data_table.c.job == job, self._data_table.c.name == file_name)

            return self.connection.execute(s).scalar()

    def get_dict_size(self, dict_name: str) -> int:
        """Get the storage size of the specified dictionary.

        Args:
            dict_name: the dictionary name

        Returns:
            the storage size of the specified dictionary
        """
        s = select(func.sum(func.char_length(self._dict_table.c.data))).where(self._dict_table.c.name == dict_name)

        return self.connection.execute(s).scalar() or 0

    def get_all_dict_size(self) -> int:
        """Get the storage size of all dictionaries data.

        Returns:
            the storage size of all dictionaries data
        """
        s = select(func.sum(func.char_length(self._dict_table.c.data)))

        return self.connection.execute(s).scalar() or 0

    def get_job_size(self, job: int) -> int:
        """Get the storage size of all files data from a job.

        Returns:
            the storage size of all files data from a job
        """
        s = select(func.sum(func.char_length(self._data_table.c.data))).where(self._data_table.c.job == job)

        return self.connection.execute(s).scalar() or 0

    def get_all_data_size(self) -> int:
        """Get the storage size of all files data.

        Returns:
            the storage size of all files data
        """
        s = select(func.sum(func.char_length(self._data_table.c.data)))

        return self.connection.execute(s).scalar() or 0

    def get_data_count(self, job: int) -> int:
        """Get the number of files in a job.

        Args:
            job: the job id

        Returns:
            the number of files in the job
        """
        s = select(func.count()).where(self._data_table.c.job == job)

        return self.connection.execute(s).scalar()

    def get_db_size(self) -> int:
        """Get the storage size of the database file.

        Returns:
            the storage size of the database file
        """
        conn = self.connection
        return conn.execute(text("pragma page_count;")).scalar() * conn.execute(text("pragma page_size;")).scalar()

    def close(self) -> None:
        """Close the connection to the database, and dispose it.

        Notes:
            - the driver may not be used after this method is called
        """
        self.connection.close()
        self._engine.dispose()
