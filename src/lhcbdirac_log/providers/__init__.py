"""This package provides the base classes and exceptions for the data provider layer, and multiple implementations of these.

Description:
    Production/subproduction can be access through a data provider.
    From data providers, job entries can be accessed, created, updated, and deleted.
    From job entries, data entries can be accessed, created, updated, and deleted.
    From data entries, data can be read/written.

    E.g.:
        Production 00001234/0000:
            - Job n°00000042:
                - Data log_data_0001.xml
                - Data job.info
                  ...
            - Job 00001200:
                - Data log_data_0042.xml
                - Data log.txt
                - Data stdout.xml
                ...

    Dict providers are used to stored Zstandard dictionaries, used for both compression and decompression.
    Dict entries are used to access dictionaries data, identified by a unique name.
    The name of a dictionary is the common part between all related files = the filenames without numbers.

    E.g.:
        - Both log_data_0001.xml and log_data_0042.xml are compressed with the same dictionary, named log_data_xxxx.xml.

    When a provider is read-only, it is not possible to create, update, or delete anything.
    When a data provider is compressed (assigned to a dictionary provider),
    underlying data is compressed with Zstandard and the dictionary provider contains the dictionaries,
    used for both compression and decompression.

    Some data providers implementations may not implement the related dict provider,
    supporting only the old "uncompressed" format. On the other hand, some implementations may only support access to
    the new "compressed" format (Zstandard).
    Some implementations may only support the read-only mode.

    The "compressed" status, indicating if the underlying data are compressed in Zstandard or just in a raw uncompressed
    format, is just an "indication". Indeed, no matter what type is a data provider, it will always read/write data as
    is without any processing, considering data are provided/gave already in the good compressed/uncompressed state.

Classes:
    - DataProvider: Base class for data provider implementations.
    - DictProvider: Base class for Zstandard dictionary provider implementations.
    - DataEntry: Base class for data entry objects.
    - DictEntry: Base class for Zstandard dictionary entry objects.
    - JobEntry: Base class for job entry objects.
    - JobInfo: Base class for job information objects.

Exceptions:
    - DataExistsError: Exception raised when data already exists in the provider.
    - DataNotExistsError: Exception raised when data does not exist in the provider.
    - DictExistsError: Exception raised when a dictionary already exists in the provider.
    - DictInvalidError: Exception raised when a dictionary is invalid.
    - DictNotExistsError: Exception raised when a dictionary does not exist in the provider.
    - JobExistsError: Exception raised when a job already exists in the provider.
    - JobNotExistsError: Exception raised when a job does not exist in the provider.
    - ReadOnlyError: Exception raised when a provider is read-only.
"""

from .base import (
    DataEntry,
    DataExistsError,
    DataNotExistsError,
    DataProvider,
    DictEntry,
    DictExistsError,
    DictInvalidError,
    DictNotExistsError,
    DictProvider,
    JobEntry,
    JobExistsError,
    JobInfo,
    JobNotExistsError,
    ReadOnlyError,
)

__all__ = (
    "ReadOnlyError",
    "DataEntry",
    "DataExistsError",
    "DataNotExistsError",
    "DataProvider",
    "DictEntry",
    "DictExistsError",
    "DictInvalidError",
    "DictNotExistsError",
    "DictProvider",
    "JobEntry",
    "JobInfo",
    "JobExistsError",
    "JobNotExistsError",
)
