"""Implementation of the data provider layer for SQL databases.

This implementation only support the new compressed (Zstandard) format.
Thus, a dictionary provider always exists and is used to store the Zstandard dictionaries.

Thanks to the database layout (using SQLite with SQLAlchemy), dict and data entries are linked,
making dict impossible to be deleted if data entries are still using it (safety).

Schema:
    - data: String name [PK], Integer job [PK, FK:job.id], Integer dict [FK:dict.id], BLOB data.
    - dict: Integer id [PK], String name [UNIQUE], Integer zstd_id [NULLABLE], BLOB data [NULLABLE].
    - job: Integer id [PK], Integer dirac_id [NULLABLE], Boolean success [NULLABLE].

Classes:
    - SQLDataEntry: SQL data entry implementation.
    - SQLDictEntry: SQL dictionary entry implementation.
    - SQLJobEntry: SQL job entry implementation.
    - SQLDataProvider: SQL data provider implementation.
    - SQLDictProvider: SQL dictionary provider implementation.
    - SQLDriver: the SQLite driver used by the other classes.
    - SQLDataIO: SQL data file-like IO implementation.
"""

from .accessors import SQLDataEntry, SQLDataIO, SQLDictEntry, SQLJobEntry
from .driver import SQLDriver
from .providers import SQLDataProvider, SQLDictProvider

__all__ = (
    "SQLDataEntry",
    "SQLDataProvider",
    "SQLDictEntry",
    "SQLDictProvider",
    "SQLDriver",
    "SQLJobEntry",
    "SQLDataIO",
)
