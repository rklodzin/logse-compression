"""
This example demonstrates how to create a job and a file and write and read data to it.
"""

from lhcbdirac_log.providers.memory import MemoryDataProvider


def main() -> None:
    """
    This example demonstrates how to create a job and a file and write and read data to it and delete it.
    """

    with MemoryDataProvider("example") as pr:
        job = pr.create(42)  # create a new job with the ID 42

        data = job.create("my-data.txt")  # create a new data with the name "my-data.txt"

        with data.writer() as writer:
            writer.write(b"Hello, World!")  # write the data

        with data.reader() as reader:
            print(reader.read())  # prints b"Hello, World!"

        data.delete()  # delete the data
        job.delete("my-data.txt")  # same as data.delete()

        pr.delete(42)  # delete the job


if __name__ == '__main__':
    main()
