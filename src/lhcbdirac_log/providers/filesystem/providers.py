"""Implementation of the data provider layer for the local filesystem.

This module provides the filesystem implementation of providers classes for the data provider layer.

Classes:
    - FSDataProvider: Filesystem data provider implementation.
    - FSDictProvider: Filesystem dictionary provider implementation.
"""

from collections.abc import Generator
from os import PathLike
from pathlib import Path
from shutil import rmtree
from typing import override

from ...config import DEFAULT_CONFIG, Config
from ..base import DataExistsError, DataProvider, DictExistsError, DictNotExistsError, DictProvider, JobNotExistsError
from .accessors import FSDictEntry, FSJobEntry

__all__ = (
    "FSDataProvider",
    "FSDictProvider",
)


class FSDictProvider(DictProvider[FSDictEntry]):
    """Filesystem dictionary provider implementation."""

    __slots__ = ("_folder",)

    @property
    def folder(self) -> Path:
        """Get the folder path.

        Returns:
            the folder path
        """
        return self._folder

    def __init__(self, folder: PathLike[str] | str, config: Config = DEFAULT_CONFIG, *, readonly: bool = False) -> None:
        """Initialize the dictionary provider.

        Args:
            folder: the folder path, where to store the dictionaries
            config: the configuration to use for precomputing the dictionaries (default: DEFAULT_CONFIG)
            readonly: indicate weather the provider is read-only or not (default: False)
        """
        self._folder = Path(folder)
        self._folder.mkdir(parents=True, exist_ok=True)
        super().__init__(config, readonly=readonly)

    @override
    def _load(self, name: str) -> FSDictEntry:
        p = self._folder / name

        if not p.is_file():
            raise DictNotExistsError(p)

        return FSDictEntry(p, self._config)

    @override
    def _add(self, name: str, data: bytes, zstd_id: int) -> FSDictEntry:
        p = self._folder / name

        if p.is_file():
            raise DictExistsError(p)

        return FSDictEntry(p, self._config, data, zstd_id)

    @override
    def _iter_all(self) -> Generator[str, None, None]:
        return (f.name for f in self._folder.iterdir() if f.is_file())

    @override
    def _delete(self, name: str) -> None:
        p = self._folder / name

        try:
            p.unlink()
        except (FileNotFoundError, OSError) as err:
            raise DictNotExistsError(p) from err

    @override
    @property
    def size(self) -> int:
        return sum(f.stat().st_size for f in self._folder.iterdir() if f.is_file())


class FSDataProvider(DataProvider[FSJobEntry]):
    """Filesystem data provider implementation.

    This implementation supports both the old uncompressed and new compressed (Zstandard) formats.
    """

    __slots__ = ("_folder",)

    @property
    def folder(self) -> Path:
        """Get the folder path.

        Returns:
            the folder path
        """
        return self._folder

    def __init__(self, folder: PathLike[str] | str, dict_provider: DictProvider | None = None, *, readonly: bool = False) -> None:
        """Initialize the data provider.

        Args:
            folder: the folder path
            dict_provider: the dict provider associated to the data (default is None), specifying this implies that the provided data are compressed
            readonly: indicate weather the provider is read-only or not (default: False)
        """
        self._folder = Path(folder)
        self._folder.mkdir(parents=True, exist_ok=True)
        super().__init__(dict_provider, readonly=readonly)

    @override
    def _get(self, job: int, create: bool = False) -> FSJobEntry:
        return FSJobEntry(self._folder, job, compressed=self.compressed, readonly=self._readonly, create=create)

    @override
    def _create(self, job: int, exists_ok: bool = False) -> FSJobEntry:
        return FSJobEntry(self._folder, job, compressed=self.compressed, readonly=self._readonly, exists_ok=exists_ok)

    @override
    def jobs(self) -> Generator[int, None, None]:
        yield from map(int, (p.name for p in self._folder.iterdir() if p.is_dir() and p.name.isdecimal()))

    @override
    def _delete(self, job: int, *, force: bool = False) -> None:
        j = self.get(job)

        try:
            if force:
                rmtree(j.folder)
            else:
                j.folder.rmdir()
        except FileNotFoundError as err:  # pragma: no cover
            raise JobNotExistsError(job) from err
        except OSError as err:
            raise DataExistsError(job) from err
