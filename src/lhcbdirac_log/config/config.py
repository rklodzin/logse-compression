"""Configuration module for the library.

This module provides a configuration class `Config` that can be used to set setting used across the library.

The default configuration is stored in the `DEFAULT_CONFIG` variable and is often used as a default
argument in functions and methods, when a configuration is not provided.
"""

from zstandard import ZstdCompressionParameters

__all__ = (
    "DEFAULT_CONFIG",
    "Config",
)


class Config:
    """This class is used to store the configuration settings used by the library.

    A special instance of this class is stored in the `DEFAULT_CONFIG` variable and is often used as a default

    Notes:
        - an instance may not be modified after creation, depending of the settings used and if it is currently in use
    """

    # default compression settings
    DEFAULT_COMPRESSION_THREADS: int = 0  # single thread
    DEFAULT_COMPRESSION_LEVEL: int = 18  # <= 22

    DEFAULT_STREAM_READ_BUFFER: int = 1048576  # (de)compression stream buffer size
    DEFAULT_STREAM_WRITE_BUFFER: int = 1048576  # (de)compression stream buffer size

    DEFAULT_MAX_DATA_SIZE: int = 536870912  # maximum data size (default: 512 MB)

    # default training settings
    DEFAULT_TRAIN_MAX_SAMPLE = 9999  # sample size (in files number)
    DEFAULT_TRAIN_MAX_SIZE = 50 << 20  # sample size (in bytes, 50 MiB)
    DEFAULT_TRAIN_DICT_SIZE_FACTOR = 70  # recommended between 10 and 100 (~ sample size / trained dict size)
    DEFAULT_TRAIN_QUEUE_SIZE = 400  # sample queue size threshold before training

    DEFAULT_TRAIN_SPLIT_POINT = 0.75  # split point (0.0 to 1.0)
    DEFAULT_TRAIN_LEVEL = 3  # compression level used to train the dictionary (only for dict testing?)
    DEFAULT_TRAIN_NOTIFICATIONS = 2  # notification level
    DEFAULT_TRAIN_THREADS = -1  # number of threads

    __slots__ = (
        "compression_level",
        "compression_thread",
        "params",
        "stream_read_buffer",
        "stream_write_buffer",
        "max_data_size",
        "train_dict_size_factor",
        "train_level",
        "train_max_sample",
        "train_max_size",
        "train_queue_size",
        "train_notifications",
        "train_split_point",
        "train_threads",
    )

    def __init__(
        self,
        compression_level: int = DEFAULT_COMPRESSION_LEVEL,
        compression_thread: int = DEFAULT_COMPRESSION_THREADS,
        params: ZstdCompressionParameters | None = None,
        stream_read_buffer: int = DEFAULT_STREAM_READ_BUFFER,
        stream_write_buffer: int = DEFAULT_STREAM_WRITE_BUFFER,
        max_data_size: int = DEFAULT_MAX_DATA_SIZE,
        train_max_sample: int = DEFAULT_TRAIN_MAX_SAMPLE,
        train_max_size: int = DEFAULT_TRAIN_MAX_SIZE,
        train_dict_size_factor: int = DEFAULT_TRAIN_DICT_SIZE_FACTOR,
        train_queue_size: int = DEFAULT_TRAIN_QUEUE_SIZE,
        train_split_point: float = DEFAULT_TRAIN_SPLIT_POINT,
        train_level: int = DEFAULT_TRAIN_LEVEL,
        train_notifications: int = DEFAULT_TRAIN_NOTIFICATIONS,
        train_threads: int = DEFAULT_TRAIN_THREADS,
    ) -> None:
        """Initialize the configuration.

        Args:
            compression_level: the compression level to use (up to 22)
            compression_thread: the number of threads to use (0: single thread, -1: all cpu, >0: number of threads)
            params: the compression parameters to use (default is based on compression_level and compression_thread,
            with write_checksum, write_dict_id and write_content_size set to True)
            stream_read_buffer: the stream read buffer size
            stream_write_buffer: the stream write buffer size
            max_data_size: the maximum data size allowed for compression, above will be rejected

            train_max_sample: the maximum number of samples used for training 1 dictionary
            train_max_size: the maximum size of the dataset used for training 1 dictionary
            train_dict_size_factor: the factor used to calculate the final dictionary size from the dataset size
            train_queue_size: the queue size threshold before training
            train_split_point: the split point used to split the dataset into training and validation sets, must be between 0.0 and 1.0
            train_level: the compression level used to train the dictionary
            train_notifications: the notification level used to by the trainer (0: none, 1: errors, 2: progressive info, 3: more info, 4: all info)
            train_threads: the number of threads used by the trainer (-1: auto/all, 0: single thread, >0: number of threads)
        """
        self.compression_level = compression_level
        self.compression_thread = compression_thread or 1

        self.params = (
            ZstdCompressionParameters.from_level(
                compression_level,
                write_checksum=True,  # ensure data integrity
                write_dict_id=True,  # ensure dictionary id is written
                write_content_size=True,  # allow faster decompression
                threads=self.compression_thread,  # threads
            )
            if params is None
            else params
        )

        self.stream_read_buffer = stream_read_buffer
        self.stream_write_buffer = stream_write_buffer

        self.max_data_size = max_data_size

        self.train_max_sample = train_max_sample
        self.train_max_size = train_max_size
        self.train_dict_size_factor = train_dict_size_factor
        self.train_queue_size = train_queue_size
        self.train_split_point = train_split_point
        self.train_level = train_level
        self.train_notifications = train_notifications
        self.train_threads = train_threads


DEFAULT_CONFIG = Config()
