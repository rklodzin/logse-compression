"""This module provides the thread pools classes, allowing parallelisation of zstd processing.

The dispatching system of the tasks is decentralized, all tasks are put in a common queue, and workers
will take tasks from this queue to process them. The workers are managed by the thread pool, and the
tasks are managed by the task queue.

Classes:
    - ThreadPool: an abstract thread pool managing workers and their tasks.
    - CompressorPool: a thread pool managing data compressors.
    - DecompressorPool: a thread pool managing data decompressors.
"""

import os
from abc import ABC
from time import time
from typing import ClassVar, override

from ...config import DEFAULT_CONFIG, Config
from ...providers import DataEntry, DictProvider
from ..processors import ZstandardCompressor, ZstandardDecompressor, ZstandardProcessor
from .processor import ProcessorPool
from .queue import TaskQueue
from .task import ProcessingTask, Task
from .worker import Worker

__all__ = (
    "CompressorPool",
    "DecompressorPool",
    "ThreadPool",
)


class ThreadPool[P: ZstandardProcessor](ABC):
    """A thread pool managing workers and the task queue.

    Notes:
        - Must be subclassed with defined processor type
    """

    PROCESSOR_TYPE: ClassVar[type[ZstandardProcessor]]
    TRAINING_ENABLED: ClassVar[bool]

    __slots__ = (
        "_stopping",
        "_queue",
        "_dict_provider",
        "_processor_type",
        "_procpool",
        "_workpool",
        "_config",
    )

    @override
    def __init_subclass__(cls, *, processor: type[P] | None = None, training: bool | None = None) -> None:
        """Initialize the subclass.

        Args:
            processor: the processor type to use
            training: indicate if training is enabled or not

        Raises:
            TypeError: if any is None or if processor is not a ZstandardProcessor subclass
        """
        if processor is None or not issubclass(processor, ZstandardProcessor):
            msg = f"'processor' subclass argument is not or badly defined: {None if processor is None else processor.__qualname__} is not a ZstandardProcessor subclass"
            raise TypeError(msg)

        if training is None:
            msg = f"'training' subclass argument is not defined for {cls.__qualname__}"
            raise TypeError(msg)

        cls.PROCESSOR_TYPE = processor
        cls.TRAINING_ENABLED = training

    def __init__(
        self,
        dict_provider: DictProvider,
        config: Config = DEFAULT_CONFIG,
        workers: int = os.cpu_count() or 2,
        processors: int | None = None,
    ) -> None:
        """Initialize the pool.

        Args:
            dict_provider: the dictionary provider to use
            config: the configuration to use (default: DEFAULT_CONFIG)
            workers: the fixed number of workers (default: os.cpu_count() or 2)
            processors: the fixed number of processors, gave as positive (workers <= processors),
                or the variable number of processors, gave as negative (1 <= -processors <= workers,
                where the real number of processors is: N = max(workers, (loaded dict + 1) * -processors). (default None, maximum)

        Notes:
            The number of processors and workers can be controlled independently, because of the following reasons:
            - a worker can process one task at a time, and a task can be processed by one worker only at a time
            - a task use a processor to process data, and a processor can process data for one task only at a time
            - processor use a zstd dictionary for processing data, but changing the dictionary is expensive
              so it is better to keep the same dictionary for a while, and so to have many processors with different dictionaries
        """
        super().__init__()

        self._config = config
        self._queue = TaskQueue(workers * self.TRAINING_ENABLED, dict_provider, config)
        self._dict_provider = dict_provider

        self._processor_type: type[P] = self.PROCESSOR_TYPE  # type: ignore  # mypy failure

        self._workpool = tuple(Worker(self._queue) for _ in range(workers))
        self._procpool = ProcessorPool[P](lambda: self._processor_type(config), workers, processors)
        self._stopping = False

    @property
    def running(self) -> bool:
        """Check if the pool is running.

        Returns:
            True if the pool is running, False otherwise
        """
        return any(w.running for w in self._workpool)

    @property
    def stopping(self) -> bool:
        """Check if the pool is stopping.

        Returns:
            True if the pool is stopping, False otherwise
        """
        return self._stopping

    @property
    def stopped(self) -> bool:
        """Check if the pool is stopped.

        Returns:
            True if the pool is stopped, False otherwise
        """
        return not self.running and self._stopping

    @property
    def rejected(self) -> list[tuple[Task, Exception]]:
        """Get the rejected tasks.

        Returns:
            the rejected tasks
        """
        return self._queue.rejected

    def add(self, data_in: DataEntry, data_out: DataEntry) -> None:
        """Add a task to the queue.

        Args:
            data_in: the input data accessor
            data_out: the output data accessor

        Raises:
            RuntimeError: if the pool is stopping

        Notes:
            - the pool will start if not running
        """
        if not self.running:
            self.start()

        if self._stopping:
            msg = f"{self.__class__.__qualname__} is stopping or stopped"
            raise RuntimeError(msg)

        self._queue.put(ProcessingTask(self._procpool, data_in, data_out, self._dict_provider, self._config))

    def start(self) -> None:
        """Start the pool.

        Notes:
            - do nothing if the pool is already running
        """
        if not self.running and not self._stopping:
            for w in self._workpool:
                w.start()

    def wait(self) -> None:
        """Wait until the task queue is empty."""
        if self.running:
            self._queue.join()

    def flush(self) -> None:
        """Flush all remaining incomplete training queues to training tasks.

        Must be called after all tasks are added to the queue.
        """
        if self.running:
            self._queue.flush()

    def stop(self) -> None:
        """Request the pool to stop.

        Notes:
            - this call wait() first, blocking
            - the pool will effectively stop after all workers are done
            - workers will effectively stop after all tasks are done (wait())
            - to stop the pool instantly, clear the queue first
        """
        self.wait()
        if not self._stopping and self.running:
            for _ in self._workpool:
                self._queue.put(None)  # one None for each worker

            self._stopping = True

    def join(self, timeout: float | None = None) -> None:
        """Join the pool.

        Args:
            timeout: the timeout in seconds, used to join the pool (default: None)
        """
        t = time()

        for w in self._workpool:
            w.join(timeout)

            if timeout is not None:
                timeout += t - (s := time())
                t = s
                if timeout <= 0:
                    break


class CompressorPool(
    ThreadPool[ZstandardCompressor],
    processor=ZstandardCompressor,
    training=True,
):
    """Thread pool implementation for data compression."""

    __slots__ = ()


class DecompressorPool(
    ThreadPool[ZstandardDecompressor],
    processor=ZstandardDecompressor,
    training=False,
):
    """Thread pool implementation for data decompression."""

    __slots__ = ()
