"""
Test the performance of the threading configuration
"""

import time

from lhcbdirac_log import CompressorPool
from lhcbdirac_log.providers.fsspec import RootDataProvider
from lhcbdirac_log.providers.filesystem import FSDataProvider
from lhcbdirac_log.providers.memory import MemoryDataProvider
from lhcbdirac_log.providers.database import SQLDriver, SQLDataProvider


if __name__ == '__main__':
    n = 100

    with RootDataProvider("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/logSE/lhcb/MC/2016/LOG/00211361/0000") as src:

        with SQLDataProvider(SQLDriver.create("test.db")) as dst:

            pool = CompressorPool(dst.dict_provider, workers=20)
            st = time.time()

            for x, job in enumerate(src):
                out = dst.create(job.job)
                out.update_info(job.info)

                for data in job:
                    pool.add(data, out.create(data.name))
                if x == n:
                    break

            pool.flush()
            pool.stop()
            pool.join()

            print("done in", time.time() - st)
            print(pool.rejected, "jobs rejected")
            print("size:", dst.data_size, dst.size)

            import code
            code.interact(local=locals())
