"""Implementation of the in-memory data provider layer.

This module provides the in-memory implementation of providers classes for the data provider layer.

Classes:
    - MemoryDictProvider: Memory dictionary provider implementation.
    - MemoryDataProvider: Memory data provider implementation.
"""

from collections.abc import Generator
from typing import override

from ...config import DEFAULT_CONFIG, Config
from ..base import DataExistsError, DataProvider, DictExistsError, DictNotExistsError, DictProvider
from .accessors import MemoryDictEntry, MemoryJobEntry

__all__ = (
    "MemoryDictProvider",
    "MemoryDataProvider",
)


class MemoryDictProvider(DictProvider[MemoryDictEntry]):
    """Memory dictionary provider implementation."""

    CACHE = dict[str, dict[str, bytes]]()

    __slots__ = ("_cache",)

    @classmethod
    def clear_cache(cls, name: str | None = None) -> None:
        """Clear the cache.

        Args:
            name: the name of the cache entry to clear (default: None, for all)
        """
        if name is not None:
            cls(name).clear()
            del cls.CACHE[name]
        else:
            for name in cls.CACHE:
                cls(name).clear()

            cls.CACHE.clear()

    def __init__(self, name: str, config: Config = DEFAULT_CONFIG, *, readonly: bool = False) -> None:
        """Initialize the dictionary provider.

        Args:
            name: the provider name in the cache entry
            config: the configuration to use for precomputing the dictionaries (default: DEFAULT_CONFIG)
            readonly: indicate weather the provider is read-only or not (default: False)

        Notes:
            - The cache must be manually cleared using the `clear_cache` class method.
        """
        self._cache = self.CACHE.setdefault(name, {})
        super().__init__(config, readonly=readonly)

    @override
    def _load(self, name: str) -> MemoryDictEntry:
        if name not in self._cache:
            raise DictNotExistsError(name)

        return MemoryDictEntry(name, self._cache, self._config)

    @override
    def _add(self, name: str, data: bytes, zstd_id: int) -> MemoryDictEntry:
        if name in self._cache:
            raise DictExistsError(name)

        return MemoryDictEntry(name, self._cache, self._config, data, zstd_id)

    @override
    def _iter_all(self) -> Generator[str, None, None]:
        yield from list(self._cache)

    @override
    def _delete(self, name: str) -> None:
        if self._cache.pop(name, None) is None:
            raise DictNotExistsError(name)

    @override
    @property
    def size(self) -> int:
        return sum(map(len, self._cache.values()))


class MemoryDataProvider(DataProvider[MemoryJobEntry]):
    """Memory data provider implementation.

    This implementation supports both the old uncompressed and new compressed (Zstandard) formats.
    """

    CACHE = dict[str, dict[int, dict[str, bytearray]]]()

    __slots__ = ("_cache",)

    @classmethod
    def clear_cache(cls, name: str | None = None) -> None:
        """Clear the cache.

        Args:
            name: the name of the cache entry to clear (default: None, for all)
        """
        if name is not None:
            cls(name).clear(force=True)
            del cls.CACHE[name]
        else:
            for name in cls.CACHE:
                cls(name).clear(force=True)

            cls.CACHE.clear()

    def __init__(self, name: str, dict_provider: DictProvider | None = None, *, readonly: bool = False) -> None:
        """Initialize the data provider.

        Args:
            name: the provider name in the cache entry
            dict_provider: the dict provider associated to the data (default is None), specifying this implies that the provided data are compressed
            readonly: indicate weather the provider is read-only or not (default: False)
        """
        self._cache = self.CACHE.setdefault(name, {})
        super().__init__(dict_provider, readonly=readonly)

    @override
    def _get(self, job: int, *, create: bool = False) -> MemoryJobEntry:
        return MemoryJobEntry(self._cache, job, compressed=self.compressed, readonly=self._readonly, create=create)

    @override
    def _create(self, job: int, *, exists_ok: bool = False) -> MemoryJobEntry:
        return MemoryJobEntry(self._cache, job, compressed=self.compressed, readonly=self._readonly, exists_ok=exists_ok)

    @override
    def jobs(self) -> Generator[int, None, None]:
        yield from list(self._cache)

    @override
    def _delete(self, job: int, *, force: bool = False) -> None:
        j = self.get(job)

        if force:
            j.clear()
        elif any(j.files()):
            raise DataExistsError(job)

        self._cache.pop(job)
