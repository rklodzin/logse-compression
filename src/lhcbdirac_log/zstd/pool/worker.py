"""This module provides the thread worker class for the zstd module.

Classes:
    - Worker: a thread worker class that processes tasks from the specified task queue.
"""

from __future__ import annotations

from threading import Thread
from typing import override

from .queue import TaskQueue

__all__ = ("Worker",)


class Worker(Thread):
    """[Internal] A thread worker class that processes tasks from the specified task queue.

    Notes:
        - the worker will stop if None is fetched from the queue
        - the worker signals the queue that tasks are done
        - stopped workers cannot be restarted (python threads limitation)
    """

    _COUNT = 0

    __slots__ = ("_queue",)

    def __init__(self, queue: TaskQueue) -> None:
        """Initialize the worker.

        Args:
            queue: the task queue to get tasks from
        """
        self._inc_count()
        super().__init__(name=f"{self.__class__.__qualname__}-{self._COUNT}")
        self._queue = queue

    @property
    def running(self) -> bool:
        """Check if the worker is running.

        Returns:
            True if the worker is running, False otherwise
        """
        return self.is_alive()

    @override
    def run(self) -> None:
        """The worker's main loop.

        Notes:
            - the worker will stop if None is fetched from the queue
            - the worker signals the queue that tasks are done
        """
        while (task := self._queue.get()) is not None:
            try:
                task.run()
            except Exception as e:
                self._queue.reject(task, e)
            finally:
                self._queue.task_done()

        self._queue.task_done()

    @classmethod
    def _inc_count(cls) -> None:
        """Increment the instance counter."""
        cls._COUNT += 1
