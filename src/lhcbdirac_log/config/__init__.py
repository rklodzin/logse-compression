"""Configuration module for the library.

This module provides a configuration class `Config` that can be used to set setting used across the library.

The default configuration is stored in the `DEFAULT_CONFIG` variable and is often used as a default
argument in functions and methods, when a configuration is not provided.
"""

from .config import DEFAULT_CONFIG, Config

__all__ = (
    "DEFAULT_CONFIG",
    "Config",
)
