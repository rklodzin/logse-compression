"""This package provides the Zstandard compressor and decompressor wrapper classes.

These classes respect a common interface through ZstandardProcessor and can be used interchangeably.
The ZstandardTrainer class is also provided through the same API but is a little special.

Classes:
    - ZstandardCompressor: Zstandard compressor wrapper class.
    - ZstandardDecompressor: Zstandard decompressor wrapper class.
    - ZstandardProcessor: Zstandard base processor abstract interface.
    - ZstandardTrainer: Zstandard trainer wrapper class.
"""

from .compressor import ZstandardCompressor
from .decompressor import ZstandardDecompressor
from .processor import ZstandardProcessor
from .trainer import ZstandardTrainer

__all__ = (
    "ZstandardCompressor",
    "ZstandardDecompressor",
    "ZstandardProcessor",
    "ZstandardTrainer",
)
