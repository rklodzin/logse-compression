"""This module provides the common abstract base class for Zstandard processor modules.

Classes:
    - ZstandardProcessor: Zstandard processor abstract base class.
"""

from abc import ABC, abstractmethod
from collections.abc import Iterator
from typing import BinaryIO, final

from ...config import DEFAULT_CONFIG, Config
from ...providers import DictEntry

__all__ = ("ZstandardProcessor",)


class ZstandardProcessor[T](ABC):
    """Zstandard processor abstract base class.

    Provides a common interface for Zstandard compressor and decompressor wrappers, and trainer too.
    """

    __slots__ = (
        "_config",
        "_dict",
        "_processor",
    )

    def __init__(self, config: Config = DEFAULT_CONFIG, dict_entry: DictEntry | None = None) -> None:
        """Initialize the processor.

        Args:
            config: the configuration to use for the processing (default is DEFAULT_CONFIG)
            dict_entry: the dictionary entry to use (can be set/changed later)
        """
        super().__init__()

        self._dict = dict_entry
        self._config = config
        self._processor: T | None = None

    @property
    @final
    def dict_name(self) -> str | None:
        """Get the dictionary name of the processor or None.

        Returns:
            the dictionary name or None if no dictionary is set
        """
        if self._dict is None:
            return None

        return self._dict.dict_name

    @property
    def dict(self) -> DictEntry | None:
        """Get the dictionary entry.

        Returns:
            the dictionary entry or None if no dictionary is set
        """
        return self._dict

    @dict.setter
    def dict(self, value: DictEntry) -> None:
        """Set the dictionary.

        Args:
            value: the dictionary entry

        Notes:
            - Setting a different dictionary will reset the internal processor
        """
        if self._dict is not value:
            self._dict = value
            self._processor = None  # dict cannot be changed on the fly, so we need to reset it

    @property
    @final
    def processor(self) -> T:
        """Get the internal processor.

        Returns:
            the internal processor
        """
        if self._processor is None:
            self._processor = self._new()
        return self._processor

    @abstractmethod
    def _new(self) -> T:
        """[Internal] Create a new internal processor.

        Returns:
            a new internal processor
        """

    @abstractmethod
    def process(self, *data: bytes | bytearray | memoryview) -> bytes | Iterator[bytes]:
        """Process the provided data.

        Args:
            *data: the data to process

        Returns:
            a bytes object (if single output) or
            a bytes iterator

        Notes:
            - empty data will return an empty bytes
        """

    @abstractmethod
    def process_stream(
        self,
        instream: BinaryIO,
        outstream: BinaryIO,
        insize: int = -1,
    ) -> tuple[int, int]:
        """Process the provided stream.

        Args:
            instream: the stream to process
            outstream: the stream to write the processed data to
            insize: the size of the input stream, -1 for unknown

        Returns:
            a tuple with the number of bytes read and written
        """
