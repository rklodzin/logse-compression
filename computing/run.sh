#!/usr/bin/env bash


export LOG_SRC_PATH
export LOG_PROCESSING_WORKER

LP=$LOG_SRC_PATH
export LP  # Copy because LbEnv erases $LOG_SRC_PATH (why???)

source /cvmfs/lhcb.cern.ch/lib/LbEnv

set -euo pipefail
IFS=$'\n\t'

LOG_SRC_PATH=$LP

export LOG_SRC_PATH
export LOG_PROCESSING_WORKER

exec lb-conda -e LOG_SRC_PATH=$LOG_SRC_PATH -e LOG_PROCESSING_WORKER=$LOG_PROCESSING_WORKER -e TOKEN=$TOKEN computing/dirac-log-convert python process.py
