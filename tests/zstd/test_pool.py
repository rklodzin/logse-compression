"""
Test the zstd thread pool.
"""
import os

import pytest
from zstandard import ZstdError

from lhcbdirac_log import ThreadPool, ZstandardCompressor, CompressorPool, DecompressorPool, DictProvider, DataProvider, DataNotExistsError


class TestPool:
    """Test the zstd thread pool."""

    def test_subclass(self) -> None:
        """Test the subclass."""

        with pytest.raises(TypeError):
            _ = type("InvalidPool", (ThreadPool,), {})

        proc = (None, int, ZstandardCompressor)
        train = (None, True, False)

        for p in proc:
            with pytest.raises(TypeError):
                _ = type("InvalidPool", (ThreadPool,), {}, processor=p)

            for t in train:
                with pytest.raises(TypeError):
                    _ = type("InvalidPool", (ThreadPool,), {}, training=t)

                if p is ZstandardCompressor and t is not None:
                    _ = type("ValidPool", (ThreadPool,), {}, processor=ZstandardCompressor, training=t)
                else:
                    with pytest.raises(TypeError):
                        _ = type("ValidPool", (ThreadPool,), {}, processor=p, training=t)

    def test_run(self, dict_provider: DictProvider) -> None:
        """Test the thread management."""

        pool = CompressorPool(dict_provider)
        assert not pool.running
        assert not pool.stopping

        pool.stop()  # do nothing

        assert not pool.running
        assert not pool.stopping

        pool.start()

        assert pool.running
        assert not pool.stopping

        pool.start()  # do nothing
        pool.start()  # do nothing

        pool.wait()  # do nothing
        pool.stop()
        pool.stop()  # do nothing
        pool.start()  # do nothing

        assert pool.stopping

        pool.join()

        assert not pool.running
        assert pool.stopping  # not reset
        assert pool.stopped

    def test_add(self, compressed_data_provider: DataProvider) -> None:
        """Test adding tasks to the pool."""

        pool = CompressorPool(compressed_data_provider.dict_provider)
        compressed_data_provider.dict_provider.mark_invalid("dxxxx")
        job = compressed_data_provider.create(99)

        pool.add(job.create("d0000"), job.create("d0001"))

        assert pool.running  # started automatically by add
        assert not pool.stopping

        pool.stop()

        assert pool.stopping

        with pytest.raises(RuntimeError):
            pool.add(job.create("d0000"),
                     job.get("d0001"))

        assert pool.stopping

        pool.join(0)  # with timeout
        pool.join()  # without timeout

        assert not pool.running
        assert pool.stopping

    def test_process(self,
                     empty_decompressed_data_provider: DataProvider,
                     empty_compressed_data_provider: DataProvider,
                     alt_empty_decompressed_data_provider: DataProvider) -> None:
        """Test the processing."""

        # in -> out -> result: check in == result
        input_data = empty_decompressed_data_provider
        output_data = empty_compressed_data_provider
        result_data = alt_empty_decompressed_data_provider

        config = output_data.dict_provider.config
        t = config.train_queue_size
        old_m = config.max_data_size
        old_tm = config.train_max_size
        m = config.train_max_size = config.max_data_size = 10 << 20  # 10 MB

        try:
            cpool = CompressorPool(output_data.dict_provider, config)
            dpool = DecompressorPool(output_data.dict_provider, config)
            xpool = DecompressorPool(output_data.dict_provider, config)

            num = data_size = 0

            # setup data

            for i in range(t + 10):
                j = input_data.create(i + 1)
                enough_th = i >= t - 10

                for enough in ('n', 'e'):
                    if enough_th and enough == "n":
                        continue

                    for variant in ('r', 'g'):  # v: regular series, g: regular with some giant data
                        regular = variant == 'r' or (0 < i < (t + 9 if enough == 'e' else t - 11))

                        # empty
                        with j.create(f"e-{enough}{variant}{i:04}").writer() as w:
                            w.write(b"" if regular else os.urandom(m))
                            num += 1
                            data_size += 0 if regular else m

                        # not empty
                        with j.create(f"a-{enough}{variant}{i:04}").writer() as w:
                            w.write(os.urandom(1024) if regular else os.urandom(m))
                            num += 1
                            data_size += 1024 if regular else m

                        # partially empty
                        with j.create(f"p-{enough}{variant}{i:04}").writer() as w:
                            num += 1
                            if i % 2 or not i:
                                w.write(os.urandom(1024) if regular else os.urandom(m))
                                data_size += 1024 if regular else m

                        # not for all job
                        if i % 10 == 0 or (i + 1) % 10 == 0:
                            # empty
                            with j.create(f"x-{enough}{variant}{i:04}").writer() as w:
                                w.write(b"" if regular else os.urandom(m))
                                num += 1
                                data_size += 0 if regular else m

                            # not empty
                            with j.create(f"y-{enough}{variant}{i:04}").writer() as w:
                                w.write(os.urandom(1024) if regular else os.urandom(m))
                                num += 1
                                data_size += 1024 if regular else m

                            # partially empty
                            with j.create(f"z-{enough}{variant}{i:04}").writer() as w:
                                num += 1
                                if i % 20 or not i:
                                    w.write(os.urandom(1024) if regular else os.urandom(m))
                                    data_size += 1024 if regular else m

                if i == 0:
                    with j.create(f"o-g{i:04}").writer() as w:  # giant, unique
                        w.write(os.urandom(m))
                        num += 1
                        data_size += m

            assert input_data.data_size == data_size

            for job in input_data:
                out = output_data.create(job.job)
                for data in job:
                    cpool.add(data, out.create(data.name))

            cpool.flush()
            cpool.stop()
            cpool.join()

            assert len(cpool.rejected) == 25
            r, x = list(zip(*cpool.rejected))
            assert all('g' in i.data_in.name for i in r)
            assert all(isinstance(e, ValueError) and "too large" in e.args[0] for e in x)

            for job in output_data:
                out = result_data.create(job.job)
                for data in job:
                    dpool.add(data, out.create(data.name))

            dpool.flush()
            dpool.stop()
            dpool.join()

            assert len(dpool.rejected) == 0

            for job in input_data:
                rjob = result_data.get(job.job)

                for data in job:
                    if data.size >= m:
                        with pytest.raises(DataNotExistsError):
                            rjob.get(data.name)
                    else:
                        with data.reader() as r, rjob.get(data.name).reader() as e:
                            assert r.read() == e.read()

            # try decompression with lost dict
            try:
                output_data.dict_provider.clear()
            except Exception:
                pass
            else:
                result_data.clear(force=True)

                for job in output_data:
                    out = result_data.create(job.job)
                    for data in job:
                        xpool.add(data, out.create(data.name))

                xpool.flush()
                xpool.stop()
                xpool.join()

                assert len(xpool.rejected) > 0
                assert all(isinstance(e, ZstdError) and "mismatch" in e.args[0] for t, e in xpool.rejected)

        finally:
            config.max_data_size = old_m
            config.train_max_size = old_tm
