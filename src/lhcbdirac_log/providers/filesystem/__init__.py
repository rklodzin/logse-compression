"""Implementation of the data provider layer for the local filesystem.

This implementation supports both the old uncompressed and new compressed (Zstandard) formats.

This implementation stores the data in files, in directories named after the job id (8 digits zero-padded).

Classes:
    - FSDataEntry: Filesystem data entry implementation.
    - FSDictEntry: Filesystem dictionary entry implementation.
    - FSJobEntry: Filesystem job entry implementation.
    - FSDataProvider: Filesystem data provider implementation.
    - FSDictProvider: Filesystem dictionary provider implementation.
"""

from .accessors import FSDataEntry, FSDictEntry, FSJobEntry
from .providers import FSDataProvider, FSDictProvider

__all__ = (
    "FSDataEntry",
    "FSDataProvider",
    "FSDictEntry",
    "FSDictProvider",
    "FSJobEntry",
)
