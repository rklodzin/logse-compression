"""Implementation of the data provider layer for fsspec supported (remote) filesystems.

This module provides the fsspec implementation of entries classes for the data provider layer.

Classes:
    - RootDataEntry: fsspec data entry implementation.
    - RootJobEntry: fsspec job entry implementation.
"""

from __future__ import annotations

from collections.abc import Generator
from typing import IO, BinaryIO, override
from zipfile import ZipFile

from ..base import DataEntry, DataNotExistsError, JobEntry, JobNotExistsError, ReadOnlyError
from .driver import RootDriver

__all__ = (
    "RootDataEntry",
    "RootJobEntry",
)


class RootDataEntry(DataEntry):
    """Fsspec data entry implementation.

    Only supports the old uncompressed (means ZIP) format.
    Only supports read-only mode.
    """

    __slots__ = ("_job_entry",)

    def __init__(self, job: RootJobEntry, name: str) -> None:
        """[Internal] Initialize the data entry.

        Args:
            job: the job entry
            name: the data name
        """
        self._job_entry = job
        super().__init__(name, job.job, compressed=False, readonly=True)

    @override
    def _reader(self) -> BinaryIO | IO[bytes]:
        try:
            return self._job_entry.reader.open(f"{self._job_entry.prefix}{self._name}", "r")
        except FileNotFoundError as err:  # pragma: no cover
            raise DataNotExistsError(self._name) from err

    @override
    def _writer(self) -> BinaryIO:  # pragma: no cover
        pass

    @override
    def _size(self) -> int | None:
        try:
            return self._job_entry.reader.getinfo(f"{self._job_entry.prefix}{self._name}").file_size
        except FileNotFoundError:  # pragma: no cover
            return None

    @override
    def _delete(self) -> None:  # pragma: no cover
        pass


class RootJobEntry(JobEntry[RootDataEntry]):
    """Fsspec job entry implementation.

    Only supports the old uncompressed (means ZIP) format.
    Only supports read-only mode.
    """

    __slots__ = (
        "_driver",
        "_prefix",
        "_reader",
        "_path",
    )

    def __init__(self, driver: RootDriver, job: int) -> None:
        """[Internal] Initialize the job entry.

        Args:
            driver: the root driver
            job: the job id

        Raises:
            JobNotExistsError: if the job does not exist
        """
        self._driver = driver
        self._reader: ZipFile | None = None

        self._path = f"{self._driver.path}/{job:08}.zip"

        if not self._driver.fs.isfile(self._path):
            raise JobNotExistsError(job)

        self._prefix = f"{job:08}/"

        super().__init__(job, compressed=False, readonly=True)

    @property
    def driver(self) -> RootDriver:
        """Get the root driver.

        Returns:
            the root driver
        """
        return self._driver

    @property
    def prefix(self) -> str:
        """Get the job path prefix with trailing slash.

        Returns:
            the path prefix (e.g. "00000001/")
        """
        return self._prefix

    @property
    def reader(self) -> ZipFile:
        """Get the zip-file handler.

        Returns:
            the zip reader
        """
        if self._reader is None:
            self._reader = ZipFile(self._driver.fs.open(self._path))

        return self._reader

    @override
    def _get(self, name: str, *, create: bool = False) -> RootDataEntry:
        r = self.reader
        zname = f"{self._prefix}{name}"

        try:
            r.getinfo(zname)  # raises KeyError if the file does not exist
        except KeyError as err:
            raise DataNotExistsError(name) from err

        return RootDataEntry(self, name)

    @override
    def _create(self, name: str, *, exists_ok: bool = False) -> RootDataEntry:  # pragma: no cover
        pass

    @override
    def files(self) -> Generator[str, None, None]:
        yield from (i.filename.removeprefix(self._prefix) for i in self.reader.infolist())

    @override
    @property
    def job_size(self) -> int:  # optimizes the default implementation
        return self._driver.fs.size(self._path) or 0

    @override
    @property
    def data_size(self) -> int:  # optimizes the default implementation
        return sum(i.file_size for i in self.reader.infolist())

    @override
    def delete(self, name: str) -> None:  # optimizes the default implementation
        raise ReadOnlyError(name)

    @override
    def __len__(self) -> int:
        return len(self.reader.infolist())

    def __del__(self) -> None:
        """Close the zip-file reader."""
        if self._reader is not None:
            fp = self._reader.fp
            self._reader.close()

            if fp is not None:
                fp.close()

            self._reader = None

    @override
    def _update_info(self) -> None:  # pragma: no cover
        pass
