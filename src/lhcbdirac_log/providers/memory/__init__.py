"""Implementation of the in-memory data provider layer.

This implementation supports both the old uncompressed and new compressed (Zstandard) formats.

This implementation stores the data in memory, in a cache of Python dictionaries objects.

Classes:
    - MemoryDataEntry: Memory data entry implementation.
    - MemoryDictEntry: Memory dictionary entry implementation.
    - MemoryJobEntry: Memory job entry implementation.
    - MemoryDataProvider: Memory data provider implementation.
    - MemoryDictProvider: Memory dictionary provider implementation.
    - MemoryDataIO: Memory data file-like IO implementation.
"""

from .accessors import MemoryDataEntry, MemoryDataIO, MemoryDictEntry, MemoryJobEntry
from .providers import MemoryDataProvider, MemoryDictProvider

__all__ = (
    "MemoryDataEntry",
    "MemoryDataProvider",
    "MemoryDictEntry",
    "MemoryDictProvider",
    "MemoryJobEntry",
    "MemoryDataIO",
)
