"""Base module for the data provider layer of the library.

This defines provider base classes for job/data and dict access.

Classes:
    - Provider: base provider class, provides common methods and properties for all providers.
    - DictProvider: base dict provider class, provides an abstraction layer for dict management.
    - DataProvider: base data provider class, provides an abstraction layer for data management.
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Generator, Iterator
from shutil import copyfileobj
from types import TracebackType
from typing import Self, final, override

from ...config import Config
from .accessors import DictEntry, Entry, JobEntry
from .exceptions import DictExistsError, DictInvalidError, DictNotExistsError, ReadOnlyError

__all__ = (
    "DataProvider",
    "DictProvider",
    "Provider",
)


class Provider[E: Entry](ABC):
    """Base provider class.

    Defines common methods and properties for all providers subclasses.
    A provider manages its related entries.
    """

    __slots__ = ("_readonly",)

    def __init__(self, *, readonly: bool) -> None:
        """[Internal] Initialize the provider.

        Args:
            readonly: indicate weather the provider is read-only or not
        """
        super().__init__()

        self._readonly = readonly

    @property
    def readonly(self) -> bool:
        """Check if the provider is read-only.

        Returns:
            True if the provider is read-only, False otherwise
        """
        return self._readonly

    @property
    @final
    def empty(self) -> bool:
        """Check if the provider is empty.

        Returns:
            True if the provider is empty, False otherwise
        """
        return not len(self)

    def open(self) -> None:
        """Open the provider internal resources.

        Must be called before using the provider.

        Notes:
            - can be called multiple times without issues (reentrant)
            - may not be called after closing
            - called when entering the context manager
        """
        return

    def close(self) -> None:
        """Close the provider internal resources.

        Must be called after using the provider.

        Notes:
            - can be called multiple times without issues
            - may not be called before opening
            - called when exiting the context manager
        """
        return

    @final
    def __enter__(self) -> Self:
        """Alias for provider.open().

        Returns:
            the provider itself

        Notes:
            - call open() when entering the context manager
            - can be called multiple times without issues (reentrant)
            - may not be called after closing
        """
        self.open()
        return self

    @final
    def __exit__(
        self,
        exc_type: type[BaseException] | None,
        exc_val: BaseException | None,
        exc_tb: TracebackType | None,
    ) -> None:
        """Alias for provider.close().

        Args:
            exc_type: the exception type
            exc_val: the exception value
            exc_tb: the exception traceback

        Notes:
            - call close() when exiting the context manager
            - can be called multiple times without issues
        """
        self.close()

    @property
    @abstractmethod
    def size(self) -> int:
        """Get the total size of the provider.

        Returns:
            the total size
        """

    @abstractmethod
    def __len__(self) -> int:
        """Get the number of entries.

        Returns:
            the number of entries
        """


class DictProvider[E: DictEntry](Provider[E], ABC):
    """Base class for Zstandard dictionary provider implementations, providing an abstraction layer for dictionary management.

    Dict providers are used to stored Zstandard dictionaries, used for both compression and decompression.

    The provider manages the dictionaries and provides access to them.
    Dict entries are used to access dictionaries data, identified by a unique name.
    The name of a dictionary is the common part between all related files = the filenames without numbers.

    E.g.:
        - Both log_data_0001.xml and log_data_0042.xml are compressed with the same dictionary, named log_data_xxxx.xml.

    Dictionaries are loaded on demand and are marked as missing if they do not exist.
    This marking can be used to avoid trying to load them again if they are not found.

    Dictionaries that failed their training are marked as invalid.
    Whereas missing dictionaries, invalid dictionaries marks can be persistent, depending on the implementation.
    """

    __slots__ = (
        "_config",
        "_dicts",
        "_invalid",
        "_missing",
    )

    def __init__(self, config: Config, *, readonly: bool) -> None:
        """[Internal] Initialize the provider.

        Args:
            config: the configuration to use for precomputing the dictionaries
            readonly: indicate weather the provider is read-only or not
        """
        super().__init__(readonly=readonly)
        self._dicts = dict[str, E]()
        self._invalid = self._load_invalid()
        self._missing = set[str]()
        self._config = config

    def _load_invalid(self) -> set[str]:
        """[Internal] Load the invalid dicts names.

        Returns:
            the invalid dicts names

        Notes:
            - default implementation returns an empty set
            - must be implemented if invalid dicts are saved
        """
        return set()

    def _mark_invalid(self, name: str) -> None:
        """[Internal] Mark a dict as invalid.

        Args:
            name: the dict name

        Notes:
            - default implementation does nothing
            - must be implemented if invalid dicts are saved
        """

    @property
    def config(self) -> Config:
        """Get the configuration.

        Returns:
            the configuration
        """
        return self._config

    @final
    def is_invalid(self, name: str) -> bool:
        """Check if the dict is marked as invalid.

        Args:
            name: the dict name

        Returns:
            True if the dict is invalid, False otherwise

        Notes:
            - Invalid dicts are dicts that failed to train
            - These dicts may be marked only when a training was attempted
        """
        return name in self._invalid

    @final
    def is_missing(self, name: str) -> bool:
        """Check if the dict is marked as missing.

        Args:
            name: the dict name

        Returns:
            True if the dict is missing, False otherwise

        Notes:
            - Missing dicts are dicts that failed to load
            - These dicts are marked only when loading were attempted
            - Must not be confused with !is_loaded
        """
        return name in self._missing

    @final
    def is_loaded(self, name: str) -> bool:
        """Check if the dict is loaded (exists in cache).

        Args:
            name: the dict name

        Returns:
            True if the dict is loaded, False otherwise

        Notes:
            - same as: 'dict_name' in provider (__contains__)
        """
        return name in self

    @final
    def mark_invalid(self, name: str) -> None:
        """Mark a dict as invalid.

        Args:
            name: the dict name

        Raises:
            DictExistsError: if the dict is already loaded

        Notes:
            - unload if the dict is loaded but not exist anymore (may never happen under normal usages)
        """
        if self.is_loaded(name):
            if self[name].exists:
                raise DictExistsError(name)
            del self._dicts[name]

        if self.is_missing(name):
            self._missing.discard(name)

        self._mark_invalid(name)
        self._invalid.add(name)

    @final
    def mark_missing(self, name: str) -> None:
        """Mark a dict as missing.

        Args:
            name: the dict name

        Raises:
            DictExistsError: if the dict is already loaded
            DictInvalidError: if the dict is already marked as invalid

        Notes:
            - unload if the dict is loaded but not exist (may never happen under normal usages)
        """
        if self.is_loaded(name):
            if self[name].exists:
                raise DictExistsError(name)
            del self._dicts[name]

        if self.is_invalid(name):
            raise DictInvalidError(name)

        self._missing.add(name)

    @override
    @property
    def size(self) -> int:
        """Get the total size of all dicts data.

        Returns:
            the total size
        """
        return sum(self[i].size for i in self)

    @abstractmethod
    def _load(self, name: str) -> E:
        """Load a dict.

        Args:
            name: the dict name

        Raises:
            DictNotExistsError: if the dict does not exist
        """

    @abstractmethod
    def _add(self, name: str, data: bytes, zstd_id: int) -> E:
        """[Internal] Create a new dict entry.

        Args:
            name: the dict name
            data: the dict data
            zstd_id: the zstd dictionary id

        Raises:
            DictExistsError: if the dict already exists
        """

    @final
    def add(self, name: str, data: bytes, zstd_id: int, *, load: bool = True) -> E:
        """Add a new dict entry from data.

        Args:
            name: the dict name
            data: the dict data
            zstd_id: the zstd dictionary id
            load: if True, keep the dict loaded (default is True)

        Returns:
            the dict entry

        Raises:
            DictExistsError: if the dict already exists
            ReadOnlyError: if the provider is read-only
        """
        if self._readonly:
            msg = "The provider is read-only"
            raise ReadOnlyError(msg)

        d = self._add(name, data, zstd_id)
        if load:
            self._dicts[name] = d

        self._missing.discard(name)  # ensure valid state
        self._invalid.discard(name)  # ensure valid state
        return d

    @abstractmethod
    def _delete(self, name: str) -> None:
        """[Internal] Delete a dict entry.

        Args:
            name: the dict name to delete

        Raises:
            DictNotExistsError: if the dict does not exist
        """

    @final
    def delete(self, name: str) -> None:
        """Delete a dict entry.

        Args:
            name: the dict name to delete

        Raises:
            DictNotExistsError: if the dict does not exist
            ReadOnlyError: if the provider is read-only

        Notes:
            - may or may not check if data are linked to the dict before deletion
            - if the dict is loaded, it is obviously considered unloaded
            - instances of the related dict entry, if still accessible, may not be used nor trusted anymore
            - same as: del provider[dict_name]
        """
        if self._readonly:
            msg = "The provider is read-only"
            raise ReadOnlyError(msg)

        self._delete(name)
        self._dicts.pop(name, None)  # delete it from the loaded dicts if loaded

    @final
    def clear(self) -> None:
        """Clear all the existing dicts.

        Raises:
            ReadOnlyError: if the provider is read-only
        """
        if self._readonly:
            msg = "The provider is read-only"
            raise ReadOnlyError(msg)

        for i in self:
            del self[i]

    @final
    def transfer(self, target: DictProvider | None) -> int:
        """Transfer (copy) all dicts to another provider.

        Args:
            target: the target provider

        Returns:
            the number of transferred dicts

        Raises:
            ValueError: if the target provider is not specified (None)
            DictExistsError: if copied dicts already exists in the target provider
            ReadOnlyError: if the target provider is read-only

        Notes:
            - Nothing is transferred if the target provider is the same as the source provider
        """
        if self is target:
            return 0

        if target is None:
            msg = "The target provider must be specified"
            raise ValueError(msg)

        if target.readonly:
            msg = "The target provider is read-only"
            raise ReadOnlyError(msg)

        for i in self._invalid:
            target.mark_invalid(i)

        n = -1
        for n, i in enumerate(self):
            if (d := self.get(i)) is not None:
                target.add(i, d.data, d.zstd_id, load=False)

        return n + 1

    @final
    def _iter_loaded(self) -> Generator[str, None, None]:
        """[Internal] Get the loaded dicts names.

        Returns:
            a generator of the loaded dicts names
        """
        yield from self._dicts.keys()

    @abstractmethod
    def _iter_all(self) -> Generator[str, None, None]:
        """[Internal] Get all the dicts names (loaded and non-loaded/loadable).

        Returns:
            a generator of all the dicts names
        """

    @final
    def iter(self, *, loaded_only: bool = False) -> Generator[str, None, None]:
        """Get the dicts names.

        Args:
            loaded_only: if True, only returns the loaded dicts names, otherwise returns all loadable (default False)

        Returns:
            a generator of the dicts names
        """
        return self._iter_loaded() if loaded_only else self._iter_all()

    @final
    def get(
        self,
        name: str,
        default: E | None = None,
        *,
        invalid_ok: bool = False,
        missing_ok: bool = True,
    ) -> E | None:
        """Get the dict.

        Args:
            name: the dict name to get
            default: the default value returned on ignored errors (default is None)
            invalid_ok: if True, ignores invalid dict error and returns the default value (default is False)
            missing_ok: if True, ignores missing dict error and returns the default value (default is True)

        Returns:
            the dict entry or the default value

        Raises:
            DictInvalidError: if the dict is invalid and invalid_ok is False
            DictNotExistsError: if the dict does not exist and missing_ok is False

        Notes:
            - if invalid_ok and missing_ok are both False, then it is equivalent to provider[dict_name] (__getitem__)
            - if not found, the dict is marked as missing before raising (avoiding trying to load it again on next call)
            - getting the same dict multiple times will return the same object (cached)
        """
        try:
            return self[name]

        except DictInvalidError:
            if not invalid_ok:
                raise

        except DictNotExistsError:
            if not missing_ok:
                raise

        return default

    @final
    def __getitem__(self, name: str) -> E:
        """Get the dict.

        Args:
            name: the dict name to get

        Returns:
            the dict entry

        Raises:
            DictNotExistsError: if the dict does not exist
            DictInvalidError: if the dict is invalid

        Notes:
            - if not found, the dict is marked as missing before raising (avoiding trying to load it again on next call)
            - getting the same dict multiple times will return the same object (cached)
            - same as: provider.get(dict_name, invalid_ok=False, missing_ok=False)
        """
        if self.is_invalid(name):  # only the provider can know if it is invalid
            raise DictInvalidError(name)

        if self.is_missing(name):  # avoid trying to (re)load if we know it is missing
            raise DictNotExistsError(name)

        if (d := self._dicts.get(name)) is None:  # not already loaded
            try:
                d = self._dicts[name] = self._load(name)  # try to load it
            except Exception as err:
                self._missing.add(name)  # mark it as missing
                raise DictNotExistsError(name) from err

        return d

    @final
    def __delitem__(self, name: str) -> None:
        """Delete a dict entry.

        Args:
            name: the dict name to delete

        Raises:
            DictNotExistsError: if the dict does not exist
            ReadOnlyError: if the provider is read-only

        Notes:
            - may or may not check if data are linked to the dict before deletion
            - if the dict is loaded, it is obviously considered unloaded
            - instances of the related dict entry, if still accessible, may not be used nor trusted anymore
            - same as: provider.delete(dict_name)
        """
        self.delete(name)

    @final
    def __contains__(self, name: str) -> bool:
        """Check if the dict is loaded (exists in cache).

        Args:
            name: the dict name

        Returns:
            True if the dict loaded, False otherwise

        Notes:
            - same as: provider.is_loaded(dict_name)
        """
        return name in self._dicts

    @final
    def __iter__(self) -> Iterator[str]:
        """Get all the dicts names.

        Returns:
            an iterator of all the dicts names

        Notes:
            - same as: provider.iter(False)
        """
        return self.iter()

    @final
    def __len__(self) -> int:
        """Get the number of existing dicts.

        Returns:
            the number of existing dicts
        """
        return sum(1 for _ in self)


class DataProvider[J: JobEntry](Provider[J], ABC):
    """Base class for data provider implementations, providing an abstraction layer for data management.

    Production/subproduction can be access through a data provider.
    From data providers, job entries can be accessed, created, updated, and deleted.
    From job entries, data entries can be accessed, created, updated, and deleted.
    From data entries, data can be read/written.

    E.g.:
        Production 00001234/0000:
            - Job n°00000042:
                - Data log_data_0001.xml
                - Data job.info
                  ...
            - Job 00001200:
                - Data log_data_0042.xml
                - Data log.txt
                - Data stdout.xml
                ...

    Dict providers are used to stored Zstandard dictionaries, used for both compression and decompression.
    Dict entries are used to access dictionaries data, identified by a unique name.
    The name of a dictionary is the common part between all related files = the filenames without numbers.

    E.g.:
        - Both log_data_0001.xml and log_data_0042.xml are compressed with the same dictionary, named log_data_xxxx.xml.

    When a provider is read-only, it is not possible to create, update, or delete anything.
    When a data provider is compressed (assigned to a dictionary provider),
    underlying data is compressed with Zstandard and the dictionary provider contains the dictionaries,
    used for both compression and decompression.

    Some data providers implementations may not implement the related dict provider,
    supporting only the old "uncompressed" format. On the other hand, some implementations may only support access to
    the new "compressed" format (Zstandard).
    Some implementations may only support the read-only mode.

    The "compressed" status, indicating if the underlying data are compressed in Zstandard or just in a raw uncompressed
    format, is just an "indication". Indeed, no matter what type is a data provider, it will always read/write data as
    is without any processing, considering data are provided/gave already in the good compressed/uncompressed state.
    """

    __slots__ = ("_dict_provider",)

    def __init__(self, dict_provider: DictProvider | None = None, *, readonly: bool) -> None:
        """[Internal] Initialize the provider.

        Args:
            dict_provider: the dict provider associated to the data (default is None), specifying this implies that the provided data are compressed.
                           Some providers may not support this or supports only dict providers from the same implementation.
            readonly: indicate weather the provider is read-only or not
        """
        super().__init__(readonly=readonly)
        self._dict_provider = dict_provider

    @property
    @final
    def compressed(self) -> bool:
        """Check if the underlying data are compressed or not (in Zstandard).

        Returns:
            True if the data are compressed, False otherwise

        Notes:
            - True implies that the `dict_provider` is not None
            - False implies that the `dict_provider` is None
        """
        return self._dict_provider is not None

    @property
    @final
    def dict_provider(self) -> DictProvider | None:
        """Get the linked dict provider or None.

        Returns:
            the dict provider or None
        """
        return self._dict_provider

    @abstractmethod
    def _get(self, job: int, *, create: bool = False) -> J:
        """[Internal] Get a job entry.

        Args:
            job: the job id
            create: if True, create the job if it does not exist (default is False)

        Returns:
            the job entry

        Raises:
            JobNotExistsError: if the job does not exist and create is False
        """

    @final
    def get(self, job: int, *, create: bool = False) -> J:
        """Get a job entry.

        Args:
            job: the job id
            create: if True, create the job if it does not exist (default is False)

        Returns:
            the job entry

        Raises:
            JobNotExistsError: if the job does not exist and create is False
            ReadOnlyError: if the provider is read-only and create is True
        """
        if create and self.readonly:
            msg = "The provider is read-only"
            raise ReadOnlyError(msg)

        return self._get(job, create=create)

    @abstractmethod
    def _create(self, job: int, *, exists_ok: bool = False) -> J:
        """[Internal] Create a job entry.

        Args:
            job: the job id
            exists_ok: if True, ignore the error if the job already exists (default is False)

        Returns:
            the job entry

        Raises:
            JobExistsError: if the job already exists and exists_ok is False
        """

    @final
    def create(self, job: int, *, exists_ok: bool = False) -> J:
        """Create a job entry.

        Args:
            job: the job id
            exists_ok: if True, ignore the error if the job already exists (default is False)

        Returns:
            the job entry

        Raises:
            JobExistsError: if the job already exists and exists_ok is False
            ReadOnlyError: if the provider is read-only
        """
        if self.readonly:
            msg = "The provider is read-only"
            raise ReadOnlyError(msg)

        return self._create(job, exists_ok=exists_ok)

    @abstractmethod
    def _delete(self, job: int, *, force: bool = False) -> None:
        """[Internal] Delete a job.

        Args:
            job: the job id
            force: if True, delete the job even if it is not empty (default is False)

        Raises:
            JobNotExistsError: if the job does not exist
            DataExistsError: if the job is not empty and force is False
        """

    @final
    def delete(self, job: int, *, force: bool = False) -> None:
        """Delete a job.

        Args:
            job: the job id
            force: if True, delete the job even if it is not empty (default is False)

        Raises:
            JobNotExistsError: if the job does not exist
            DataExistsError: if the job is not empty and force is False
            ReadOnlyError: if the provider is read-only
        """
        if self.readonly:
            msg = "The provider is read-only"
            raise ReadOnlyError(msg)

        self._delete(job, force=force)

    @final
    def clear(self, *, force: bool = False) -> None:
        """Clear all the jobs.

        Args:
            force: if True, delete the jobs even if they are not empty (default is False)

        Raises:
            DataExistsError: if a job is not empty and force is False
            ReadOnlyError: if the provider is read-only
        """
        if self.readonly:
            msg = "The provider is read-only"
            raise ReadOnlyError(msg)

        for j in self.jobs():
            self.delete(j, force=force)

    @abstractmethod
    def jobs(self) -> Generator[int, None, None]:
        """Get all existing jobs id.

        Returns:
            a generator of the jobs id
        """

    @override
    @property
    def size(self) -> int:
        """Get the size of all jobs.

        Returns:
            the whole job size

        Notes:
            - see JobEntry.job_size for more details
        """
        return sum(i.job_size for i in self)

    @property
    def data_size(self) -> int:
        """Get all stored data size.

        Returns:
            the whole data size

        Notes:
            - see JobEntry.data_size for more details
        """
        return sum(i.data_size for i in self)

    @final
    def __iter__(self) -> Iterator[J]:
        """Iterate over all the jobs entries.

        Returns:
            an iterator of the jobs entries
        """
        return (self.get(i) for i in self.jobs())

    @final
    def __getitem__(self, job: int) -> J:
        """Get a job entry.

        Args:
            job: the job id

        Returns:
            the job entry

        Raises:
            JobNotExistsError: if the job does not exist

        Notes:
            - same as: provider.get(job)
        """
        return self.get(job)

    @override
    def __len__(self) -> int:
        """Get the number of jobs.

        Returns:
            the number of jobs
        """
        return sum(1 for _ in self.jobs())

    @final
    def transfer(self, target: DataProvider, *, limit: int = 0) -> int:
        """Transfer (copy) all jobs data to another provider.

        Args:
            target: the target provider
            limit: the maximum number of jobs to transfer (default is 0, meaning all)

        Returns:
            the number of transferred jobs

        Raises:
            ValueError: if the target provider is not of the same format (`compressed` value is different)
            JobExistsError: if copied jobs already exists in the target provider
            DictExistsError: if copied dicts already exists in the target provider
            ReadOnlyError: if the target provider is read-only

        Notes:
            - nothing is transferred if the target provider is the same as the source provider
            - if the providers are compressed (Zstandard format), the dict provider is also transferred, first
            - dict provider transfer is only performed if the target dict provider is not read-only
        """
        if self is target:
            return 0

        if self.compressed != target.compressed:
            msg = "The data provider compression mode must be the same"
            raise ValueError(msg)

        if target.readonly:
            msg = "The target provider is read-only"
            raise ReadOnlyError(msg)

        if self.compressed and not target.dict_provider.readonly:
            self.dict_provider.transfer(target.dict_provider)

        n = -1
        for n, job in enumerate(self):
            tjob = target.create(job.job, exists_ok=False)

            if tjob.compressed:
                tjob.update_info(job.info)

            for i in job:
                dst = tjob.create(i.name, exists_ok=False)

                with i.reader() as r, dst.writer() as w:
                    copyfileobj(r, w)

            if limit and n >= limit - 1:
                break

        return n + 1
